# README #

Инструкция для Mac

1. Качаем и устанавливаем с помощью brew **android-sdk**

```
#!sh

brew install android

```

1. Экспортируем переменную для окружения 


```
#!sh

export ANDROID_HOME=/usr/local/opt/android-sdk
```


2. После установки запускаем Android SDK manager из консоли командой

```
#!sh

android

```

3. Ставим галки на папках
 
* Extras
* Android 4.0.3
* Android 6.0

4. Пока качается необходимо скачать и установить
[Intel HAXM driver](https://software.intel.com/ru-ru/android/articles/intel-hardware-accelerated-execution-manager-end-user-license-agreement-macosx)

5. Запускаем проект из репозитория сборщиком выбираем gradle

Если не видит android-sdk вручную к папке указываем

```
#!sh

/usr/local/Cellar/android-sdk/24.4.1_1

```

6. Когда IDEA запустилась необходимо создать эмулятор через который будет запускаться проект

Нажимаем **Run**. IDEA предлагает создать эмулятор

Жмем кнопку **...**
![1.png](https://bitbucket.org/repo/y4nEBR/images/4018153141-1.png)

Создаем новый эмулятор жмем на кнопку **Create Virtual Device**
![2.png](https://bitbucket.org/repo/y4nEBR/images/3926656332-2.png)

7. Выбираем из списка понравившийся экран

8. Выбираем эмулятор из списка Marshmalow x86_64

![3.png](https://bitbucket.org/repo/y4nEBR/images/126834191-3.png)

9.Переходим на следующий экран и нажимаем **Finish**

10. При запуске приложения выбираем созданный эмулятор