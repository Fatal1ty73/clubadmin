package ru.dz.clubadmin.api.services;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.Promoter;
import ru.dz.clubadmin.domain.models.Promotion;

import java.util.List;

public interface PromoService {

    @POST("/api/v1/promotions/fb")
    Call<ResponseMessage> createFeedback(@Query("api_key") String apiKey, @Body Promotion promotion);

    @GET("/api/v1/promotions/fb/users")
    Call<List<Promoter>> getPromoters(@Query("api_key") String apiKey, @Query("venueId") Long venueId);

}
