package ru.dz.clubadmin.api.services;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.*;

import java.util.List;

public interface ReportsService {
    @GET("/api/v1/reports/clicker")
    Call<List<ClickerReportPoint>> getClickerReport(@Query("api_key") String apiKey,
                                                    @Query("venueId") Long venueId,
                                                    @Query("dateTo") String dateTo,
                                                    @Query("dateFrom") String dateFrom,
                                                    @Query("reportType") ReportTypeEnum reportType
    );

    @GET("/api/v1/reports/clicker/pdf")
    Call<ResponseMessage> getClickerReportPdf(@Query("api_key") String apiKey,
                                              @Query("venueId") Long venueId,
                                              @Query("dateTo") String dateTo,
                                              @Query("dateFrom") String dateFrom,
                                              @Query("reportType") ReportTypeEnum reportType
    );

    @GET("/api/v1/reports/employees")
    Call<EmployeesReport> getEmployeesReport(@Query("api_key") String apiKey,
                                             @Query("venueId") Long venueId,
                                             @Query("dateTo") String dateTo,
                                             @Query("dateFrom") String dateFrom,
                                             @Query("reportType") ReportTypeEnum reportType
    );

    @GET("/api/v1/reports/employees/pdf")
    Call<ResponseMessage> getEmployeesReportPdf(@Query("api_key") String apiKey,
                                                @Query("venueId") Long venueId,
                                                @Query("dateTo") String dateTo,
                                                @Query("dateFrom") String dateFrom,
                                                @Query("reportType") ReportTypeEnum reportType
    );

    @GET("/api/v1/reports/promoters")
    Call<PromotersReport> getPromotersReport(@Query("api_key") String apiKey,
                                             @Query("venueId") Long venueId,
                                             @Query("dateTo") String dateTo,
                                             @Query("dateFrom") String dateFrom,
                                             @Query("reportType") ReportTypeEnum reportType
    );

    @GET("/api/v1/reports/promoters/pdf")
    Call<ResponseMessage> getPromotersReportPdf(@Query("api_key") String apiKey,
                                                @Query("venueId") Long venueId,
                                                @Query("dateTo") String dateTo,
                                                @Query("dateFrom") String dateFrom,
                                                @Query("reportType") ReportTypeEnum reportType
    );

    @GET("/api/v1/reports/reservations")
    Call<Report> getReservationsReport(@Query("api_key") String apiKey,
                                       @Query("venueId") Long venueId,
                                       @Query("dateTo") String dateTo,
                                       @Query("dateFrom") String dateFrom,
                                       @Query("reportType") ReportTypeEnum reportType
    );

    @GET("/api/v1/reports/reservations/pdf")
    Call<ResponseMessage> getReservationsReportPdf(@Query("api_key") String apiKey,
                                                   @Query("venueId") Long venueId,
                                                   @Query("dateTo") String dateTo,
                                                   @Query("dateFrom") String dateFrom,
                                                   @Query("reportType") ReportTypeEnum reportType
    );


}
