package ru.dz.clubadmin.api.services;

import retrofit2.Call;
import retrofit2.http.*;
import ru.dz.clubadmin.domain.CurrentClickerHumanStats;
import ru.dz.clubadmin.domain.CurrentClickerStats;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.AlertClicker;

public interface ClickerService {
    @POST("/api/v1/clicker/alert")
    Call<ResponseMessage> sendAlert(@Query("api_key") String apiKey, @Body AlertClicker alert);

    @GET("/api/v1/clicker/state")
    Call<CurrentClickerStats> getCurrentClickerStats(@Query("api_key") String apiKey, @Query("venueId") Long venueId, @Query("eventId") Long eventId);

    @PUT("/api/v1/clicker/state")
    Call<ResponseMessage> updateStats(@Query("api_key") String apiKey, @Query("venueId") Long venueId, @Query("eventId") Long eventId, @Body CurrentClickerHumanStats clickerUpdate);
}
