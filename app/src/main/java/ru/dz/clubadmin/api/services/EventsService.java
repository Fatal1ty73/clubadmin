package ru.dz.clubadmin.api.services;


import retrofit2.Call;
import retrofit2.http.*;
import ru.dz.clubadmin.domain.EventCalendar;
import ru.dz.clubadmin.domain.EventsList;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.EventInfo;

import java.util.List;

public interface EventsService {
    @GET("/api/v1/events")
    Call<EventsList> getEvents(@Query("api_key") String apiKey,
                               @Query("date") String date,
                               @Query("venueId") Long venueId);

    @POST("/api/v1/events")
    Call<ResponseMessage> createEvent(@Query("api_key") String apiKey, @Body EventInfo eventInfo);

    @PUT("/api/v1/events")
    Call<ResponseMessage> updateEvent(@Query("api_key") String apiKey, @Body EventInfo eventInfo);

    @GET("/api/v1/events/calendar")
    Call<List<EventCalendar>> getEventsCalendar(@Query("api_key") String apiKey,
                                                @Query("date") String date,
                                                @Query("venueId") String venueId);

    @DELETE("/api/v1/events/{id}")
    Call<ResponseMessage> deleteEvent(@Path("id") Long eventId, @Query("api_key") String apiKey);

}
