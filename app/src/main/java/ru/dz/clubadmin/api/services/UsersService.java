package ru.dz.clubadmin.api.services;

import retrofit2.Call;
import retrofit2.http.*;
import ru.dz.clubadmin.domain.ApiKey;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.UserInfo;
import ru.dz.clubadmin.domain.models.User;
import ru.dz.clubadmin.domain.models.UserSettings;

public interface UsersService {

    @DELETE("/api/v1/user")
    Call<ResponseMessage> deleteUser(@Query("api_key") String apiKey);

    @GET("/api/v1/user")
    Call<UserInfo> getCurrentUser(@Query("api_key") String apiKey);

    @POST("/api/v1/user")
    Call<ResponseMessage> createUser(@Body User user);

    @PUT("/api/v1/user")
    Call<ResponseMessage> updateUser(@Query("api_key") String apiKey, @Body User user);

    @GET("/api/v1/user/api_key")
    Call<ApiKey> getApiKey(@Query("phoneNumber") String phoneNumber,
                           @Query("securityCode") String securityCode);

    @GET("/api/v1/user/code")
    Call<ResponseMessage> getSecurityCode(@Query("phoneNumber") String phone);

    @GET("/api/v1/user/settings")
    Call<UserSettings> getUserSettings(@Query("api_key") String apiKey);

    @PUT("/api/v1/user/settings")
    Call<UserSettings> updateUserSettings(@Query("api_key") String apiKey, @Body UserSettings settings);
}
