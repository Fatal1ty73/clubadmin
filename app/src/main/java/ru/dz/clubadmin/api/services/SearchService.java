package ru.dz.clubadmin.api.services;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.dz.clubadmin.domain.SearchUserInfo;

import java.util.List;

public interface SearchService {
    @GET("/api/v1/search/users")
    Call<List<SearchUserInfo>> searchUser(@Query("api_key") String apiKey,
                                          @Query("namePart") String namePart,
                                          @Query("phonePart") String phonePart,
                                          @Query("venueId") String venueId);
}
