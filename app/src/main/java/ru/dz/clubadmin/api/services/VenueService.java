package ru.dz.clubadmin.api.services;


import retrofit2.Call;
import retrofit2.http.*;
import ru.dz.clubadmin.domain.*;
import ru.dz.clubadmin.domain.models.EndOfDayItem;
import ru.dz.clubadmin.domain.models.State;
import ru.dz.clubadmin.domain.models.TagTO;
import ru.dz.clubadmin.domain.models.Venue;

import java.util.List;

public interface VenueService {

    @POST("api/v1/venue")
    Call<Venue> createVenue(@Query("api_key") String apiKey,
                            @Body Venue venue);

    @PUT("api/v1/venue")
    Call<ResponseMessage> updateVenue(@Query("api_key") String apiKey,
                                      @Body Venue venue);

    @DELETE("api/v1/venue/{id}")
    Call<ResponseMessage> deleteVenue(@Path("id") Long id,
                                      @Query("api_key") String apiKey);

    @GET("api/v1/venue/{id}")
    Call<Venue> getVenue(@Path("id") Long id,
                         @Query("api_key") String apiKey);

    @GET("/api/v1/venue/{id}/customers")
    Call<List<VisitorInfo>> getCustomersList(@Path("id") Long id,
                                             @Query("api_key") String apiKey,
                                             @Query("pageIndex") Integer pageIndex,
                                             @Query("pageSize") Integer pageSize,
                                             @Query("tags") String tagsOption);

    @GET("/api/v1/venue/{id}/eod")
    Call<List<EndOfDayItem>> getEndOfDay(@Path("id") Long id,
                                         @Query("api_key") String apiKey,
                                         @Query("eventId") Long eventId,
                                         @Query("date") String date);


    @GET("/api/v1/venue/{id}/guest/{guestId}")
    Call<VisitorInfo> getGuestInfo(@Path("id") Long id,
                                   @Path("guestId") Long guestId,
                                   @Query("api_key") String apiKey);


    @GET("api/v1/venue/{id}/nextdate")
    Call<NextDate> getNextEventDate(@Path("id") Long id, @Query("api_key") String apiKey, @Query("from") String from);

    @GET("api/v1/venue/{id}/people")
    Call<VenuePeopleList> getVenueRequestsAndEmployees(@Path("id") long id,
                                                       @Query("api_key") String apiKey);


    @GET("api/v1/venue/{id}/prevdate")
    Call<NextDate> getPrevEventDate(@Path("id") Long id, @Query("api_key") String apiKey, @Query("from") String from);

    @DELETE("api/v1/venue/{id}/request")
    Call<ResponseMessage> discardVenueRequest(@Path("id") int id,
                                              @Query("api_key") String apiKey);

    @DELETE("api/v1/venue/{id}/request")
    Call<ResponseMessage> discardVenueRequest(@Path("id") int id,
                                              @Query("api_key") String apiKey,
                                              @Query("userId") String userId);

    @POST("api/v1/venue/{id}/request")
    Call<ResponseMessage> createVenueRequest(@Path("id") Long venueId,
                                             @Query("api_key") String apiKey,
                                             @Body VenueRequest venueRequest);

    @PUT("api/v1/venue/{id}/request")
    Call<ResponseMessage> updateStateOfVenueRequest(@Path("id") int id,
                                                    @Query("api_key") String apiKey,
                                                    @Body VenueRequest venueRequest);

    @GET("api/v1/venue/{id}/requests")
    Call<List<UserInfo>> getVenueRequests(@Path("id") int id,
                                          @Query("api_key") String apiKey);

    @GET("api/v1/venue/{id}/seating")
    Call<SeatingPlaceList> getAvailableSeatingLists(@Path("id") int id,
                                                    @Query("api_key") String apiKey);

    @GET("/api/v1/venue/{id}/snapshot")
    Call<Snapshot> getVenueSnapshot(@Path("id") int id,
                                    @Query("api_key") String apiKey,
                                    @Query("date") String date,
                                    @Query("eventId") Long eventId);

    @GET("api/v1/venue/{id}/staff")
    Call<List<UserInfo>> getVenueStaff(@Path("id") int id,
                                       @Query("api_key") String apiKey);


    @GET("api/v1/venue/{id}/staffLists")
    Call<StaffList> getAvailableStaffForReservation(@Path("id") long id, @Query("api_key") String apiKey);

    @PUT("/api/v1/venue/{id}/state")
    Call<ResponseMessage> updateState(@Path("id") int id,
                                      @Query("api_key") String apiKey,
                                      @Query("date") String date,
                                      @Query("eventId") Long eventId,
                                      @Body State state);


    @DELETE("/api/v1/venue/{id}/tag")
    Call<ResponseMessage> deleteTag(@Path("id") Long id,
                                    @Query("api_key") String apiKey,
                                    @Query("tag") String tag);

    @GET("/api/v1/venue/{id}/tag")
    Call<List<String>> getTags(@Path("id") Long id,
                               @Query("api_key") String apiKey);


    @POST("/api/v1/venue/{id}/tag")
    Call<ResponseMessage> addTag(@Path("id") Long venueId,
                                 @Query("api_key") String apiKey,
                                 @Body TagTO tag);

    @GET("/api/v1/venue/{id}/upcoming")
    Call<List<ReservationInfo>> getUpcomingReservations(@Path("id") Long id,
                                              @Query("api_key") String apiKey,
                                              @Query("pageIndex") Integer pageIndex,
                                              @Query("pageSize") Integer pageSize);

    @GET("/api/v1/venues")
    Call<List<Venue>> getAllVenues(@Query("api_key") String apiKey);

    @GET("api/v1/venues/my")
    Call<List<Venue>> venuesListOfCurrentUser(@Query("api_key") String apiKey);
}
