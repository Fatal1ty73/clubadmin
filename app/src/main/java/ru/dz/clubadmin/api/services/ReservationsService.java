package ru.dz.clubadmin.api.services;

import retrofit2.Call;
import retrofit2.http.*;
import ru.dz.clubadmin.domain.*;

import java.util.ArrayList;
import java.util.List;

public interface ReservationsService {
    @POST("api/v1/reservation")
    Call<ReservationInfo> createReservation(@Query("api_key") String apiKey,
                                            @Query("approved") Boolean approved,
                                            @Body ReservationInfo reservationInfo);

    @PUT("api/v1/reservation")
    Call<ResponseMessage> updateReservation(@Query("api_key") String apiKey, @Body ReservationInfo reservation);

    @DELETE("api/v1/reservation/{id}")
    Call<ResponseMessage> deleteReservation(@Path("id") Long id, @Query("api_key") String apiKey);

    @GET("api/v1/reservation/{id}")
    Call<ReservationInfo> getReservation(@Query("api_key") String apiKey, @Path("id") Long reservationId);

    @PUT("api/v1/reservation/{id}/assignStaff")
    Call<ResponseMessage> assignStaff(@Path("id") Long reservationId,
                                      @Query("api_key") String apiKey,
                                      @Query("role") String role,
                                      @Body SearchUserInfo staffInfo);


    @PUT("api/v1/reservation/{id}/assignStaffBatch")
    Call<ResponseMessage> assignStaffBatch(@Path("id") Long reservationId,
                                           @Query("api_key") String apiKey,
                                           @Body ArrayList<StaffAssignment> staffInfo);

    @PUT("api/v1/reservation/{id}/assignTable")
    Call<ResponseMessage> assignTable(@Path("id") Long id,
                                      @Query("api_key") String apiKey,
                                      @Body TableInfo tableInfo);

    @PUT("api/v1/reservation/{id}/guests")
    Call<ResponseMessage> changeSeatedGuests(@Path("id") Long reservationId,
                                             @Query("api_key") String apiKey,
                                             @Query("action") String action,
                                             @Query("gender") String gender);

    @DELETE("/api/v1/reservation/{id}/image")
    Call<ResponseMessage> deleteReservationPicture(@Path("id") Long id,
                                                   @Query("api_key") String apiKey,
                                                   @Query("url") String url);

    @PUT("/api/v1/reservation/{id}/image")
    Call<ResponseMessage> attachReservationPicture(@Path("id") Long id,
                                                   @Query("api_key") String apiKey,
                                                   @Query("url") String url);

    @PUT("/api/v1/reservation/{id}/move")
    Call<ResponseMessage> moveReservation(@Path("id") Long id,
                                          @Query("api_key") String apiKey,
                                          @Query("eventId") Long eventId,
                                          @Query("newDate") String newDate);

    @PUT("/api/v1/reservation/{id}/reactivate")
    Call<ResponseMessage> reactivateReservation(@Path("id") Long id,
                                                @Query("api_key") String apiKey);


    @PUT("/api/v1/reservation/{id}/state")
    Call<ResponseMessage> changeReservationState(@Path("id") Long id,
                                                 @Query("api_key") String apiKey,
                                                 @Query("newState") String newState);

    @PUT("/api/v1/reservation/{id}/tags")
    Call<ResponseMessage> setTagReservation(@Path("id") Long id,
                                            @Query("api_key") String apiKey,
                                            @Body List<String> tags);

    @PUT("/api/v1/reservation/{id}/unassign")
    Call<ResponseMessage> unassignTable(@Path("id") Long id,
                                        @Query("api_key") String apiKey);

    @PUT("/api/v1/reservation/{id}/unassignStaff")
    Call<ResponseMessage> unassignStaff(@Path("id") Long id,
                                        @Query("api_key") String apiKey,
                                        @Query("role") String role,
                                        @Body SearchUserInfo staffInfo);

    @PUT("/api/v1/reservation/{id}/unassignStaffBatch")
    Call<ResponseMessage> unassignStaffBatch(@Path("id") Long reservationId,
                                             @Query("api_key") String apiKey,
                                             @Body ArrayList<StaffAssignment> staffInfo);

    @GET("api/v1/reservations")
    Call<ReservationsLists> getReservations(@Query("api_key") String apiKey,
                                            @Query("venueId") String venueId,
                                            @Query("date") String date,
                                            @Query("eventId") Long eventId);

    @GET("/api/v1/reservations/bs")
    Call<List<ReservationInfo>> getReservationsBs(@Query("api_key") String apiKey,
                                                  @Query("venueId") String venueId,
                                                  @Query("date") String date,
                                                  @Query("bsType") String bsType,
                                                  @Query("eventId") Long eventId);


}
