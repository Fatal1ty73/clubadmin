package ru.dz.clubadmin.api.services;

import retrofit2.Call;
import retrofit2.http.*;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.TableInfo;
import ru.dz.clubadmin.domain.TableWithSeating;
import ru.dz.clubadmin.domain.TablesList;
import ru.dz.clubadmin.domain.models.CreateTableRequest;

import java.util.List;

public interface TablesService {
    @GET("/api/v1/tables")
    Call<TablesList> getSeatingPlaceList(@Query("api_key") String apiKey, @Query("venueId") Long venueId);

    @POST("/api/v1/tables")
    Call<ResponseMessage> createTable(@Query("api_key") String apiKey, @Body CreateTableRequest table);

    @PUT("/api/v1/tables")
    Call<ResponseMessage> updateTable(@Query("api_key") String apiKey, @Body TableInfo table);

    @GET("/api/v1/tables/seating")
    Call<List<TableWithSeating>> getTablesWithSeating(@Query("venueId") Long venueId,
                                                      @Query("date") String date,
                                                      @Query("eventId") Long eventId,
                                                      @Query("api_key") String apiKey);

    @DELETE("/api/v1/tables/{id}")
    Call<ResponseMessage> deleteSeatingPlace(@Path("id") Long id, @Query("api_key") String apiKey);

    @PUT("/api/v1/tables/{id}/close")
    Call<ResponseMessage> closeTable(@Path("id") Long id,
                                     @Query("api_key") String apiKey,
                                     @Query("date") String date);

    @PUT("/api/v1/tables/{id}/open")
    Call<ResponseMessage> openTable(@Path("id") Long id,
                                    @Query("api_key") String apiKey,
                                    @Query("date") String date);
}
