package ru.dz.clubadmin.api.services;


import retrofit2.Call;
import retrofit2.http.*;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.FeedbackInfo;
import ru.dz.clubadmin.domain.models.FeedbackList;

public interface FeedbackService {
    @GET("/api/v1/feedbacks")
    Call<FeedbackList> getFeedback(@Query("api_key") String apiKey, @Query("reservationId") String reservationId);

    @GET("/api/v1/feedbacks")
    Call<FeedbackList> getFeedbackByUserId(@Query("api_key") String apiKey, @Query("reservationId") String reservationId, @Query("userId") String userId);

    @POST("/api/v1/feedbacks")
    Call<ResponseMessage> createFeedback(@Query("api_key") String apiKey, @Body FeedbackInfo feedback);

    @PUT("/api/v1/feedbacks")
    Call<ResponseMessage> updateFeedback(@Query("api_key") String apiKey, @Body FeedbackInfo feedback);

    @DELETE("/api/v1/feedbacks/{id}")
    Call<ResponseMessage> deleteFeedback(@Path("id") Long id, @Query("api_key") String apiKey);
}
