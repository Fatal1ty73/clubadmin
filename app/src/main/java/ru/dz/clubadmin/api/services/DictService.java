package ru.dz.clubadmin.api.services;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.Map;


public interface DictService {
    @GET("/api/v1/dict/bstypes")
    Call<Map<String,String>> getBstype(@Query("api_key") String apiKey);

    @GET("/api/v1/dict/groups")
    Call<Map<String,String>> getReservationsGroupTypes(@Query("api_key") String apiKey);

    @GET("/api/v1/dict/promoters")
    Call<Map<String,String>> getPromotrsTypes(@Query("api_key") String apiKey);

    @GET("/api/v1/dict/roles")
    Call<Map<String,String>> getRoles(@Query("api_key") String apiKey);
}
