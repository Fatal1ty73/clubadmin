package ru.dz.clubadmin.api.services;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.dz.clubadmin.domain.ScheduleInfo;

public interface ScheduleService {
    @GET("/api/v1/schedule")
    Call<ScheduleInfo> getUserSchedule(@Query("api_key") String apiKey, @Query("venueId") String venueId, @Query("eventId") Long eventId);

}
