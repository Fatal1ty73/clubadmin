package ru.dz.clubadmin;

import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.google.inject.Inject;
import io.fabric.sdk.android.Fabric;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.domain.ApiKey;
import ru.dz.clubadmin.ui.fragments.auth.AuthFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

@ContentView(R.layout.activity_main)
public class VenueAuthActivity extends RoboActivity {
    private static final String TAG = "AuthActivity";
    private static final String CRASHLYTICS_KEY_CRASHES = "crashes";

    private FragmentManager fragmentManager;

    @Inject
    private AuthFragment authFragment;

    @Inject
    private FragmentUtils fragmentUtils;
    @Inject
    private ApiKeyHelper apiKeyHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CrashlyticsCore crashlyticsCore = new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build();
        Fabric.with(this, new Crashlytics.Builder().core(crashlyticsCore).build(), new CrashlyticsNdk());
        Crashlytics.setBool(CRASHLYTICS_KEY_CRASHES, true);

        ApiKey responseMessage = apiKeyHelper.getApiKey();
        Log.i(TAG, "Last record " + responseMessage.getApiKey());

        fragmentManager = getFragmentManager();
        loadAuthFragment();
    }

    private void loadAuthFragment() {
        fragmentUtils.changeFragmentContent(fragmentManager, authFragment, R.id.contentPanel);
    }
    @Override
    public void onBackPressed() {
        if(fragmentManager.getBackStackEntryCount() != 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
            System.exit(0);

        }
    }

}
