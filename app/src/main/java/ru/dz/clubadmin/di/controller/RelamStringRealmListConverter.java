package ru.dz.clubadmin.di.controller;

import com.google.gson.*;
import io.realm.RealmList;
import ru.dz.clubadmin.domain.models.RealmString;

import java.lang.reflect.Type;

public class RelamStringRealmListConverter implements JsonSerializer<RealmList<RealmString>>,
        JsonDeserializer<RealmList<RealmString>> {


    @Override
    public RealmList<RealmString> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        RealmList<RealmString> tags = new RealmList<>();
        JsonArray ja = json.getAsJsonArray();
        for (JsonElement je : ja) {
            tags.add(new RealmString(je.getAsJsonPrimitive().getAsString()));
        }
        return tags;
    }

    @Override
    public JsonElement serialize(RealmList<RealmString> src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray ja = new JsonArray();
        for (RealmString tag : src) {
            ja.add(context.serialize(tag.getString()));
        }
        return ja;
    }
}