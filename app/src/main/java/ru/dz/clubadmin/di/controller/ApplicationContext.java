package ru.dz.clubadmin.di.controller;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Singleton;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmObject;
import okhttp3.OkHttpClient;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import roboguice.inject.RoboApplicationProvider;
import ru.dz.clubadmin.api.services.*;
import ru.dz.clubadmin.domain.models.RealmString;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class ApplicationContext extends RoboApplicationProvider<ClubadminApp> {
    private static final int TIMEOUT = 60;
    private static final int WRITE_TIMEOUT = 120;
    private static final int CONNECT_TIMEOUT = 10;


    @Singleton
    public Realm provideRealm() {
        String REALM_NAME = "default10.realm";

        RealmConfiguration config = new RealmConfiguration.Builder(get().getApplicationContext())
                .name(REALM_NAME)
                .build();
        return Realm.getInstance(config);
    }

    @Singleton
    private Retrofit getRetrofit(){
        OkHttpClient CLIENT = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .build();


        String API_ENDPOINT = "http://ec2-54-218-70-183.us-west-2.compute.amazonaws.com";
        Gson GSON_CLIENT =  new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                        return new DateTime(json.getAsJsonPrimitive().getAsLong(), DateTimeZone.getDefault()).toDate();
                    }
                }).registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
                    @Override
                    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
                        return new JsonPrimitive(src.getTime());
                    }
                }).registerTypeAdapter(new TypeToken<RealmList<RealmString>>() {}.getType(),
                        new RelamStringRealmListConverter())
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(GSON_CLIENT))
                .client(CLIENT)
                .build();
        return retrofit;
    }

    @Singleton
    public UsersService getUsersService() {

        return getRetrofit().create(UsersService.class);
    }

    @Singleton
    public VenueService getVenueService() {
        return getRetrofit().create(VenueService.class);
    }

    @Singleton
    public ReservationsService getReservationService() {
        return getRetrofit().create(ReservationsService.class);
    }

    @Singleton
    public PromoService getPromoService() {
        return getRetrofit().create(PromoService.class);
    }
    @Singleton
    public ClickerService getClickerService() {
        return getRetrofit().create(ClickerService.class);
    }
    @Singleton
    public TablesService getTablesService() {
        return getRetrofit().create(TablesService.class);
    }


    public SearchService getSearchService() {
        return getRetrofit().create(SearchService.class);
    }

    @Singleton
    public FeedbackService getFeedbackService() {
        return getRetrofit().create(FeedbackService.class);
    }

    @Singleton
    public EventsService getEventService() {
        return getRetrofit().create(EventsService.class);
    }

    @Singleton
    public ReportsService getReportsService() {
        return getRetrofit().create(ReportsService.class);
    }

    @Singleton
    public ScheduleService getScheduleService() {
        return getRetrofit().create(ScheduleService.class);
    }

}
