package ru.dz.clubadmin.ui.fragments.venue;


import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.inject.Inject;

import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class VenuesSettingsFragment extends RoboFragment {
    @InjectView(R.id.btnTags)
    Button tagBtn;

    @Inject
    FragmentUtils fragmentUtils;
    @Inject
    private FragmentManager fragmentManager;
    @Inject
    VenueTagsFragment venueTagsFragment;

    public VenuesSettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.venues_settings_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fragmentManager = getFragmentManager();

        tagBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentUtils.changeFragmentContent(fragmentManager, venueTagsFragment, R.id.content, false);
            }
        });
    }
}


