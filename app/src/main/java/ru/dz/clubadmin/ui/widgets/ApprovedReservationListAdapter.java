package ru.dz.clubadmin.ui.widgets;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import ru.dz.clubadmin.R;

/**
 * Created by Sam (samir@peller.tech) on 27.06.2016.
 */
public abstract class ApprovedReservationListAdapter<T> extends ArrayAdapter<T> {
    private LayoutInflater mInflater;

    @LayoutRes
    private int resource;
    private List<T> list;

    public ApprovedReservationListAdapter(Context context, @LayoutRes int resource, List<T> list) {
        super(context, resource, list);
        this.resource = resource;
        this.list = list;

        mInflater = LayoutInflater.from(context);
    }

    public View getView(int position, android.view.View convertView,
                        ViewGroup parent) {
        FindViewHolder holder;
        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(resource, parent, false);
            holder = new FindViewHolder();
            holder.reserveImage = (ImageView) row.findViewById(R.id.reserve_img);
            holder.reserveName = (TextView) row.findViewById(R.id.reserve_name);
            holder.reservedBy = (TextView) row.findViewById(R.id.reserved_by);
            holder.reserveNote = (TextView) row.findViewById(R.id.reserve_note);
            holder.reserveGuests = (TextView) row.findViewById(R.id.reserve_guests);
            holder.layout = (RelativeLayout) row.findViewById(R.id.reserve_content_rl);
            holder.assign = (ImageButton) row.findViewById(R.id.assign_button);
        } else {
            holder = (FindViewHolder) row.getTag();
        }
        T item = this.list.get(position);
        fillItem(item, holder);
        return row;
    }

    public abstract void fillItem(T item, FindViewHolder holder);

    public class FindViewHolder {
        public RelativeLayout layout;
        public ImageView reserveImage;
        public TextView reserveName, reservedBy, reserveNote, reserveGuests;
        public ImageButton assign;
    }
}
