package ru.dz.clubadmin.ui.activities;

import android.app.FragmentManager;
import android.os.Bundle;
import com.google.inject.Inject;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.ui.fragments.venue.VenueListFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

@ContentView(R.layout.activity_venue)
public class VenueListActivity extends RoboActivity {

    private static final String TAG = "VenueActivity";
    private String apiKey;

    @Inject
    FragmentUtils fragmentUtils;

    private FragmentManager fragmentManager;
    @Inject
    private VenueListFragment venueListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentManager = getFragmentManager();

        fragmentUtils.changeFragmentContent(fragmentManager, venueListFragment, R.id.venue_list, false);
    }

    @Override
    public void onBackPressed() {
        if(fragmentManager.getBackStackEntryCount() != 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
            finishAffinity();

        }
    }
}
