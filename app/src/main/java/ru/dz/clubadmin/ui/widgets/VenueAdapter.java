package ru.dz.clubadmin.ui.widgets;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import ru.dz.clubadmin.R;

import java.util.List;

public abstract class VenueAdapter<T> extends ArrayAdapter<T> {

    private LayoutInflater mInflater;

    @LayoutRes
    private int resource;
    private List<T> list;

    public VenueAdapter(Context context, @LayoutRes int resource, List<T> list) {
        super(context, resource, list);
        this.resource = resource;
        this.list = list;

        mInflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {
        ViewHolder holder;
        View row = convertView;
        if (row == null) {

            row = mInflater.inflate(resource, parent, false);
            holder = new ViewHolder();
            holder.coverVenue = (ImageView) row.findViewById(R.id.cover_venue);
            holder.logoVenue = (ImageView) row.findViewById(R.id.logo_venue);
            holder.titleVenue = (TextView) row.findViewById(R.id.title_venue);
            holder.descriptionVenue = (TextView) row.findViewById(R.id.description_venue);
            holder.infoVenue = (ImageButton) row.findViewById(R.id.info_venue);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        T item = this.list.get(position);
        fillItem(item, holder);
        return row;
    }

    public abstract void fillItem(T item, ViewHolder holder);

    public class ViewHolder {
        public ImageView coverVenue;
        public ImageView logoVenue;
        public TextView titleVenue, descriptionVenue;
        public ImageButton infoVenue;
    }
}