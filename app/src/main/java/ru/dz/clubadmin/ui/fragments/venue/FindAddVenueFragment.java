package ru.dz.clubadmin.ui.fragments.venue;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Toast;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboListFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.models.Venue;
import ru.dz.clubadmin.loaders.venue.VenueListLoader;
import ru.dz.clubadmin.ui.utils.FragmentUtils;
import ru.dz.clubadmin.ui.widgets.FindVenueAdapter;

import java.util.ArrayList;
import java.util.List;

public class FindAddVenueFragment extends RoboListFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {

    private static final String TAG = "FindAddVenueFragment";
    private String apiKey;

    private FragmentManager fragmentManager;
    private List<Venue> loadedVenuesList = new ArrayList<>();

    @InjectView(R.id.back_to_venue_list_from_add)
    private ImageButton backButton;
    @InjectView(R.id.search_venue_id)
    private SearchView searchVenue;
    @Inject
    private FragmentUtils fragmentUtils;
    @Inject
    private VenueListFragment venueListFragment;
    @Inject
    private VenueInfoFragment venueInfoFragment;
    @Inject
    private ApiKeyHelper apiKeyHelper;
    @Inject
    private ApplicationContext applicationContext;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_find_add_venue, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        this.apiKey = apiKeyHelper.getApiKey().getApiKey();
        if (this.apiKey != null) {
            getLoaderManager().initLoader(R.id.venue_list_loader, null, this);
        }
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentUtils.changeFragmentContent(fragmentManager, venueListFragment, R.id.venue_list);
            }
        });
        searchVenue.setQueryHint("Enter Venue name");
        searchVenue.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //showToast(String.valueOf(hasFocus));
            }
        });
        searchVenue.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                searchAndView(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchAndView(newText);
                return false;
            }
        });
        searchVenue.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                loadAdapter(loadedVenuesList);
                return false;
            }
        });
    }
    private void searchAndView(String query) {
        if (query.length() == 0) {
            loadAdapter(loadedVenuesList);
        } else {
            List<Venue> findedVenueList = new ArrayList<>();
            for (Venue venue : loadedVenuesList) {
                if (venue.getName().toLowerCase().contains(query.toLowerCase())) {
                    findedVenueList.add(venue);
                }
            }
            loadAdapter(findedVenueList);
        }
    }
    private void showToast(String message) {
        Toast toast = Toast.makeText(this.getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "Start loader");
        switch (id) {
            case R.id.venue_list_loader:
                Log.i(TAG, "Find id loader1");
                showToast("Loading venues...");
                return new VenueListLoader(this.getActivity());
            default:
                return null;
        }
    }

    private void loadAdapter(List<Venue> venueList) {
        FindVenueAdapter<Venue> adapter = new FindVenueAdapter<Venue>(this.getActivity(),
                R.layout.fragment_venue_search_item, venueList) {
            @Override
            public void fillItem(final Venue item, FindVenueAdapter<Venue>.FindViewHolder holder) {
                holder.titleVenue.setText(item.getName());
                String status = "";
                if(item.getRequestStatus()!=null){
                    status = item.getRequestStatus().toString();
                }
                holder.statusVenue.setText(status);
                holder.addressVenue.setText(item.getAddress());
                holder.contentItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle args = new Bundle();
                        args.putSerializable("item", item);
                        args.putString("from", "ADD_VENUE");
                        venueInfoFragment.setArguments(args);
                        fragmentUtils.changeFragmentContent(fragmentManager, venueInfoFragment, R.id.venue_list);
                    }
                });
                Log.i(TAG, "NAME " + item.getName());
            }
        };

        setListAdapter(adapter);

    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse baseResponse) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        if (id == R.id.venue_list_loader) {
            if (baseResponse.isSuccess()) {
                SuccessResponse<List<Venue>> resp = baseResponse.asSuccess();
                List<Venue> responseMessage = resp.getObj();
                if (responseMessage != null && !responseMessage.isEmpty()) {
                    loadedVenuesList = responseMessage;
                    Log.d(TAG, responseMessage.get(0).getName());
                    loadAdapter(responseMessage);
                }
            } else {
                ErrorMessage errorMessage = baseResponse.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                showToast(errorMessage.getError());
            }
        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }

}
