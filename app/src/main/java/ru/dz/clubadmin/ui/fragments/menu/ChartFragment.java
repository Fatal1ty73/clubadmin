package ru.dz.clubadmin.ui.fragments.menu;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import lecho.lib.hellocharts.model.*;
import lecho.lib.hellocharts.view.LineChartView;
import ru.dz.clubadmin.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sam (samir@peller.tech) on 23.05.2016.
 */
public class ChartFragment extends Fragment {
    private static final String CHART_FRAGMENT_TAG = "chartFragment";
    private LineChartView chart;
    LineChartData data = new LineChartData();
    ArrayList<Integer> allCount = new ArrayList<>();
    List<PointValue> manPoints = new ArrayList<>();
    List<PointValue> womanPoints = new ArrayList<>();
    List<PointValue> allPoints = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chart, null);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FragmentManager fragmentManager = getFragmentManager();
        View fragmentView = fragmentManager.findFragmentByTag(CHART_FRAGMENT_TAG).getView();

        if(fragmentView != null) {
            chart = (LineChartView) fragmentView.findViewById(R.id.chart);
        }

        data.setValueLabelBackgroundAuto(false);
        data.setValueLabelBackgroundColor(ContextCompat.getColor(getActivity(), R.color.chartLineDarkOrange));
        data.setValueLabelsTextColor(ContextCompat.getColor(getActivity(), R.color.chartLineOrange));
    }

    public void drawChart(ArrayList<Integer> manCount, ArrayList<Integer> womanCount, ArrayList<String> xAxisLabel) {
        calculateAllCount(manCount, womanCount);
        for(int i = 0; i < manCount.size(); i++) {
            manPoints.add(new PointValue(i, manCount.get(i)));
            womanPoints.add(new PointValue(i, womanCount.get(i)));
            allPoints.add(new PointValue(i, allCount.get(i)).setLabel(xAxisLabel.get(i) + " - " + allCount.get(i) + System.getProperty("line.separator") + "Women: " + womanCount.get(i) + " / Men: " + manCount.get(i)));
        }

        Line manLine = new Line(manPoints).setColor(ContextCompat.getColor(getActivity(), R.color.chartLineBlue)).setCubic(true).setFilled(true).setHasPoints(false);
        Line womanLine = new Line(womanPoints).setColor(ContextCompat.getColor(getActivity(), R.color.chartLineWhite)).setCubic(true).setFilled(true).setHasPoints(false);
        Line allLine = new Line(allPoints).setColor(ContextCompat.getColor(getActivity(), R.color.chartLineOrange)).setCubic(true).setFilled(true).setHasLabelsOnlyForSelected(true).setPointRadius(3);

        List<Line> lines = new ArrayList<>();
        lines.add(allLine);
        lines.add(manLine);
        lines.add(womanLine);

        List<AxisValue> axisValues = new ArrayList<>();

        for(int i = 0; i < xAxisLabel.size(); i++) {
            axisValues.add(new AxisValue(i).setLabel(xAxisLabel.get(i)));
        }

        Axis xAxis = new Axis(axisValues).setHasLines(true);
        data.setAxisXBottom(xAxis);

        Integer maxYAxisValue = getMaxYAxisValue(allCount);
        Axis yAxis = Axis.generateAxisFromRange(0, maxYAxisValue, Math.round(maxYAxisValue / 10)).setHasLines(true);
        data.setAxisYLeft(yAxis);

        data.setLines(lines);

        if (chart != null) {
            chart.setLineChartData(data);
        }
    }

    private void calculateAllCount(ArrayList<Integer> manCount, ArrayList<Integer> womanCount) {
        for(int i = 0; i < manCount.size(); i++) {
            allCount.add(manCount.get(i) + womanCount.get(i));
        }
    }

    private Integer getMaxYAxisValue(ArrayList<Integer> values) {
        int max = 0;

        for (int i = 1; i < values.size(); i++) {
            if (values.get(i) > max) {
                max = values.get(i);
            }
        }

        return max;
    }
}
