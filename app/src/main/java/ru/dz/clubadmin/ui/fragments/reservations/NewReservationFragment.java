package ru.dz.clubadmin.ui.fragments.reservations;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;

import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.ReservationInfo;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.TableInfo;
import ru.dz.clubadmin.domain.VisitorInfo;
import ru.dz.clubadmin.domain.models.GroupType;
import ru.dz.clubadmin.loaders.reservation.AssignTableLoader;
import ru.dz.clubadmin.loaders.reservation.CreateReservationLoader;
import ru.dz.clubadmin.ui.fragments.tables.TablesFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

/**
 * Created by Sam (samir@peller.tech) on 01.07.2016.
 */
public class NewReservationFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {
    private static final String TAG = "NewReservationFragment";
    private static final int CREATE_RESERVATION_INDEX = 0;
    private static final int ASSIGN_RESERVATION_TO_TABLE_INDEX = 1;
    private static final int REQUEST_DATE_TYPE  = 1;

    @InjectView(R.id.header_title)
    private TextView headerTitle;
    @InjectView(R.id.header_sub_title)
    private TextView headerSubTitle;
    @InjectView(R.id.complete_button)
    private Button completeButton;
    @InjectView(R.id.guest_information_text)
    EditText questName;
    @InjectView(R.id.guestlistRB)
    RadioButton guestlistRB;
    @InjectView(R.id.bottleserviceRB)
    RadioButton bottleserviceRB;
    @InjectView(R.id.glLayout)
    LinearLayout glLayout;
    @InjectView(R.id.bsLayout)
    LinearLayout bsLayout;
    @InjectView(R.id.tableButton)
    ToggleButton tableButton;
    @InjectView(R.id.standupButton)
    ToggleButton standupButton;
    @InjectView(R.id.barButton)
    ToggleButton barButton;
    @InjectView(R.id.guest_count)
    EditText guestCount;
    @InjectView(R.id.phone_number)
    EditText phoneNumber;
    @InjectView(R.id.booking_note)
    EditText bookingNote;
    @InjectView(R.id.guysGroupButton)
    Button guysGroupButton;
    @InjectView(R.id.girlsGroupButton)
    Button girlsGroupButton;
    @InjectView(R.id.promoGroupButton)
    Button promoGroupButton;
    @InjectView(R.id.mixGroupButton)
    Button mixGroupButton;
    @InjectView(R.id.allGirlsCompsCheckBox)
    CheckBox allGirlsCompsCheckBox;
    @InjectView(R.id.allGirlsCompsText)
    TextView allGirlsCompsText;
    @InjectView(R.id.allGirlsCompsEditText)
    EditText allGirlsCompsEditText;
    @InjectView(R.id.allGirlsReducedCheckBox)
    CheckBox allGirlsReducedCheckBox;
    @InjectView(R.id.allGirlsReducedText)
    TextView allGirlsReducedText;
    @InjectView(R.id.allGirlsReducedEditText)
    EditText allGirlsReducedEditText;
    @InjectView(R.id.allGuysCompsCheckBox)
    CheckBox allGuysCompsCheckBox;
    @InjectView(R.id.allGuysCompsText)
    TextView allGuysCompsText;
    @InjectView(R.id.allGuysCompsEditText)
    EditText allGuysCompsEditText;
    @InjectView(R.id.allGuysReducedCheckBox)
    CheckBox allGuysReducedCheckBox;
    @InjectView(R.id.allGuysReducedText)
    TextView allGuysReducedText;
    @InjectView(R.id.allGuysReducedEditText)
    EditText allGuysReducedEditText;
    @InjectView(R.id.add_tag)
    Button addTagButton;
    @Inject
    private VenueHelper venueHelper;
    @Inject
    FragmentUtils fragmentUtils;
    @Inject
    private FragmentManager fragmentManager;
    @Inject
    TablesFragment tablesFragment;
    @Inject
    AddTagFragment addTagFragment;

    private ReservationInfo reservationInfo;
    private TableInfo tableInfo;
    private GroupType groupType;
    private ArrayList<String> tags;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reservations_item_view, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        headerTitle.setText("New Reservation");
        headerSubTitle.setText(getArguments().getString("eventName"));
        tableInfo = (TableInfo)getArguments().getSerializable("tableInfo");

        bottleserviceRB.setChecked(true);
        guestlistRB.setChecked(false);
        guestlistRB.setEnabled(false);
        bsLayout.setVisibility(View.VISIBLE);
        /*standupButton.setEnabled(false);
        standupButton.setChecked(false);
        barButton.setEnabled(false);
        barButton.setChecked(false);*/

        switch(tableInfo.getBottleService().name()) {
            //case "TABLE" выставлен по умолчанию
            case "STANDUP":
                tableButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_boarder_green));
                tableButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.greenTextColor));
                standupButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_green));
                standupButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhiteText));
                break;
            case "BAR":
                tableButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_boarder_green));
                tableButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.greenTextColor));
                barButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_green));
                barButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhiteText));
                break;
        }

        guysGroupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(groupType != GroupType.ALL_GUYS) {
                    groupType = GroupType.ALL_GUYS;
                    groupTypeButtonChecking(v);
                } else {
                    groupType = null;
                    clearGroupButton(v);
                }
            }
        });
        girlsGroupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(groupType != GroupType.ALL_GIRLS) {
                    groupType = GroupType.ALL_GIRLS;
                    groupTypeButtonChecking(v);
                } else {
                    groupType = null;
                    clearGroupButton(v);
                }
            }
        });
        promoGroupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(groupType != GroupType.PROMO) {
                    groupType = GroupType.PROMO;
                    groupTypeButtonChecking(v);
                } else {
                    groupType = null;
                    clearGroupButton(v);
                }
            }
        });
        mixGroupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(groupType != GroupType.MIX) {
                    groupType = GroupType.MIX;
                    groupTypeButtonChecking(v);
                } else {
                    groupType = null;
                    clearGroupButton(v);
                }
            }
        });

        allGirlsCompsCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox) v).isChecked()) {
                    disableGirlsCheckGroup(v);
                } else {
                    enableGirlsCheckGroup();
                }
            }
        });
        allGirlsReducedCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox) v).isChecked()) {
                    disableGirlsCheckGroup(v);
                } else {
                    enableGirlsCheckGroup();
                }
            }
        });
        allGuysCompsCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox) v).isChecked()) {
                    disableGuysCheckGroup(v);
                } else {
                    enableGuysCheckGroup();
                }
            }
        });
        allGuysReducedCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox) v).isChecked()) {
                    disableGuysCheckGroup(v);
                } else {
                    enableGuysCheckGroup();
                }
            }
        });

        addTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tags != null) {
                    Bundle args = new Bundle();
                    args.putStringArrayList("tags", tags);
                    addTagFragment.setArguments(args);
                }
                fragmentUtils.changeFragmentContent(fragmentManager, addTagFragment, R.id.content);
            }
        });

        fragmentManager = getFragmentManager();

        completeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reservationInfo = new ReservationInfo();
                VisitorInfo guestInfo = new VisitorInfo();

                if(!questName.getText().toString().isEmpty()) {
                    guestInfo.setFullName(questName.getText().toString());

                    reservationInfo.setGuestInfo(guestInfo);
                    reservationInfo.setEventId(getArguments().getLong("eventId"));
                    reservationInfo.setVenueId(venueHelper.getVenue().getId());
                    reservationInfo.setBottleService(tableInfo.getBottleService());
                    DateTime displayedDate = (DateTime) getArguments().getSerializable("displayedDate");
                    reservationInfo.setReservationDate(formattedDateText(displayedDate, REQUEST_DATE_TYPE));

                    if(!guestCount.getText().toString().isEmpty()) {
                        reservationInfo.setGuestsNumber(Integer.parseInt(guestCount.getText().toString()));
                    }
                    if(!phoneNumber.getText().toString().isEmpty()) {
                        guestInfo.setPhoneNumber(phoneNumber.getText().toString());
                    }
                    if(!bookingNote.getText().toString().isEmpty()) {
                        reservationInfo.setBookingNote(bookingNote.getText().toString());
                    }
                    if(groupType != null) {
                        reservationInfo.setGroupType(groupType);
                    }

                    getLoaderManager().initLoader(CREATE_RESERVATION_INDEX, null, NewReservationFragment.this);
                }
            }
        });
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case CREATE_RESERVATION_INDEX:
                return new CreateReservationLoader(getActivity(), reservationInfo, true);
            case ASSIGN_RESERVATION_TO_TABLE_INDEX:
                return new AssignTableLoader(getActivity(), args.getLong("reservationId"), (TableInfo) getArguments().getSerializable("tableInfo"));
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        switch (loader.getId()) {
            case CREATE_RESERVATION_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<ReservationInfo> resp = data.asSuccess();
                    ReservationInfo createdReservation = resp.getObj();

                    Bundle args = new Bundle();
                    args.putLong("reservationId", createdReservation.getId());

                    getLoaderManager().initLoader(ASSIGN_RESERVATION_TO_TABLE_INDEX, args, NewReservationFragment.this);
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                }

                break;
            case ASSIGN_RESERVATION_TO_TABLE_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<ResponseMessage> resp = data.asSuccess();
                    ResponseMessage responseMessage = resp.getObj();

                    Bundle args = new Bundle();
                    args.putSerializable("eventDate", getArguments().getSerializable("displayedDate"));
                    args.putLong("eventId", getArguments().getLong("eventId"));
                    tablesFragment.setArguments(args);

                    fragmentUtils.changeFragmentContent(fragmentManager, tablesFragment, R.id.content);
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                }

                break;
            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }

    private void groupTypeButtonChecking(View v) {
        guysGroupButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        guysGroupButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_boarder_gray));
        girlsGroupButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        girlsGroupButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_boarder_gray));
        promoGroupButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        promoGroupButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_boarder_gray));
        mixGroupButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        mixGroupButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_boarder_gray));

        ((Button) v).setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhiteText));
        v.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_gray));
    }

    private void clearGroupButton(View v) {
        ((Button) v).setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        v.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_boarder_gray));
    }

    private void disableGirlsCheckGroup(View v) {
        allGirlsCompsCheckBox.setChecked(false);
        allGirlsCompsText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
        allGirlsCompsEditText.setEnabled(false);
        allGirlsCompsEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
        allGirlsReducedCheckBox.setChecked(false);
        allGirlsReducedText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
        allGirlsReducedEditText.setEnabled(false);
        allGirlsReducedEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));

        ((CheckBox) v).setChecked(true);
    }

    private void enableGirlsCheckGroup() {
        allGirlsCompsText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        allGirlsCompsEditText.setEnabled(true);
        allGirlsCompsEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        allGirlsReducedText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        allGirlsReducedEditText.setEnabled(true);
        allGirlsReducedEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
    }

    private void disableGuysCheckGroup(View v) {
        allGuysCompsCheckBox.setChecked(false);
        allGuysCompsText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
        allGuysCompsEditText.setEnabled(false);
        allGuysCompsEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
        allGuysReducedCheckBox.setChecked(false);
        allGuysReducedText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
        allGuysReducedEditText.setEnabled(false);
        allGuysReducedEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));

        ((CheckBox) v).setChecked(true);
    }

    private void enableGuysCheckGroup() {
        allGuysCompsText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        allGuysCompsEditText.setEnabled(true);
        allGuysCompsEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        allGuysReducedText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        allGuysReducedEditText.setEnabled(true);
        allGuysReducedEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
    }

    protected static String formattedDateText(DateTime date, int format) {
        if(date != null) {
            DateTimeFormatter dateFormat;

            switch(format) {
                case 0:
                    dateFormat = DateTimeFormat.forPattern("EEEE MMM d, yyyy");
                    break;
                case 1:
                    dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd");
                    break;
                default:
                    return "";
            }

            return dateFormat.print(date);
        } else {
            return "";
        }
    }
}
