package ru.dz.clubadmin.ui.fragments.reservations;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboListFragment;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.di.controller.ApplicationContext;

public class ReservationsListFragment extends RoboListFragment {

    private static final String TAG = "ReservationsList";
    private String apiKey;

    @Inject
    ApplicationContext applicationContext;

    @Inject
    private ApiKeyHelper apiKeyHelper;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reservations_list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

}
