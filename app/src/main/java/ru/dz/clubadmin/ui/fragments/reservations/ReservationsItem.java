package ru.dz.clubadmin.ui.fragments.reservations;

public enum ReservationsItem {
    GUESTLIST(0),
    PENDING(1),
    APPROVED(2),
    ARRIVED(3),
    ALL(4);

    private Integer value;

    ReservationsItem(Integer item) {
        value = item;
    }

    static public ReservationsItem getItem(Integer value) {
        for (ReservationsItem item: ReservationsItem.values()) {
            if (item.getValue().equals(value)) {
                return item;
            }
        }
        return ALL;
    }
    public Integer getValue() {
        return value;
    }
}
