package ru.dz.clubadmin.ui.widgets;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import ru.dz.clubadmin.R;

/**
 * Created by Sam (samir@peller.tech) on 03.06.2016.
 */
public abstract class SeatingListAdapter<T> extends ArrayAdapter<T> {
    private LayoutInflater mInflater;

    @LayoutRes
    private int resource;
    private List<T> list;

    public SeatingListAdapter(Context context, @LayoutRes int resource, List<T> list) {
        super(context, resource, list);
        this.resource = resource;
        this.list = list;

        mInflater = LayoutInflater.from(context);
    }

    public View getView(int position, android.view.View convertView,
                        ViewGroup parent) {
        FindViewHolder holder;
        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(resource, parent, false);
            holder = new FindViewHolder();
            holder.detailLayout = (RelativeLayout) row.findViewById(R.id.detail_rl);
            holder.emptyTableMenu = (ImageButton) row.findViewById(R.id.emptyTableMenu);
            holder.tableType = (TextView) row.findViewById(R.id.tableType);
            holder.seatingImg = (ImageView) row.findViewById(R.id.reservations_item_img);
            holder.seatingTitle = (TextView) row.findViewById(R.id.reservations_item_title);
            holder.seatingDesc = (TextView) row.findViewById(R.id.reservations_item_desc);
            holder.seatingNote = (TextView) row.findViewById(R.id.table_notes_text);
            holder.arrivedBtn = (ImageButton) row.findViewById(R.id.arrived_button_id);
            holder.counterLayout = (RelativeLayout) row.findViewById(R.id.counter_rl);
            holder.womanInc = (ImageButton) row.findViewById(R.id.woman_inc_id);
            holder.womanDec = (ImageButton) row.findViewById(R.id.woman_dec_id);
            holder.womanCount = (TextView) row.findViewById(R.id.woman_count);
            holder.manInc = (ImageButton) row.findViewById(R.id.man_inc_id);
            holder.manDec = (ImageButton) row.findViewById(R.id.man_dec_id);
            holder.manCount = (TextView) row.findViewById(R.id.man_count);
            holder.allCount = (TextView) row.findViewById(R.id.all_count);
            holder.staffStatus = (TextView) row.findViewById(R.id.staff_status_id);
            holder.reservedTableMenu = (ImageButton) row.findViewById(R.id.tableMenu);
            holder.staffLL = (LinearLayout) row.findViewById(R.id.staff_list_ll);
            holder.released = (TextView) row.findViewById(R.id.released_status_text);
            holder.main = (RelativeLayout) row.findViewById(R.id.main);
            row.setTag(holder);
        } else {
            holder = (FindViewHolder) row.getTag();
        }
        T item = this.list.get(position);
        fillItem(item, holder);
        return row;
    }

    public abstract void fillItem(T item, FindViewHolder holder);

    public class FindViewHolder {
        public RelativeLayout detailLayout, counterLayout, main;
        public ImageButton emptyTableMenu, arrivedBtn, womanInc, manInc, womanDec, manDec, reservedTableMenu;
        public ImageView seatingImg;
        public TextView tableType, seatingTitle, seatingDesc, seatingNote, womanCount, manCount, allCount, staffStatus, released;
        public LinearLayout staffLL;
    }
}
