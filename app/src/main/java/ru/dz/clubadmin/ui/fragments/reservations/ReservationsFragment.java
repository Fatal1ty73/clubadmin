package ru.dz.clubadmin.ui.fragments.reservations;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.*;
import ru.dz.clubadmin.loaders.events.EventsLoader;
import ru.dz.clubadmin.loaders.reservation.ReservationListLoader;
import ru.dz.clubadmin.loaders.venue.NextEventLoader;
import ru.dz.clubadmin.loaders.venue.PrevEventLoader;
import ru.dz.clubadmin.ui.fragments.events.EventsFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;
import ru.dz.clubadmin.ui.utils.SeparatedListAdapter;
import ru.dz.clubadmin.ui.widgets.ReservationsListAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class ReservationsFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {

    private static final String TAG = "ReservationsFragment";
    private static final int GET_CURRENT_EVENTS_INDEX = 0;
    private static final int DISPLAYED_DATE_TYPE = 0;
    private static final int REQUEST_DATE_TYPE = 1;
    private String apiKey;

    @Inject
    private ApiKeyHelper apiKeyHelper;
    @Inject
    ApplicationContext applicationContext;

    @InjectView(R.id.pager)
    private ViewPager viewPager;

    @InjectView(R.id.tab_layout)
    private TabLayout tabLayout;
    @InjectView(R.id.menu_button_id)
    private ImageButton mainMenuButton;

    @InjectView(R.id.reservations_date)
    private TextView currentDate;
    @InjectView(R.id.reservations_date_prev)
    private ImageButton prevDate;
    @InjectView(R.id.reservations_date_next)
    private ImageButton nextDate;
    @InjectView(R.id.calendar_reservations_button)
    private ImageButton calendarBtn;

    @Inject
    private VenueHelper venueHelper;
    @Inject
    private FragmentUtils fragmentUtils;
    private FragmentManager fragmentManager;
    @Inject
    private ReservationsItemView reservationsItemView;

    @Inject
    private EventsFragment eventsFragment;

    @InjectView(R.id.searchView)
    private SearchView searchView;

    @Inject
    private ReservationsListFragment listFragment;

    private ReservationsLists reservationListMessage;
    private NextDate nextDateMessage;
    private Long venueId;
    private Long eventId;
    private DateTime displayedDate;

    private ReservationsListAdapter<ReservationInfo> guestListAdapter;
    private ReservationsListAdapter<ReservationInfo> pendingListAdapter;
    private ReservationsListAdapter<ReservationInfo> approvedListAdapter;
    private ReservationsListAdapter<ReservationInfo> arrivedListAdapter;

    private ViewPagerAdapter adapter;
    private SeparatedListAdapter sepAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reservations, null);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        venueId = venueHelper.getVenue().getId();
        sepAdapter = createSepAdapter();
        setButtonEvent();
        createTabLayout(view);
        startLoader();
        guestListAdapter = createAdapter(new ArrayList<ReservationInfo>(), R.layout.fragment_reservations_item_other);
        pendingListAdapter = createAdapter(new ArrayList<ReservationInfo>(), R.layout.fragment_reservations_item_pending);
        approvedListAdapter  = createAdapter(new ArrayList<ReservationInfo>(), R.layout.fragment_reservations_item_other);
        arrivedListAdapter = createAdapter(new ArrayList<ReservationInfo>(), R.layout.fragment_reservations_item_other);

    }

    private void setButtonEvent(){
        calendarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putInt("backView", R.layout.fragment_reservations);
                eventsFragment.setArguments(args);
                fragmentUtils.changeFragmentContent(fragmentManager, eventsFragment, R.id.content);
            }
        });

        prevDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiKey key = apiKeyHelper.getApiKey();
                    Bundle args = new Bundle();
                    args.putString("currentDate", currentDate.getText().toString());
                    getActivity().getLoaderManager().restartLoader(R.id.reservations_date_prev, args, ReservationsFragment.this);

            }
        });
        nextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Bundle args = new Bundle();
                    args.putString("currentDate", currentDate.getText().toString());
                    getActivity().getLoaderManager().restartLoader(R.id.reservations_date_next, args, ReservationsFragment.this);

            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                searchAndView(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchAndView(newText);
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                searchAndView("");
                return false;
            }
        });
    }

    private void createTabLayout(final View view){
        tabLayout.setEnabled(false);
        tabLayout.addTab(tabLayout.newTab().setText("guestlist"));
        tabLayout.addTab(tabLayout.newTab().setText("pending"));
        tabLayout.addTab(tabLayout.newTab().setText("approved"));
        tabLayout.addTab(tabLayout.newTab().setText("arrived"));
        tabLayout.addTab(tabLayout.newTab().setText("all"));


        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        if (mainMenuButton!=null) {
            fragmentUtils.setMenu(mainMenuButton, view);
        }
        adapter = new ViewPagerAdapter(getChildFragmentManager());

        adapter.addFragment(listFragment);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                adapter.getItem(tab.getPosition());
                viewPager.setCurrentItem(tab.getPosition());
            }
        });

    }

    private void searchAndView(String query) {
        ReservationsItem item = ReservationsItem.getItem(tabLayout.getSelectedTabPosition());
        List<ReservationInfo> reservationInfoList;
        if(reservationListMessage == null){
            reservationListMessage = new ReservationsLists();
        }
        sepAdapter.clear();
        switch (item) {
            case GUESTLIST:
                reservationInfoList = findedReservationInfoList(reservationListMessage.getGuestlist(), query);
                adapter.addAdapter("", adapter.updatedData(guestListAdapter, reservationInfoList));
                break;
            case PENDING:
                reservationInfoList = findedReservationInfoList(reservationListMessage.getPending(), query);
                adapter.addAdapter("", adapter.updatedData(pendingListAdapter, reservationInfoList));
                break;
            case APPROVED:
                reservationInfoList = findedReservationInfoList(reservationListMessage.getApproved(), query);
                adapter.addAdapter("", adapter.updatedData(approvedListAdapter, reservationInfoList));
                break;
            case ARRIVED:
                reservationInfoList = findedReservationInfoList(reservationListMessage.getArrived(), query);
                adapter.addAdapter("", adapter.updatedData(arrivedListAdapter, reservationInfoList));
                break;
            case ALL:
                reservationInfoList = findedReservationInfoList(reservationListMessage.getGuestlist(), query);
                adapter.addAdapter("Guestlist", adapter.updatedData(guestListAdapter, reservationInfoList));
                reservationInfoList = findedReservationInfoList(reservationListMessage.getPending(), query);
                adapter.addAdapter("Pending", adapter.updatedData(pendingListAdapter, reservationInfoList));
                reservationInfoList = findedReservationInfoList(reservationListMessage.getApproved(), query);
                adapter.addAdapter("Approved", adapter.updatedData(approvedListAdapter, reservationInfoList));
                reservationInfoList = findedReservationInfoList(reservationListMessage.getArrived(), query);
                adapter.addAdapter("Arrived", adapter.updatedData(arrivedListAdapter, reservationInfoList));
                break;
            default: Log.d(TAG, "getItem: not found");
        }
        listFragment.setListAdapter(sepAdapter);
    }

    private List<ReservationInfo> findedReservationInfoList(List<ReservationInfo> reservationInfoList, String query){
        if (query.length() == 0) {
            return reservationInfoList;
        }
        List<ReservationInfo> findReservationInfoList = new ArrayList<>();
        for (ReservationInfo reservationInfo : reservationInfoList) {
            if (reservationInfo.getGuestInfo().getFullName().toLowerCase().contains(query.toLowerCase())) {
                findReservationInfoList.add(reservationInfo);
            }
        }
        return findReservationInfoList;
    }

    private void startLoader(){
        Bundle getArgs = getArguments();
        if(getArgs != null) {
            eventId = getArgs.getLong("eventId");

            if(eventId > 0) {
                displayedDate = DateTime.parse(getArguments().getString("eventDate"));
                currentDate.setText(formattedDateText(displayedDate, DISPLAYED_DATE_TYPE));

                Bundle args = new Bundle();
                args.putString("currentDate", formattedDateText(displayedDate, REQUEST_DATE_TYPE));
                getActivity().getLoaderManager().restartLoader(R.id.reservation_list_loader, args, this);
            } else {
                getActivity().getLoaderManager().initLoader(GET_CURRENT_EVENTS_INDEX, null, this);
            }
        }
    }

    private void showToast(String message){
        Toast toast = Toast.makeText(getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.reservation_list_loader:
                Log.i(TAG, "Find id loader");
                showToast("Wait, try send...");
                return new ReservationListLoader(getActivity(), String.valueOf(venueId), args.getString("currentDate"), eventId);
            case R.id.reservations_date_next:
                Log.i(TAG, "Find id loader: " +  R.id.reservations_date_next);
                showToast("Wait, try send...");
                return new NextEventLoader(getActivity(), venueId, args.getString("currentDate"));
            case R.id.reservations_date_prev:
                Log.i(TAG, "Find id loader: " + R.id.reservations_date_prev);
                showToast("Wait, try send..." + args.getString("currentDate"));
                return new PrevEventLoader(getActivity(), venueId, args.getString("currentDate"));
            case GET_CURRENT_EVENTS_INDEX:
                showToast("Wait, try send...");
                displayedDate = DateTime.now();
                return new EventsLoader(getActivity(), displayedDate, venueId);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse baseResponse) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        switch (id){
            case R.id.reservations_date_prev:
                break;
            case R.id.reservations_date_next:
                if(baseResponse.isSuccess()){
                    SuccessResponse<NextDate> resp = baseResponse.asSuccess();
                    nextDateMessage = resp.getObj();
                    if (nextDateMessage != null) {
                        currentDate.setText(nextDateMessage.getDate());

                        Log.i(TAG, nextDateMessage.getDate());

                        Bundle args = new Bundle();
                        args.putString("currentDate", nextDateMessage.getDate());
                        getActivity().getLoaderManager().restartLoader(R.id.reservation_list_loader, args, this);
                    }
                }
                break;
            case R.id.reservation_list_loader:
                if (baseResponse.isSuccess()) {
                    SuccessResponse<ReservationsLists> resp = baseResponse.asSuccess();
                    reservationListMessage = resp.getObj();
                    tabLayout.setEnabled(true);
                    adapter.getItem(tabLayout.getSelectedTabPosition());
                    if (reservationListMessage != null) {
                        Log.d(TAG, "Size: " + reservationListMessage.getArrived().size());
                    }
                } else {
                    ErrorMessage errorMessage = baseResponse.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }
                break;
            case GET_CURRENT_EVENTS_INDEX:
                if (baseResponse.isSuccess()) {
                    SuccessResponse<EventsList> resp = baseResponse.asSuccess();
                    EventsList eventsListMessage = resp.getObj();
                    if (eventsListMessage != null) {
                        Bundle args;
                        switch (eventsListMessage.getEventsList().size()) {
                            case 1:
                                currentDate.setText(formattedDateText(displayedDate, DISPLAYED_DATE_TYPE));

                                eventId = eventsListMessage.getEventsList().get(0).getId();
                                args = new Bundle();
                                args.putString("currentDate", formattedDateText(displayedDate, REQUEST_DATE_TYPE));
                                getActivity().getLoaderManager().restartLoader(R.id.reservation_list_loader, args, this);

                                break;
                            //case size 0, > 1
                            default:
                                args = new Bundle();
                                args.putInt("backView", R.layout.fragment_reservations);
                                eventsFragment.setArguments(args);
                                fragmentUtils.changeFragmentContent(fragmentManager, eventsFragment, R.id.content);

                                break;
                        }
                    }
                } else {
                    errorLoadFinish(baseResponse);
                }
        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }

    private ReservationsListAdapter<ReservationInfo> createAdapter(List<ReservationInfo> reservationInfos, @LayoutRes final int resource) {
        return new ReservationsListAdapter<ReservationInfo>(this.getActivity(), resource, reservationInfos) {
            @Override
            public void fillItem(final ReservationInfo item, ViewHolder holder) {
                if(item != null) {
                    VisitorInfo visitorInfo = item.getGuestInfo();
                    if(visitorInfo != null) {
                        holder.titleVenue.setText(visitorInfo.getFullName());
                        if(visitorInfo.getUserpic() != null && !visitorInfo.getUserpic().isEmpty()){
                            try {
                                fragmentUtils.getLoaderImage(visitorInfo.getUserpic(),holder.icon);
                            } catch (InterruptedException | TimeoutException | ExecutionException e) {
                                if(!e.getMessage().isEmpty()) Log.e(TAG, e.getMessage());
                                else e.printStackTrace();
                            }
                        } else {
                            holder.icon.setImageResource(R.drawable.non_img);
                        }
                    }
                    //TODO compliment берется только у девушек, надо сделать правильно!
                    String compliment = (item.getComplimentGirlsQty() == null) ? "" : item.getComplimentGirlsQty().toString();
                    holder.complimentQty.setText(compliment);
                    //TODO reduced берется только у девушек, надо сделать правильно!
                    String reduced = (item.getReducedGirlsQty() == null) ? "" : item.getReducedGirlsQty().toString();
                    holder.reducedQty.setText(reduced);
                    holder.feedbackCount.setText(String.valueOf(item.getFeedbackCount()));
                    String note = (item.getBookingNote() == null) ? "" : item.getBookingNote();
                    holder.notes.setText(note);
                    String bookedByFullName = (item.getBookedBy() != null) ? item.getBookedBy().getFullName() : "";
                    holder.description.setText(bookedByFullName);

                    holder.body.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("reservationInfo", item);
                            reservationsItemView.setArguments(bundle);
                            fragmentUtils.detachFragment(fragmentManager, ReservationsFragment.this);
                            fragmentUtils.changeFragmentContent(fragmentManager, reservationsItemView, R.id.content);
                        }
                    });

                    switch (resource) {
                        case R.layout.fragment_reservations_item_other:

                            break;
                        case R.layout.fragment_reservations_item_pending:
                            break;
                    }
                }
            }
        };
    }

    private SeparatedListAdapter createSepAdapter() {
        return new SeparatedListAdapter(this.getActivity(), R.layout.fragment_reservation_title);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private ReservationsListFragment mFragmentList;

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            if(reservationListMessage != null) {
                ReservationsItem item = ReservationsItem.getItem(position);
                sepAdapter.clear();
                switch (item) {
                    case GUESTLIST:
                        addAdapter("", updatedData(guestListAdapter, reservationListMessage.getGuestlist()));
                        break;
                    case PENDING:
                        addAdapter("", updatedData(pendingListAdapter, reservationListMessage.getPending()));
                        break;
                    case APPROVED:
                        addAdapter("", updatedData(approvedListAdapter, reservationListMessage.getApproved()));
                        break;
                    case ARRIVED:
                        addAdapter("", updatedData(arrivedListAdapter, reservationListMessage.getArrived()));
                        break;
                    case ALL:
                        addAdapter("Guestlist", updatedData(guestListAdapter, reservationListMessage.getGuestlist()));
                        addAdapter("Pending", updatedData(pendingListAdapter, reservationListMessage.getPending()));
                        addAdapter("Approved", updatedData(approvedListAdapter, reservationListMessage.getApproved()));
                        addAdapter("Arrived", updatedData(arrivedListAdapter, reservationListMessage.getArrived()));
                        break;
                    default: Log.d(TAG, "getItem: not found");
                }

                mFragmentList.setListAdapter(sepAdapter);
            }

            return mFragmentList;
        }

        @Override
        public int getCount() {
            return (mFragmentList != null)? 1: 0;
        }

        public void addFragment(ReservationsListFragment fragment) {
            mFragmentList = fragment;
        }

        public ReservationsListAdapter<ReservationInfo> updatedData(ReservationsListAdapter<ReservationInfo> adapter, List<ReservationInfo> list) {
            adapter.clear();
            adapter.addAll(list);
            adapter.notifyDataSetChanged();
            return adapter;
        }
        public void addAdapter(String title, ReservationsListAdapter<ReservationInfo> adapter) {
            if(adapter.getSize() > 0) {
                sepAdapter.addSection(title, adapter);
                sepAdapter.notifyDataSetChanged();
            }
        }
    }

    private void errorLoadFinish(BaseResponse data) {
        ErrorMessage errorMessage = data.asError().getObj();
        if(errorMessage != null) {
            Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
            showToast(errorMessage.getError());
        }
    }

    private String formattedDateText(DateTime date, int format) {
        if(date != null) {
            DateTimeFormatter dateFormat;

            switch(format) {
                case 0:
                    dateFormat = DateTimeFormat.forPattern("EEEE MMM d, yyyy");
                    break;
                case 1:
                    dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd");
                    break;
                default:
                    return "";
            }

            return dateFormat.print(date);
        } else {
            return "";
        }
    }
}
