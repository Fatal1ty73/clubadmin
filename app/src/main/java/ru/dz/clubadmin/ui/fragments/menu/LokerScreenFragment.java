package ru.dz.clubadmin.ui.fragments.menu;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

/**
 * Created by nikitakulagin on 28.04.16.
 */
public class LokerScreenFragment extends RoboFragment {
    @InjectView(R.id.close_button_id)
    ImageButton closeButton;
    @Inject
    ClickerFragment clickerFragment;
    @Inject
    FragmentUtils fragmentUtils;
    @InjectView(R.id.vip_arrival_text)
    EditText vipArrival;
    @InjectView(R.id.police_id)
    RelativeLayout police_button;
    @InjectView(R.id.fire_id)
    RelativeLayout fire_button;
    @InjectView(R.id.mls_id)
    RelativeLayout mls_button;
    @InjectView(R.id.liquor_id)
    RelativeLayout liquor_button;
    @InjectView(R.id.capacity_id)
    RelativeLayout capacity_button;
    @InjectView(R.id.send_request_button_id)
    Button sendButton;

    private boolean isPolice = false;
    private boolean isFire = false;
    private boolean isMls = false;
    private boolean isLiquor = false;
    private boolean isCapacity = false;
    private void changePoliceStatus() {
        isPolice = !isPolice;
        if (isPolice) {
            setSendClickable(isPolice);
            police_button.setBackgroundResource(R.drawable.police_clicked);
        } else {
            setSendClickable(isPolice);
            police_button.setBackgroundResource(R.drawable.police_3);
        }
    }
    private void changeFireStatus() {
        isFire = !isFire;
        if (isFire) {
            setSendClickable(isFire);
            fire_button.setBackgroundResource(R.drawable.fire_clicked);
        } else {
            setSendClickable(isFire);
            fire_button.setBackgroundResource(R.drawable.fire_call_button);
        }
    }
    private void changeMlsStatus() {
        isMls = !isMls;
        if (isMls) {
            setSendClickable(isMls);
            mls_button.setBackgroundResource(R.drawable.mls_clicked);
        } else {
            setSendClickable(isMls);
            mls_button.setBackgroundResource(R.drawable.mls_call_button);
        }
    }
    private void changeLiquorStatus() {
        isLiquor = !isLiquor;
        if (isLiquor) {
            setSendClickable(isLiquor);
            liquor_button.setBackgroundResource(R.drawable.liquor_clicked);
        } else {
            setSendClickable(isLiquor);
            liquor_button.setBackgroundResource(R.drawable.liquor_call_button);
        }
    }
    private void changeCapacityStatus() {
        isCapacity = !isCapacity;
        if (isCapacity) {
            setSendClickable(isCapacity);
            capacity_button.setBackgroundResource(R.drawable.capacity_clicked);
        } else {
            setSendClickable(isCapacity);
            capacity_button.setBackgroundResource(R.drawable.capacity_call_button);
        }
    }
    private void resetStatuses() {
        capacity_button.setBackgroundResource(R.drawable.capacity_call_button);
        liquor_button.setBackgroundResource(R.drawable.liquor_call_button);
        mls_button.setBackgroundResource(R.drawable.mls_call_button);
        fire_button.setBackgroundResource(R.drawable.fire_call_button);
        police_button.setBackgroundResource(R.drawable.police_3);
        isPolice = false;
        isFire = false;
        isMls = false;
        isLiquor = false;
        isCapacity = false;
        setSendClickable(false);
    }
    private void setSendClickable(boolean flag) {
        if (flag) {
            sendButton.setClickable(flag);
            sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhiteText));
            sendButton.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPhoneButton));
        } else {
            sendButton.setClickable(flag);
            sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGreySendText));
            sendButton.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorUncheckedSendButton));
        }
    }
    private FragmentManager fragmentManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_broadcast, null);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        vipArrival.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetStatuses();
            }
        });
        fire_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetStatuses();
                changeFireStatus();
            }
        });
        police_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetStatuses();
                changePoliceStatus();
            }
        });
        mls_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetStatuses();
                changeMlsStatus();
            }
        });
        liquor_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetStatuses();
                changeLiquorStatus();
            }
        });
        capacity_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetStatuses();
                changeCapacityStatus();
            }
        });
        if(closeButton != null) {
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentUtils.changeFragmentContent(fragmentManager, clickerFragment, R.id.content, false);
                }
            });
        }
    }
}
