package ru.dz.clubadmin.ui.widgets;


import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import ru.dz.clubadmin.R;

import java.util.List;

public abstract class EventItemAdapter<T> extends ArrayAdapter<T> {
    private LayoutInflater mInflater;

    @LayoutRes
    private int resource;
    private List<T> list;

    public EventItemAdapter(Context context, @LayoutRes int resource, List<T> list) {
        super(context, resource, list);
        this.resource = resource;
        this.list = list;

        mInflater = LayoutInflater.from(context);
    }

    public View getView(int position, android.view.View convertView,
                       ViewGroup parent) {
        FindViewHolder holder;
        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(resource, parent, false);
            holder = new FindViewHolder();
            holder.venueItemImg = (ImageView) row.findViewById(R.id.venue_item_img);
            holder.deleteEvent = (ImageView) row.findViewById(R.id.delete_event_img);
            holder.eventName = (TextView) row.findViewById(R.id.event_name_txt);
            holder.eventDate = (TextView) row.findViewById(R.id.event_date_txt);
            holder.eventItemLayout = (RelativeLayout) row.findViewById(R.id.item_event_rl);
            row.setTag(holder);
        } else {
            holder = (FindViewHolder) row.getTag();
        }
        T item = this.list.get(position);
        fillItem(item, holder);
        return row;
    }

    public abstract void fillItem(T item, FindViewHolder holder);

    public class FindViewHolder {
        public ImageView venueItemImg;
        public TextView eventName, eventDate;
        public ImageView deleteEvent;
        public RelativeLayout eventItemLayout;
    }
}
