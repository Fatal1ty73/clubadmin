package ru.dz.clubadmin.ui.fragments.auth;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.domain.ApiKey;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.UserInfo;
import ru.dz.clubadmin.loaders.users.CurrentUserLoader;
import ru.dz.clubadmin.ui.activities.VenueListActivity;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

public class AuthFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {

    private static final String TAG = "AuthFragment";

    private FragmentManager fragmentManager;

    @Inject
    private ApiKeyHelper apiKeyHelper;

    @Inject
    private RegistrationFragment registrationFragment;
    @InjectView(R.id.phoneLoginBtn)
    Button phoneBtn;

    @InjectView(R.id.loadingPanel)
    RelativeLayout loadAnimation;

    @Inject
    FragmentUtils fragmentUtils;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_auth, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fragmentManager = getFragmentManager();

        phoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentUtils.changeFragmentContent(fragmentManager, registrationFragment, R.id.contentPanel);
            }
        });
        ApiKey key = apiKeyHelper.getApiKey();
        if(key.getApiKey() != null && !key.getApiKey().isEmpty()) {
            Log.i(TAG, "Key not empty");
            getActivity().getLoaderManager().restartLoader(R.id.current_user_loader, null, this);
        } else {
            loadAnimation.setVisibility(View.GONE);
            apiKeyHelper.saveApiKey(new ApiKey("a6ddb7a7-443b-4812-9a09-ecd3001669f7"));
            Log.i(TAG, "Key is empty");

        }
    }
    private void showToast(String message){
        Toast toast = Toast.makeText(getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    private void showVenuesActivity() {
        Intent intent = new Intent(getActivity(), VenueListActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.current_user_loader:
                Log.i(TAG, "Find id loader");
                showToast("Check api key...");
                return new CurrentUserLoader(getActivity());
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse baseResponse) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        if (id == R.id.current_user_loader) {

            if (baseResponse.isSuccess()) {
                loadAnimation.setVisibility(View.GONE);
                SuccessResponse<UserInfo> resp = baseResponse.asSuccess();
                UserInfo responseMessage = resp.getObj();
                if (responseMessage != null) {

                    Log.d(TAG, responseMessage.getFullName());
                    showToast(responseMessage.getFullName());
                    showVenuesActivity();
                }
            } else {
                loadAnimation.setVisibility(View.GONE);
                ErrorMessage errorMessage = baseResponse.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                showToast(errorMessage.getError());
            }
        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }
}
