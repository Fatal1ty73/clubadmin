package ru.dz.clubadmin.ui.fragments.schedule;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.ui.utils.FragmentUtils;


public class MyScheduleFragment extends RoboFragment {
    private final String TAG = this.getClass().getSimpleName();

    @Inject
    FragmentUtils fragmentUtils;

    @InjectView(R.id.menu_button_id)
    private ImageButton mainMenuButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_schedule, null);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mainMenuButton!=null) {
            fragmentUtils.setMenu(mainMenuButton, view);
        }

    }
}
