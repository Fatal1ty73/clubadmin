package ru.dz.clubadmin.ui.fragments.signin;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.loaders.users.SecurityCodeLoader;
import ru.dz.clubadmin.ui.fragments.auth.RegistrationFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

public class SignInFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {


    private static final String TAG = "SignInFragment";

    private FragmentManager fragmentManager;

    @Inject
    private RegistrationFragment registrationFragment;
    @Inject
    private EnterCodeFragment enterCodeFragment;

    @InjectView(R.id.logInBtn)
    private Button mSignInBtn;
    @InjectView(R.id.textPhoneNumber)
    private EditText mPhoneNumber;
    @InjectView(R.id.closeSignInBtn)
    private ImageButton closeBtn;

    private Handler handler;

    @Inject
    FragmentUtils fragmentUtils;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_in, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close(v);
            }
        });
        mSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLoad();
            }
        });

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if(msg.what == 2) {
                    Log.d(TAG, "onLoad finished : handler called. setting the fragment.");
                    showEnterCodeFragment();
                }
            }
        };
    }

    public void close(View view) {
        fragmentUtils.changeFragmentContent(fragmentManager, registrationFragment, R.id.contentPanel);
    }

    private void startLoad() {
        Log.i(TAG, "Clicked!");
        getActivity().getLoaderManager().restartLoader(R.id.security_code_loader, null, this);
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.security_code_loader:
                Log.i(TAG, "Find id loader");
                showToast("Wait, try send...");
                return new SecurityCodeLoader(getActivity(), mPhoneNumber.getText().toString());
            default:
                return null;
        }
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }


    private void showEnterCodeFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("phone", mPhoneNumber.getText().toString());

        enterCodeFragment.setArguments(bundle);
        fragmentUtils.changeFragmentContent(fragmentManager, enterCodeFragment, R.id.contentPanel);
    }



    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse baseResponse) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        if (id == R.id.security_code_loader) {

            if (baseResponse.isSuccess()) {
                SuccessResponse<ResponseMessage> resp = baseResponse.asSuccess();
                ResponseMessage responseMessage = resp.getObj();
                if (responseMessage != null) {
                    Log.d(TAG, responseMessage.getResponse());
                    showToast(responseMessage.getResponse());
                    handler.sendEmptyMessage(2);
                }
            } else {
                ErrorMessage errorMessage = baseResponse.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                showToast(errorMessage.getError());
            }

        }
        getLoaderManager().destroyLoader(id);

    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }


}
