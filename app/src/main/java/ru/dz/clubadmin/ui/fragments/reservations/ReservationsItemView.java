package ru.dz.clubadmin.ui.fragments.reservations;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.inject.Inject;

import org.joda.time.DateTime;

import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import roboguice.util.Strings;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.ReservationInfo;
import ru.dz.clubadmin.domain.VisitorInfo;
import ru.dz.clubadmin.domain.models.BottleServiceTypeEnum;
import ru.dz.clubadmin.loaders.reservation.UpdateReservationLoader;
import ru.dz.clubadmin.ui.fragments.tables.TablesFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class ReservationsItemView extends RoboFragment  implements LoaderManager.LoaderCallbacks<BaseResponse> {

    private final String TAG = "ReservationsItemView";

    private ReservationInfo reservationInfo;

    @InjectView(R.id.booked_by_text)
    private TextView bookedBy;

    @InjectView(R.id.arrival_time_text)
    private TextView arrivalTime;

    @InjectView(R.id.last_visit)
    private TextView lastVisit;

    @InjectView(R.id.icon_reservation)
    private ImageView iconReservation;

    @InjectView(R.id.notes_count)
    private TextView notesCount;

    @InjectView(R.id.header_sub_title)
    private TextView headerSubTitle;

    @InjectView(R.id.guest_information_text)
    private EditText guestInformationText;

    @InjectView(R.id.guest_count)
    private EditText guestCount;

    @InjectView(R.id.phone_number)
    private EditText phoneNumber;

    @InjectView(R.id.min_spend_text)
    private EditText minSpendText;

    @InjectView(R.id.bottle_min_text)
    private EditText bottleMinText;

    @InjectView(R.id.booking_note)
    private EditText bookingNote;

    @InjectView(R.id.send_notify_me_on_arrival)
    private ToggleButton sendNotifyArrival;
    @InjectView(R.id.tableButton)
    private ToggleButton tableButton;
    @InjectView(R.id.standupButton)
    private ToggleButton standupButton;
    @InjectView(R.id.barButton)
    private ToggleButton barButton;

    @InjectView(R.id.allGirlsCompsCheckBox)
    private CheckBox allGirlsCompsCheckBox;
    @InjectView(R.id.allGirlsReducedCheckBox)
    private CheckBox allGirlsReducedCheckBox;
    @InjectView(R.id.allGuysCompsCheckBox)
    private CheckBox allGuysCompsCheckBox;
    @InjectView(R.id.allGuysReducedCheckBox)
    private CheckBox allGuysReducedCheckBox;


    @InjectView(R.id.add_tag)
    private Button addTag;

    @InjectView(R.id.bottleserviceRB)
    private Button bottleService;

    @InjectView(R.id.tags)
    private TextView tags;
    @InjectView(R.id.estimated_time_text)
    private TextView estimatedTimeText;

    @InjectView(R.id.allGirlsCompsEditText)
    private EditText allGirlsCompsEditText;
    @InjectView(R.id.allGuysCompsEditText)
    private EditText allGuysCompsEditText;
    @InjectView(R.id.allGirlsReducedEditText)
    private EditText allGirlsReducedEditText;
    @InjectView(R.id.allGuysReducedEditText)
    private EditText allGuysReducedEditText;

    @InjectView(R.id.allGirlsCompsText)
    private TextView allGirlsCompsText;
    @InjectView(R.id.allGuysCompsText)
    private TextView allGuysCompsText;
    @InjectView(R.id.allGirlsReducedText)
    private TextView allGirlsReducedText;
    @InjectView(R.id.allGuysReducedText)
    private TextView allGuysReducedText;


    @InjectView(R.id.guestlistRB)
    private RadioButton guestlistRB;
    @InjectView(R.id.bottleserviceRB)
    private RadioButton bottleserviceRB;

    @InjectView(R.id.header_back_button)
    private ImageButton back;

    @InjectView(R.id.bsLayout)
    private LinearLayout bsLayout;
    @InjectView(R.id.glLayout)
    private LinearLayout glLayout;

    @Inject
    private FragmentUtils fragmentUtils;
    private FragmentManager fragmentManager;
    @Inject
    private ReservationsFragment reservationsFragment;
    @Inject
    private VenueHelper venueHelper;
    private Long venueId;

    @InjectView(R.id.complete_button)
    private Button completeButton;

    @Inject
    TablesFragment tablesFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reservations_item_view, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        Bundle args = getArguments();
        venueId = venueHelper.getVenue().getId();
        reservationInfo = (ReservationInfo) args.getSerializable("reservationInfo");
        fillFragment();
        setButtonEvent();
    }

    private void fillFragment(){
        if(reservationInfo != null){
            headerSubTitle.setText(Strings.toString(reservationInfo.getReservationDate()));
            bookedBy.setText(Strings.toString(reservationInfo.getBookedBy().getFullName()));
            arrivalTime.setText(Strings.toString(reservationInfo.getArrivalTime()));

            if(reservationInfo.getBookedBy() != null) {
                if(reservationInfo.getBookedBy().getUserpic() != null && !reservationInfo.getBookedBy().getUserpic().isEmpty()) {
                    try {
                        fragmentUtils.getLoaderImage(reservationInfo.getBookedBy().getUserpic(),iconReservation);
                    } catch (InterruptedException | TimeoutException | ExecutionException e){
                        if(!e.getMessage().isEmpty()) Log.w(TAG, e.getMessage());
                        else e.printStackTrace();
                    }
                } else {
                    iconReservation.setImageResource(R.drawable.non_img);
                }
            }
            if(reservationInfo.getGuestInfo() != null) {
                guestInformationText.setText(Strings.toString(reservationInfo.getGuestInfo().getFullName()));
                guestCount.setText(Strings.toString(reservationInfo.getGuestInfo().getTotalVisits()));
                phoneNumber.setText(Strings.toString(reservationInfo.getGuestInfo().getPhoneNumber()));
                lastVisit.setText(Strings.toString(reservationInfo.getGuestInfo().getLastReservationDate()));
            }
            bookingNote.setText(Strings.toString(reservationInfo.getBookingNote()));
            StringBuilder tegBuilder = new StringBuilder();
            for(String tag : reservationInfo.getTags()){
                tegBuilder.append(tag);
            }
            tags.setText(tegBuilder.toString());

            sendNotifyArrival.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(TAG, "SEND NOTIFY");
                }
            });
            bookingNote.setText(reservationInfo.getBookingNote());
            minSpendText.setText(Strings.toString(reservationInfo.getMinSpend()));
            bottleMinText.setText(Strings.toString(reservationInfo.getBottleMin()));
            Boolean notifyArrival = (reservationInfo.getNotifyMgmtOnArrival() != null) ?
                    reservationInfo.getNotifyMgmtOnArrival() : Boolean.FALSE;
            sendNotifyArrival.setChecked(notifyArrival);
            notesCount.setText(Strings.toString(reservationInfo.getFeedbackCount()));
            allGirlsCompsEditText.setText(Strings.toString(reservationInfo.getComplimentGirlsQty()));
            allGuysCompsEditText.setText(Strings.toString(reservationInfo.getComplimentGuysQty()));
            allGirlsReducedEditText.setText(Strings.toString(reservationInfo.getReducedGirlsQty()));
            allGuysReducedEditText.setText(Strings.toString(reservationInfo.getReducedGuysQty()));

            setBottleServiceType(reservationInfo.getBottleService());
            setReducedCheckBox();
            setCompsCheckBox();

            completeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getReservationInfo();
                }
            });
        }
    }

    private void setReducedCheckBox(){
        Boolean allGirlsReducedCheck = (reservationInfo.getReducedGirls() != null) ?
                reservationInfo.getReducedGirls() : Boolean.FALSE;
        allGirlsReducedCheckBox.setChecked(allGirlsReducedCheck);
        if(allGirlsReducedCheck){
            enableGirlsReduced();
            enableGirlsComps();
        } else {
            disableGirlsReduced();
        }
        Boolean allGuysReducedCheck = (reservationInfo.getReducedGuys() != null) ?
                reservationInfo.getReducedGuys() : Boolean.FALSE;
        allGuysReducedCheckBox.setChecked(allGuysReducedCheck);
        if(allGuysReducedCheck) {
            enableGuysReduced();
        } else {
            disableGuysReduced();
        }
    }
    private void disableGirlsReduced(){
        allGirlsReducedText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
        allGirlsReducedEditText.setEnabled(false);
        allGirlsReducedEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
    }
    private void enableGirlsReduced(){
        allGirlsReducedText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        allGirlsReducedEditText.setEnabled(true);
        allGirlsReducedEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
    }
    private void disableGuysReduced(){
        allGuysReducedText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
        allGuysReducedEditText.setEnabled(false);
        allGuysReducedEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
    }
    private void enableGuysReduced(){
        allGuysReducedText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        allGuysReducedEditText.setEnabled(true);
        allGuysReducedEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
    }

    private void setCompsCheckBox(){
        Boolean allGirlsCompsCheck = (reservationInfo.getComplimentGirls() != null) ?
                reservationInfo.getComplimentGirls() : Boolean.FALSE;
        allGirlsCompsCheckBox.setChecked(allGirlsCompsCheck);
        if(allGirlsCompsCheck){
            enableGirlsComps();
            enableGirlsReduced();
        } else {
            disableGirlsComps();
        }
        Boolean allGuysCompsCheck = (reservationInfo.getComplimentGuys() != null) ?
                reservationInfo.getComplimentGuys() : Boolean.FALSE;
        allGuysCompsCheckBox.setChecked(allGuysCompsCheck);
        if(allGuysCompsCheck) {
            enableGuysComps();
        } else {
            disableGuysComps();
        }
    }
    private void disableGirlsComps(){
        allGirlsCompsText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
        allGirlsCompsEditText.setEnabled(false);
        allGirlsCompsEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
    }
    private void enableGirlsComps(){
        allGirlsCompsText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        allGirlsCompsEditText.setEnabled(true);
        allGirlsCompsEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
    }
    private void disableGuysComps(){
        allGuysCompsText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
        allGuysCompsEditText.setEnabled(false);
        allGuysCompsEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.tvColor));
    }
    private void enableGuysComps(){
        allGuysCompsText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
        allGuysCompsEditText.setEnabled(true);
        allGuysCompsEditText.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
    }

    private void setBottleServiceType(BottleServiceTypeEnum bottleServiceType) {
        int tableButtonTextColor = ContextCompat.getColor(getActivity(), R.color.greenTextColor);
        int standupButtonTextColor = ContextCompat.getColor(getActivity(), R.color.greenTextColor);
        int barButtonTextColor = ContextCompat.getColor(getActivity(), R.color.greenTextColor);
        if(bottleServiceType != null) {
            switch (bottleServiceType.name()) {
                case "TABLE":
                    tableButtonTextColor = ContextCompat.getColor(getActivity(), R.color.colorWhiteText);
                    tableButton.setChecked(true);
                    standupButton.setChecked(false);
                    barButton.setChecked(false);
                    break;
                case "STANDUP":
                    standupButtonTextColor = ContextCompat.getColor(getActivity(), R.color.colorWhiteText);
                    tableButton.setChecked(false);
                    standupButton.setChecked(true);
                    barButton.setChecked(false);
                    break;
                case "BAR":
                    barButtonTextColor = ContextCompat.getColor(getActivity(), R.color.colorWhiteText);
                    tableButton.setChecked(false);
                    standupButton.setChecked(false);
                    barButton.setChecked(true);
                    break;
                default:
                    tableButton.setChecked(false);
                    standupButton.setChecked(false);
                    barButton.setChecked(false);
            }
        } else {
            tableButton.setChecked(false);
            standupButton.setChecked(false);
            barButton.setChecked(false);
        }
        tableButton.setTextColor(tableButtonTextColor);
        standupButton.setTextColor(standupButtonTextColor);
        barButton.setTextColor(barButtonTextColor);
    }
    private ReservationInfo getReservationInfo() {
        if(reservationInfo != null) {
            VisitorInfo guestInfo = reservationInfo.getGuestInfo();
            if(!guestInformationText.getText().toString().isEmpty()) {
                guestInfo.setFullName(guestInformationText.getText().toString());

                reservationInfo.setGuestInfo(guestInfo);
                reservationInfo.setEventId(getArguments().getLong("eventId"));
                reservationInfo.setVenueId(venueHelper.getVenue().getId());

                if(tableButton.isChecked()) {
                    reservationInfo.setBottleService(BottleServiceTypeEnum.TABLE);
                } else if (standupButton.isChecked()) {
                    reservationInfo.setBottleService(BottleServiceTypeEnum.STANDUP);
                } else if (barButton.isChecked()) {
                    reservationInfo.setBottleService(BottleServiceTypeEnum.BAR);
                }

                DateTime displayedDate = (DateTime) getArguments().getSerializable("displayedDate");
                reservationInfo.setReservationDate(NewReservationFragment.formattedDateText(displayedDate, 1));

                if(!guestCount.getText().toString().isEmpty()) {
                    reservationInfo.setGuestsNumber(Integer.parseInt(guestCount.getText().toString()));
                }
                if(!phoneNumber.getText().toString().isEmpty()) {
                    guestInfo.setPhoneNumber(phoneNumber.getText().toString());
                }
                if(!bookingNote.getText().toString().isEmpty()) {
                    reservationInfo.setBookingNote(bookingNote.getText().toString());
                }
                getLoaderManager().initLoader(R.id.complete_button, null, ReservationsItemView.this);
            }
        }
        return reservationInfo;
    }

    private void setButtonEvent() {
        View.OnClickListener select = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean glLayoutCheck = guestlistRB.isChecked();
                if(!glLayoutCheck) {
                    bsLayout.setVisibility(View.VISIBLE);
                    glLayout.setVisibility(View.GONE);
                } else {
                    bsLayout.setVisibility(View.GONE);
                    glLayout.setVisibility(View.VISIBLE);
                }
            }
        };
        guestlistRB.setOnClickListener(select);
        bottleserviceRB.setOnClickListener(select);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentUtils.changeFragmentContent(fragmentManager, reservationsFragment, R.id.content);
            }
        });
        allGuysCompsCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(allGuysCompsCheckBox.isChecked()) {
                    enableGuysComps();
                    enableGuysReduced();
                } else {
                    disableGuysComps();
                    disableGuysReduced();
                }
            }
        });
        allGirlsCompsCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(allGirlsCompsCheckBox.isChecked()) {
                    enableGirlsComps();
                    enableGirlsReduced();
                } else {
                    disableGirlsComps();
                    disableGuysReduced();
                }
            }
        });
        allGirlsReducedCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(allGirlsReducedCheckBox.isChecked()) {
                    enableGirlsReduced();
                    enableGuysComps();
                } else {
                    disableGirlsReduced();
                    disableGirlsComps();
                }
            }
        });
        allGuysReducedCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(allGuysReducedCheckBox.isChecked()) {
                    enableGuysReduced();
                    enableGuysComps();
                } else {
                    disableGuysReduced();
                    disableGuysComps();
                }
            }
        });
        View.OnClickListener bottleServiceBtns = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton btn = ((ToggleButton) v);
                if(btn.isChecked()) {
                    btn.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhiteText));
                } else {
                    btn.setTextColor(ContextCompat.getColor(getActivity(), R.color.greenTextColor));
                }
            }
        };
        tableButton.setOnClickListener(bottleServiceBtns);
        standupButton.setOnClickListener(bottleServiceBtns);
        barButton.setOnClickListener(bottleServiceBtns);
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.complete_button:
                return new UpdateReservationLoader(getActivity(), reservationInfo);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        switch (loader.getId()) {
            case R.id.complete_button:
                if(data.isSuccess()) {

                    Bundle args = new Bundle();
                    args.putSerializable("eventDate", getArguments().getSerializable("displayedDate"));
                    args.putLong("eventId", getArguments().getLong("eventId"));
                    tablesFragment.setArguments(args);

                    fragmentUtils.changeFragmentContent(fragmentManager, tablesFragment, R.id.content);
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    if (errorMessage != null) {
                        Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    }
                }
                break;
            default:
        }
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }
}
