package ru.dz.clubadmin.ui.widgets;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import ru.dz.clubadmin.ui.fragments.account.MyRolesFragment;
import ru.dz.clubadmin.ui.fragments.account.ProfileDetailsFragment;

/**
 * Created by nikitakulagin on 15.04.16.
 */
public class AccountPageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    public AccountPageAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.mNumOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ProfileDetailsFragment profileFragment = new ProfileDetailsFragment();
                return profileFragment;
            case 1:
                MyRolesFragment rolesFragment = new MyRolesFragment();
                return rolesFragment;
            default:
                ProfileDetailsFragment profileFragment2 = new ProfileDetailsFragment();
                return profileFragment2;        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
