package ru.dz.clubadmin.ui.fragments.venue;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;

import java.util.List;

import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.TagTO;
import ru.dz.clubadmin.loaders.venue.AddVenueTagLoader;
import ru.dz.clubadmin.loaders.venue.DeleteVenueTagLoader;
import ru.dz.clubadmin.loaders.venue.GetVenueTagsLoader;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

/**
 * Created by Sam (samir@peller.tech) on 21.07.2016.
 */
public class VenueTagsFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse>{
    private static final String TAG = "TagFragment";
    private static final int GET_TAGS_INDEX = 0;
    private static final int DELETE_TAG_INDEX = 1;
    private static final int ADD_TAG_INDEX = 2;
    private static final String SUCCESS_RESPONSE_STRING = "Ok";

    @InjectView(R.id.back_to_venue_settings)
    ImageButton backBtn;
    @InjectView(R.id.tags_list_ll)
    FlowLayout tagListLL;
    @InjectView(R.id.complete_button)
    Button addTagButton;
    @InjectView(R.id.tags_edit_text)
    EditText editText;
    @Inject
    FragmentUtils fragmentUtils;
    @Inject
    FragmentManager fragmentManager;
    @Inject
    VenuesSettingsFragment venuesSettingsFragment;
    @Inject
    VenueHelper venueHelper;

    private Long venueId;
    private LinearLayout tagLLToDelete;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tags, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        venueId = (long) venueHelper.getVenue().getId();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentUtils.changeFragmentContent(fragmentManager, venuesSettingsFragment, R.id.content, false);
            }
        });

        addTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!editText.getText().toString().isEmpty()) {
                    getLoaderManager().initLoader(ADD_TAG_INDEX, null, VenueTagsFragment.this);
                }
            }
        });

        getLoaderManager().initLoader(GET_TAGS_INDEX, null, this);
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch(id) {
            case GET_TAGS_INDEX:
                return new GetVenueTagsLoader(getActivity(), venueId);
            case DELETE_TAG_INDEX:
                return new DeleteVenueTagLoader(getActivity(), venueId, args.getString("tagName"));
            case ADD_TAG_INDEX:
                TagTO tagTO = new TagTO();
                tagTO.setTag(editText.getText().toString());
                return new AddVenueTagLoader(getActivity(), venueId, tagTO);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        int id = loader.getId();

        switch(id) {
            case GET_TAGS_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<List<String>> response = data.asSuccess();
                    List<String> tagsList = response.getObj();

                    for(final String tag : tagsList) {
                        final LinearLayout tagLL = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.tag_layout, null);
                        ((TextView) tagLL.getChildAt(0)).setText(tag);
                        tagLL.getChildAt(1).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                tagLLToDelete = tagLL;
                                Bundle args = new Bundle();
                                args.putString("tagName", tag);
                                if(getLoaderManager().getLoader(DELETE_TAG_INDEX) == null) {
                                    getLoaderManager().initLoader(DELETE_TAG_INDEX, args, VenueTagsFragment.this);
                                } else {
                                    getLoaderManager().restartLoader(DELETE_TAG_INDEX, args, VenueTagsFragment.this);
                                }
                            }
                        });

                        tagListLL.addView(tagLL);
                    }
                } else {
                    errorLoadFinish(data);
                }

                break;
            case DELETE_TAG_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<ResponseMessage> response = data.asSuccess();
                    ResponseMessage responseMessage = response.getObj();

                    if(responseMessage != null && responseMessage.getResponse().equals(SUCCESS_RESPONSE_STRING)) {
                        tagLLToDelete.setVisibility(View.GONE);
                    }
                } else {
                    errorLoadFinish(data);
                }

                break;
            case ADD_TAG_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<ResponseMessage> response = data.asSuccess();
                    ResponseMessage responseMessage = response.getObj();

                    if(responseMessage != null && responseMessage.getResponse().equals(SUCCESS_RESPONSE_STRING)) {
                        final LinearLayout tagLL = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.tag_layout, null);
                        final String newTag = editText.getText().toString();
                        ((TextView) tagLL.getChildAt(0)).setText(newTag);
                        tagLL.getChildAt(1).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                tagLLToDelete = tagLL;
                                Bundle args = new Bundle();
                                args.putString("tagName", newTag);
                                if(getLoaderManager().getLoader(DELETE_TAG_INDEX) == null) {
                                    getLoaderManager().initLoader(DELETE_TAG_INDEX, args, VenueTagsFragment.this);
                                } else {
                                    getLoaderManager().restartLoader(DELETE_TAG_INDEX, args, VenueTagsFragment.this);
                                }
                            }
                        });

                        tagListLL.addView(tagLL);
                    }
                } else {
                    errorLoadFinish(data);
                }

                break;
        }

        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }

    private void errorLoadFinish(BaseResponse data) {
        ErrorMessage errorMessage = data.asError().getObj();
        if(errorMessage != null) {
            Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
            showToast(errorMessage.getError());
        }
    }

    private void showToast(String message){
        Toast toast = Toast.makeText(getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }
}
