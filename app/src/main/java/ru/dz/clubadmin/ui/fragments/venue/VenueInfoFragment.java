package ru.dz.clubadmin.ui.fragments.venue;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.database.users.UserInfoHelper;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.UserInfo;
import ru.dz.clubadmin.domain.VenueRequest;
import ru.dz.clubadmin.domain.models.Venue;
import ru.dz.clubadmin.domain.models.VenueRequestStatus;
import ru.dz.clubadmin.domain.models.VenueRole;
import ru.dz.clubadmin.loaders.venue.CreateVenueRequestLoader;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.TRANSPARENT;

public class VenueInfoFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {

    private static final String TAG = "VenueInfoFragment";

    @Inject
    private ApiKeyHelper apiKeyHelper;
    @Inject
    private UserInfoHelper userInfoHelper;
    @Inject
    private FindAddVenueFragment findAddVenueFragment;
    private FragmentManager fragmentManager;

    @Inject
    private FragmentUtils fragmentUtils;
    @Inject
    private VenueListFragment venueListFragment;

    @InjectView(R.id.venue_name)
    private TextView name;
    @InjectView(R.id.address_venue)
    private TextView address;
    @InjectView(R.id.capacity_venue_text)
    private TextView capacity;
    @InjectView(R.id.status_venue_text)
    private TextView status;
    @InjectView(R.id.logo_info_venue)
    private ImageView logo;
    @InjectView(R.id.back_to_venue_list)
    private ImageButton back;
    @InjectView(R.id.delete_button_id)
    ImageButton deleteButton;
    @InjectView(R.id.send_request_button_id)
    private Button sendRequestButton;
    @InjectView(R.id.roles_venue)
    private GridLayout checkBoxLayout;
    @InjectView(R.id.pick_role_text_id)
    private TextView pickRoleInfoView;
    private int yellow = Color.argb(255, 245, 166, 35);
    private int blue = Color.argb(255, 95, 156, 223);
    private int green = Color.argb(255, 95, 164, 14);
    private int gray = Color.argb(255, 246, 246, 246);

    private Venue venue;

    private List<VenueRole> selectedRoles = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_venue_info, null);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        checkBoxLayout.measure(GridLayout.LayoutParams.MATCH_PARENT, GridLayout.LayoutParams.WRAP_CONTENT);
        checkBoxLayout.setPadding(0, 0, 0, sendRequestButton.getLayoutParams().height);

        for (final VenueRole role : VenueRole.values()) {
            Log.i(TAG, role.toString());
            final CheckBox roleButton = new CheckBox(getActivity());
            roleButton.setText(role.toString());
            roleButton.setTextColor(BLACK);

            roleButton.setButtonDrawable(new StateListDrawable());
            GridLayout.LayoutParams params = new GridLayout.LayoutParams();
            params.width = ((LinearLayout) view.getParent()).getWidth() / 2;
            params.height = 90;
            roleButton.setLayoutParams(params);

            roleButton.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            roleButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sendRequestButton.getVisibility() == View.INVISIBLE)
                        sendRequestButton.setVisibility(View.VISIBLE);

                    if (selectedRoles.contains(role)) {
                        roleButton.setBackgroundColor(TRANSPARENT);
                        roleButton.setChecked(false);
                        roleButton.setBackgroundColor(Color.WHITE);
                        selectedRoles.remove(role);
                    } else {
                        roleButton.setChecked(true);
                        selectedRoles.add(role);
                        roleButton.setBackgroundResource(R.drawable.ck_bg);
                    }
                }
            });
            checkBoxLayout.addView(roleButton);
        }

        final Bundle args = getArguments();
        if (args != null) {
            Venue venue = (Venue) args.getSerializable("item");
            this.venue = venue;
            if (venue != null) {
                if (venue.getPreferredRoles() != null) {
                    List<String> roleList = new ArrayList<>();
                    Log.i(TAG, Arrays.toString(venue.getPreferredRoles().toArray()));
                    for (VenueRole role : venue.getPreferredRoles()) {
                        roleList.add(role.toString());
                    }
                    for (int i = 0; i < checkBoxLayout.getChildCount(); i++) {
                        CheckBox child = (CheckBox) checkBoxLayout.getChildAt(i);
                        if (roleList.contains(child.getText())) {
                            child.setChecked(true);
                            child.setBackgroundResource(R.drawable.ck_bg);
                        }
                    }
                }
                name.setText(venue.getName());
                address.setText(venue.getAddress());
                VenueRequestStatus statusText = venue.getRequestStatus();
                capacity.setText(venue.getCapacity().toString());
                if (args.containsKey("from")) {
                    switch (args.getString("from")) {
                        //TODO сделать эту штуку enum
                        case "ADD_VENUE":
                            pickRoleInfoView.setText("Before requesting employee access to this \n venue, please select your desired role/s.");
                            pickRoleInfoView.setTextSize(14);
                            pickRoleInfoView.setBackgroundColor(gray);
                            setButtonClickable(true);
                            fillStatus(status, statusText);
                            deleteButton.setVisibility(View.INVISIBLE);
                            break;
                        case "BROWSE_VENUE":
                            pickRoleInfoView.setText("My Roles");
                            pickRoleInfoView.setTextSize(30);
                            pickRoleInfoView.setBackgroundColor(TRANSPARENT);
                            setButtonClickable(false);
                            fillStatus(status, statusText);
                            break;
                        default:
                            setButtonClickable(false);
                            fillStatus(status, statusText);
                            break;
                    }
                }
                try {
                    fragmentUtils.getLoaderImage(venue.getLogoUrl(),logo);
                } catch (Exception ex) {
                    Log.e(TAG, ex.toString());
                }
            }
        }
        if (sendRequestButton != null) {
            sendRequestButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    status.setText("Requested");
                    status.setTextColor(yellow);
                    sendRequestButton.setVisibility(View.INVISIBLE);
                    pickRoleInfoView.setText("My Roles");
                    pickRoleInfoView.setTextSize(30);
                    pickRoleInfoView.setBackgroundColor(TRANSPARENT);
                    //TODO СДЕЛАТЬ ЗАПРОС НА СЕРВЕР!!!!!
                    VenueRequest request = new VenueRequest();
                    request.setPreferredRoles(selectedRoles);
                    String str = "";
                    for (VenueRole b : selectedRoles) {
                        str = str + b.name() + " ,";
                    }
                    getActivity().getLoaderManager().initLoader(R.id.send_request_loader, null, VenueInfoFragment.this);
                    setButtonClickable(false);
                    showToast("Sending request to \n " + str);
                    Log.i(TAG, "Sending request to \n " + str);
                }
            });
        }
        if (back != null) {
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    assert args != null;
                    switch (args.getString("from")) {
                        case "ADD_VENUE":
                            args.remove("from");
                            findAddVenueFragment.setArguments(args);
                            fragmentUtils.changeFragmentContent(fragmentManager, findAddVenueFragment, R.id.venue_list);
                            break;
                        case "BROWSE_VENUE":
                            args.remove("from");
                            venueListFragment.setArguments(args);
                            fragmentUtils.changeFragmentContent(fragmentManager, venueListFragment, R.id.venue_list);
                            break;
                        default:
                            args.remove("from");
                            findAddVenueFragment.setArguments(args);
                            fragmentUtils.changeFragmentContent(fragmentManager, findAddVenueFragment, R.id.venue_list);
                            break;
                    }
                }
            });
        }
    }

    private void setButtonClickable(Boolean status) {
        for (int i = 0; i < checkBoxLayout.getChildCount(); i++) {
            View child = checkBoxLayout.getChildAt(i);
            child.setEnabled(status);
        }
    }

    private void fillStatus(TextView status, VenueRequestStatus req) {
        if (req != null) {
            switch (req) {
                case REQUESTED:
                    status.setTextColor(yellow);
                    status.setText(req.toString());
                    break;
                case APPROVED:
                    status.setTextColor(green);
                    status.setText(req.toString());
                    break;
                default:
                    status.setText("No Added");
                    break;
            }
        } else {
            status.setText("No Added");
        }
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(this.getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.send_request_loader:
                Log.i(TAG, "Find id loader");
                showToast("Create venue request...");
                UserInfo userInfo = userInfoHelper.getUserInfo();
                VenueRequest venueRequest = new VenueRequest(userInfo.getId(), this.selectedRoles);
                return new CreateVenueRequestLoader(getActivity(), this.venue.getId(), venueRequest);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse baseResponse) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        if (id == R.id.send_request_loader) {

            if (baseResponse.isSuccess()) {
                SuccessResponse<ResponseMessage> resp = baseResponse.asSuccess();
                ResponseMessage responseMessage = resp.getObj();
                if (responseMessage != null) {
                    Log.d(TAG, responseMessage.getResponse());
                    showToast(responseMessage.getResponse());
                }
            } else {
                ErrorMessage errorMessage = baseResponse.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                showToast(errorMessage.getError());
            }
        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }
}
