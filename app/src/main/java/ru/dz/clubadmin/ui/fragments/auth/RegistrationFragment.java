package ru.dz.clubadmin.ui.fragments.auth;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.User;
import ru.dz.clubadmin.loaders.users.CreateUserLoader;
import ru.dz.clubadmin.ui.fragments.signin.EnterCodeFragment;
import ru.dz.clubadmin.ui.fragments.signin.SignInFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;
import ru.dz.clubadmin.ui.utils.MaskedWatcher;


public class RegistrationFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {

    private static final String TAG = "RegistrationFragment";

    private User mUser;

    private FragmentManager fragmentManager;
    @Inject
    private AuthFragment authFragment;
    @Inject
    private SignInFragment signInFragment;
    @Inject
    private EnterCodeFragment enterCodeFragment;

    @InjectView(R.id.signInBtn)
    private Button mSignInBtn;

    @InjectView(R.id.registrationBtn)
    private Button mRegistrationBtn;

    @InjectView(R.id.dateFieldBirth)
    private EditText mDateFieldBirth;
    @InjectView(R.id.textEmailAddress)
    private EditText mEmailTxt;
    @InjectView(R.id.textFiledUserName)
    private EditText mFullNameTxt;
    @InjectView(R.id.textRegistationPhoneNumber)
    private EditText mPhoneNumberTxt;
    @InjectView(R.id.closeBtn)
    private ImageButton closeBtn;

    private Handler handler;

    @Inject
    private FragmentUtils fragmentUtils;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_registration, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setMask();

        fragmentManager = getFragmentManager();
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close(v);
            }
        });

        mSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Clicked!");
                fragmentUtils.changeFragmentContent(fragmentManager, signInFragment, R.id.contentPanel);
            }
        });

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if(msg.what == 2) {
                    Log.d(TAG, "onLoad finished : handler called. setting the fragment.");
                    showEnterCodeFragment();
                }
            }
        };

        mRegistrationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrate();
            }
        });

    }

    private void setMask() {
        mDateFieldBirth.addTextChangedListener(
                new MaskedWatcher("##.##.####")
        );
    }

    private void close(View view) {
        fragmentUtils.changeFragmentContent(fragmentManager, authFragment, R.id.contentPanel);
    }

    private void showEnterCodeFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("phone", mPhoneNumberTxt.getText().toString());
        //set Fragmentclass Arguments
        enterCodeFragment.setArguments(bundle);

        fragmentUtils.changeFragmentContent(fragmentManager, enterCodeFragment, R.id.contentPanel);
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    private void registrate() {
        Log.i(TAG, "Clicked!");
        String phone = mPhoneNumberTxt.getText().toString();
        String fullName = mFullNameTxt.getText().toString();
        String email = mEmailTxt.getText().toString();
        String birth = mDateFieldBirth.getText().toString();

        if (!phone.equalsIgnoreCase("") &&
                !fullName.equalsIgnoreCase("") &&
                !email.equalsIgnoreCase("") &&
                !birth.equalsIgnoreCase("")) {

            User user = new User();
            user.setPhoneNumber(phone);
            user.setEmail(email);
            user.setFullName(fullName);
            try {
                user.setBirthdayDateTime(dateParser(birth));
                this.mUser = user;

                getActivity().getLoaderManager().initLoader(R.id.create_user_loader, null, this);
            } catch (Exception ex) {
                Log.e(TAG, ex.getMessage());
                showToast(ex.getMessage());
            }
        } else {
            Log.e(TAG, "Fields are empty!");
            Toast toast = Toast.makeText(getActivity(),
                    "Fields are empty!", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP, 0, 0);
            toast.show();
        }
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.create_user_loader:
                Log.i(TAG, "Find id loader");
                showToast("Try register...");
                return new CreateUserLoader(getActivity(), mUser);
            default:
                return null;
        }
    }

    private DateTime dateParser(String date) {
        DateTimeParser[] parsers = {
                DateTimeFormat.forPattern("yyyy.MM.dd").getParser(),
                DateTimeFormat.forPattern("yyyy/MM/dd").getParser(),
                DateTimeFormat.forPattern("dd/MM/yyyy").getParser(),
                DateTimeFormat.forPattern("dd.MM.yyyy").getParser(),
                DateTimeFormat.forPattern("dd-MM-yyyy").getParser(),
                DateTimeFormat.forPattern("yyyy-MM-dd").getParser()};
        DateTimeFormatter formatter = new DateTimeFormatterBuilder().append(null, parsers).toFormatter();

        return DateTime.parse(date, formatter).withZone(DateTime.now().getZone());

    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse baseResponse) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        if (id == R.id.create_user_loader) {
            if (baseResponse.isSuccess()) {
                Log.d(TAG, "Success");
                SuccessResponse<ResponseMessage> resp = baseResponse.asSuccess();
                ResponseMessage responseMessage = resp.getObj();
                if (responseMessage != null) {
                    Log.d(TAG, responseMessage.getResponse());
                    showToast(responseMessage.getResponse());
                    handler.sendEmptyMessage(2);
                }
            } else {
                ErrorMessage errorMessage = baseResponse.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                showToast(errorMessage.getError());
            }
        }
        getLoaderManager().destroyLoader(id);

    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }
}
