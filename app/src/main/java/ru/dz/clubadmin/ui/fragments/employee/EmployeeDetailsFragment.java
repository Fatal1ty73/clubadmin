package ru.dz.clubadmin.ui.fragments.employee;

import android.app.FragmentManager;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.google.inject.Inject;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.domain.VenueStaffInfo;
import ru.dz.clubadmin.domain.models.VenueRole;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

import java.util.ArrayList;
import java.util.List;

import static android.graphics.Color.BLACK;

/**
 * Created by nikitakulagin on 15.05.16.
 */
public class EmployeeDetailsFragment extends RoboFragment {
    private static final String TAG = "EmployeeDetailsFragment";
    private FragmentManager fragmentManager;

    @Inject
    private FragmentUtils fragmentUtils;
    private VenueStaffInfo employee;
    private List<String> preferedRoles = new ArrayList<>();
    @InjectView(R.id.photo_employee)
    ImageView employeePhoto;
    @InjectView(R.id.employee_email_id)
    TextView employeeEmail;
    @InjectView(R.id.employee_phone_id)
    TextView employeePhone;
    @InjectView(R.id.employee_name_id)
    TextView employeeName;
    @InjectView(R.id.employee_since_id)
    TextView employeeSince;
    @InjectView(R.id.roles_employee)
    GridLayout roles;
    @InjectView(R.id.accept_employee_id)
    private Button acceptEmployee;
    @InjectView(R.id.decline_employee_id)
    private Button declineEmployee;
    @InjectView(R.id.back_to_employee_list)
    ImageButton backButton;
    @Inject
    EmployeeFragment employeeFragment;
    private DateTimeFormatter formatter = DateTimeFormat.forPattern("MMM dd, yyyy");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_employee_details, null);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        roles.measure(GridLayout.LayoutParams.MATCH_PARENT, GridLayout.LayoutParams.WRAP_CONTENT);
        roles.setPadding(0, 0, 0, acceptEmployee.getLayoutParams().height);
        for (final VenueRole role : VenueRole.values()) {
            final CheckBox roleButton = new CheckBox(getActivity());
            roleButton.setText(role.toString());
            roleButton.setTextColor(BLACK);

            roleButton.setButtonDrawable(new StateListDrawable());
            GridLayout.LayoutParams params = new GridLayout.LayoutParams();
            params.width = ((FrameLayout) view.getParent()).getWidth() / 2;
            params.height = 90;
            roleButton.setLayoutParams(params);

            roleButton.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            roles.addView(roleButton);
        }
        final Bundle args = getArguments();
        employee = (VenueStaffInfo) args.getSerializable("employee");
        switch (args.getString("status")) {
            case "current":
                acceptEmployee.setVisibility(View.INVISIBLE);
                declineEmployee.setVisibility(View.INVISIBLE);
                break;
            case "requested":
                acceptEmployee.setVisibility(View.VISIBLE);
                declineEmployee.setVisibility(View.VISIBLE);
        }
        if (employee.getEmail()!=null) {
            employeeEmail.setText(Html.fromHtml("Email " + "<b><font color=#ffffff>" + employee.getEmail() + "</font></b>"));
        }
        employeePhone.setText(Html.fromHtml("Phone " + "<b><font color=#ffffff>" + employee.getPhoneNumber() + "</font></b>"));
        employeeSince.setText("Employee Since: " + employee.getSince().toString(formatter));
        employeeName.setText(employee.getFullName());
        if(backButton != null) {
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    employeeFragment.setArguments(args);
                    fragmentUtils.changeFragmentContent(fragmentManager, employeeFragment, R.id.content);
                }
            });
        }
        if (employee.getPreferredRoles()!=null) {
            for (VenueRole role: employee.getPreferredRoles()) {
                preferedRoles.add(role.toString());
            }
            for (int i = 0; i < roles.getChildCount(); i++) {
                CheckBox checkBox = (CheckBox) roles.getChildAt(i);
                if (preferedRoles.contains(checkBox.getText())) {
                    checkBox.setBackgroundResource(R.drawable.ck_bg);
                    checkBox.setChecked(true);
                }
            }
        }

    }
}
