package ru.dz.clubadmin.ui.fragments.events;


import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.domain.models.EventInfo;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

import java.io.Serializable;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditEventFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {

    private static final String TAG = "EditEventFragment";
    @Inject
    FragmentUtils fragmentUtils;
    private FragmentManager fragmentManager;

    @Inject
    private EventsFragment eventsFragment;

    @InjectView(R.id.edit_event_back_btn)
    ImageButton backBtn;
    @InjectView(R.id.edit_event_img)
    ImageView coverEventImg;
    @InjectView(R.id.edit_event_time_tv)
    TextView eventTimeTv;
    @InjectView(R.id.edit_event_name_txt)
    EditText eventNameTxt;
    @InjectView(R.id.edit_event_description_txt)
    EditText eventDescriptionTxt;
    @InjectView(R.id.edit_event_fb_url_txt)
    EditText eventFbUrlTxt;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_event, container, false);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        Serializable serializable = getArguments().getSerializable("event");
        if(serializable != null) {
            EventInfo eventInfo = (EventInfo) serializable ;
            fillEventFields(eventInfo);
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentUtils.changeFragmentContent(fragmentManager, eventsFragment, R.id.content);
            }
        });

    }

    private void fillEventFields(EventInfo eventInfo){
        eventTimeTv.setText((eventInfo.getStartsAt() != null) ? eventInfo.getStartsAt() : "");
        eventNameTxt.setText((eventInfo.getName() != null) ? eventInfo.getName() : "");
        eventDescriptionTxt.setText((eventInfo.getDescription() != null) ? eventInfo.getDescription() : "");
        eventFbUrlTxt.setText((eventInfo.getFbEventUrl() != null) ? eventInfo.getFbEventUrl() : "");
        if (eventInfo.getPictureUrl() != null && !eventInfo.getPictureUrl().isEmpty()) {
            try {
                fragmentUtils.getLoaderImage(eventInfo.getPictureUrl(),coverEventImg);
            } catch (Exception ex) {
                if(ex.getMessage() != null)
                    Log.e(TAG, ex.getMessage());
            }
        } else {
            coverEventImg.setImageResource(R.drawable.logo_venue);
        }
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int i, Bundle bundle) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse baseResponse) {

    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }
}
