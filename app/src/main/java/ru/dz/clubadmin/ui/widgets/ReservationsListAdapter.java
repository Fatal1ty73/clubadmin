package ru.dz.clubadmin.ui.widgets;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import ru.dz.clubadmin.R;

import java.util.List;

public abstract class ReservationsListAdapter<T> extends ArrayAdapter<T> {

    private LayoutInflater mInflater;

    @LayoutRes
    private int resource;
    private List<T> list;

    public ReservationsListAdapter(Context context, @LayoutRes int resource, List<T> list) {
        super(context, resource, list);
        this.resource = resource;
        this.list = list;

        mInflater = LayoutInflater.from(context);
    }

    public int getSize() {
        return list.size();
    }
    @Override
    public View getView(int position, View convertView,
                        ViewGroup parent) {
        ViewHolder holder;
        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(resource, parent, false);
            holder = new ViewHolder();
            holder.icon = (ImageView) row.findViewById(R.id.reservations_item_img);
            holder.titleVenue = (TextView) row.findViewById(R.id.reservations_item_title);
            holder.description = (TextView) row.findViewById(R.id.reservations_item_desc);
            holder.notes = (TextView) row.findViewById(R.id.reservations_item_text);
            holder.complimentQty = (TextView) row.findViewById(R.id.reservations_complimentQty_text);
            holder.reducedQty = (TextView) row.findViewById(R.id.reservations_reducedQty_text);
            holder.feedbackCount = (TextView) row.findViewById(R.id.reservations_feedbackCount_text);
            holder.body = (RelativeLayout) row.findViewById(R.id.main);

            switch (resource) {
                case R.layout.fragment_reservations_item_other:
                    holder.buttonTop = (ImageButton) row.findViewById(R.id.reservation_decrement);
                    holder.buttonButtom = (ImageButton) row.findViewById(R.id.reservation_increment);
                    break;
                case R.layout.fragment_reservations_item_pending:
                    holder.buttonTop = (ImageButton) row.findViewById(R.id.reservation_no);
                    holder.buttonButtom = (ImageButton) row.findViewById(R.id.reservation_yes);
                    break;
            }

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        T item = this.list.get(position);
        fillItem(item, holder);
        return row;
    }

    public abstract void fillItem(T item, ViewHolder holder);

    public class ViewHolder {
        public ImageView icon;
        public TextView titleVenue, description, notes, complimentQty, reducedQty, feedbackCount;
        public ImageButton buttonTop, buttonButtom;
        public RelativeLayout body;
    }

}
