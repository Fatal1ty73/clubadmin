package ru.dz.clubadmin.ui.widgets;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import ru.dz.clubadmin.R;

import java.util.List;

/**
 * Created by nikitakulagin on 06.04.16.
 */
public abstract class FindVenueAdapter<T> extends ArrayAdapter<T> {
    private LayoutInflater mInflater;

    @LayoutRes
    private int resource;
    private List<T> list;

    public FindVenueAdapter(Context context, @LayoutRes int resource, List<T> list) {
        super(context, resource, list);
        this.resource = resource;
        this.list = list;

        mInflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {
        FindViewHolder holder;
        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(resource, parent, false);
            holder = new FindViewHolder();
            holder.logoVenue = (ImageView) row.findViewById(R.id.find_add_logo);
            holder.titleVenue = (TextView) row.findViewById(R.id.find_add_venue_name);
            holder.addressVenue = (TextView) row.findViewById(R.id.find_add_address_venue);
            holder.statusVenue = (TextView) row.findViewById(R.id.find_add_status_venue);
            holder.contentItem = (RelativeLayout) row.findViewById(R.id.add_item_venue_id);
            row.setTag(holder);
        } else {
            holder = (FindViewHolder) row.getTag();
        }
        T item = this.list.get(position);
        fillItem(item, holder);
        return row;
    }

    public abstract void fillItem(T item, FindViewHolder holder);

    public class FindViewHolder {
        public ImageView logoVenue;
        public TextView titleVenue, addressVenue, statusVenue;
        public RelativeLayout contentItem;
    }
}
