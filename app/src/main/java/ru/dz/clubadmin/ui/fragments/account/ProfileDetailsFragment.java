package ru.dz.clubadmin.ui.fragments.account;


import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.database.users.UserInfoHelper;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.UserInfo;
import ru.dz.clubadmin.domain.models.Promoter;
import ru.dz.clubadmin.loaders.users.PromotersLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
//public class ProfileDetailsFragment extends RoboFragment {
public class ProfileDetailsFragment extends RoboFragment  implements LoaderManager.LoaderCallbacks<BaseResponse> {
    @InjectView(R.id.name_family)
    private TextView nameFamily;
    @InjectView(R.id.birth_date)
    private TextView birthDate;
    @InjectView(R.id.phone_number)
    private TextView phoneNumber;
    @InjectView(R.id.email)
    private TextView email;
    @InjectView(R.id.promoter_spinner_id)
    private Spinner promoterSpinner;
    @Inject
    private UserInfoHelper userInfoHelper;
    private static final String TAG = "ProfileDetailsFragment";

    private String apiKey;
    @Inject
    private ApiKeyHelper apiKeyHelper;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile_details, null);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final UserInfo user = userInfoHelper.getUserInfo();
        this.apiKey = apiKeyHelper.getApiKey().getApiKey();
        if (this.apiKey != null) {
            getLoaderManager().initLoader(R.id.promoter_loader, null, this);
        }
        if (nameFamily!=null) {
            nameFamily.setText(user.getFullName());
        }


        birthDate.setText(user.getBirthday());
        phoneNumber.setText(user.getPhoneNumber());
        email.setText(user.getEmail());
    }
    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "Start loader");
        switch (id) {
            case R.id.promoter_loader:
                Log.i(TAG, "Find id loader1");
                //TODO Заменить venueId на правильный
                Long venueId = 2L;
                return new PromotersLoader(this.getActivity(), venueId);
            default:
                return null;
        }
    }
    private void loadAdapter(List<Promoter> promoters) {
        List<String> arraySpinner = new ArrayList<>();
        for (Promoter prom: promoters) {
            arraySpinner.add(prom.getFullName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getActivity(),
                R.layout.dropbox_item, arraySpinner);
        adapter.setDropDownViewResource(R.layout.dropdown_spinner_item);
        promoterSpinner.setAdapter(adapter);
    }
    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse baseResponse) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        if (id == R.id.promoter_loader) {
            if (baseResponse.isSuccess()) {
                SuccessResponse<List<Promoter>> resp = baseResponse.asSuccess();
                List<Promoter> responseMessage = resp.getObj();
                if (responseMessage != null) {
                    Log.d(TAG, responseMessage.get(0).getFullName());
                    loadAdapter(responseMessage);
                }
            } else {
                ErrorMessage errorMessage = baseResponse.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
            }
        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }
}
