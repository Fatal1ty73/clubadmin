package ru.dz.clubadmin.ui.fragments.venue;

import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import roboguice.util.Strings;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.database.users.UserInfoHelper;
import ru.dz.clubadmin.domain.UserInfo;
import ru.dz.clubadmin.domain.models.Venue;
import ru.dz.clubadmin.ui.fragments.employee.EmployeeFragment;
import ru.dz.clubadmin.ui.fragments.events.EventsFragment;
import ru.dz.clubadmin.ui.fragments.menu.ClickerFragment;
import ru.dz.clubadmin.ui.fragments.promo.PromotionsFragment;
import ru.dz.clubadmin.ui.fragments.reservations.ReservationsFragment;
import ru.dz.clubadmin.ui.fragments.schedule.MyScheduleFragment;
import ru.dz.clubadmin.ui.fragments.tables.TablesFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

public class MenuVenueFragment extends RoboFragment {

    private final String TAG = this.getClass().getSimpleName();

    @Inject
    FragmentUtils fragmentUtils;
    @Inject
    private VenuesSettingsFragment venuesSettingsFragment;
    @Inject
    private ReservationsFragment reservationsFragment;
    @Inject
    private MyScheduleFragment myScheduleFragment;
    @Inject
    private EventsFragment eventsFragment;
    @Inject
    private TablesFragment tablesFragment;
    @Inject
    private PromotionsFragment promotionsFragment;
    @Inject
    private EmployeeFragment employeeFragment;
    @Inject
    private ClickerFragment clickerFragment;
    @Inject
    private VenueListFragment myVenuesFragment;
    @InjectView(R.id.drawer_layout)
    private DrawerLayout drawerLayout;
    @InjectView(R.id.main_menu)
    private NavigationView navigationView;
    @Inject
    private UserInfoHelper userInfoHelper;
    @Inject
    private VenueListFragment venueListFragment;
    private FragmentManager fragmentManager;

    private Fragment currentFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu_venue, container, false);
    }


    private void changeContent(Integer item) {
        if(currentFragment != null){
            fragmentUtils.detachFragment(fragmentManager, currentFragment);
        }
        switch (item) {
            case R.id.nav_reservations:
                currentFragment = reservationsFragment;
                fragmentUtils.changeFragmentContent(fragmentManager, reservationsFragment, R.id.content);
                break;
            case R.id.nav_clicker:
                currentFragment = clickerFragment;
                fragmentUtils.changeFragmentContent(fragmentManager, clickerFragment, R.id.content);
                break;
            case R.id.nav_employee:
                currentFragment = employeeFragment;
                fragmentUtils.changeFragmentContent(fragmentManager, employeeFragment, R.id.content);
                break;
            case R.id.nav_schedule:
                currentFragment = myScheduleFragment;
                fragmentUtils.changeFragmentContent(fragmentManager, myScheduleFragment, R.id.content);
                break;
            case R.id.nav_events:
                currentFragment = eventsFragment;
                fragmentUtils.changeFragmentContent(fragmentManager, eventsFragment, R.id.content);
                break;
            case R.id.nav_tables:
                currentFragment = tablesFragment;
                fragmentUtils.changeFragmentContent(fragmentManager, tablesFragment, R.id.content);
                break;
            case R.id.nav_promotions:
                currentFragment = promotionsFragment;
                fragmentUtils.changeFragmentContent(fragmentManager, promotionsFragment, R.id.content);
                break;
            case R.id.nav_reports:
                break;
            default:
                Log.i(TAG, "UserMenuItem is not defined "+ item);
                break;
        }
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Bundle args = getArguments();

        fragmentManager = getChildFragmentManager();
        Integer menuItem = args.getInt("menu");
        clickerFragment.setArguments(getArguments());
        employeeFragment.setArguments(getArguments());
        if(menuItem > 0 && args.getSerializable("venue") != null) {
            changeContent(menuItem);
            final UserInfo user = userInfoHelper.getUserInfo();
            reservationsFragment.setArguments(args);
            RelativeLayout myVenuesButton = ((RelativeLayout) navigationView.getHeaderView(0).findViewById(R.id.my_venues_relative_button));
            TextView myRole = ((TextView) navigationView.getHeaderView(0).findViewById(R.id.role_text_id));
            TextView myName = ((TextView) navigationView.getHeaderView(0).findViewById(R.id.name_family_text_id));
            TextView venueName = ((TextView) navigationView.getHeaderView(0).findViewById(R.id.menu_venue_tv));
            ImageButton myPhoto = ((ImageButton) navigationView.getHeaderView(0).findViewById(R.id.photo_id));
            ImageButton settings = ((ImageButton) navigationView.getHeaderView(0).findViewById(R.id.venue_settings_btn));

            Venue venue = (Venue) args.getSerializable("venue");
            venueName.setText(venue != null ? venue.getName() : "No name");


            myName.setText(user.getFullName());
            myVenuesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentUtils.changeFragmentContent(fragmentManager, myVenuesFragment, R.id.venue_list, false);
                }
            });
            try {
                fragmentUtils.getLoaderImage(user.getUserpic(),myPhoto);
            } catch (Exception ex) {
                Log.e(TAG, ex.toString());
            }


            if(user.getPreferredRoles().size() > 3) {
                String roles = user.getPreferredRoles().get(0).toString() + ", " + user.getPreferredRoles().get(1).toString() + ", " + user.getPreferredRoles().get(2).toString() +
                        " and " + String.valueOf(user.getPreferredRoles().size() - 1) + " more roles";
                myRole.setText(roles);
            } else {
                myRole.setText(Strings.join(", ", user.getPreferredRoles()));
            }

            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {
                    menuItem.setChecked(true);

                    changeContent(menuItem.getItemId());
                    drawerLayout.closeDrawers();
                    return true;
                }
            });

            settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(currentFragment != null){
                        fragmentUtils.detachFragment(fragmentManager, currentFragment);
                    }
                    currentFragment = venuesSettingsFragment;
                    fragmentUtils.changeFragmentContent(fragmentManager, venuesSettingsFragment, R.id.content);
                    drawerLayout.closeDrawers();
                }
            });
        } else {
            Log.i(TAG, "UserMenuItem is empty");
        }
    }
}
