package ru.dz.clubadmin.ui.fragments.reservations;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.loaders.venue.GetVenueTagsLoader;
import ru.dz.clubadmin.ui.fragments.venue.FlowLayout;

/**
 * Created by Sam (samir@peller.tech) on 25.07.2016.
 */
public class AddTagFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {
    private static final String TAG = "AddTagFragment";
    private static final int GET_VENUE_TAGS_INDEX = 0;

    @InjectView(R.id.tags_list_ll)
    FlowLayout flowLayout;
    @Inject
    VenueHelper venueHelper;

    private List<String> tags;
    private Long venueId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_tag, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        venueId = venueHelper.getVenue().getId();

        if(getArguments() != null && getArguments().containsKey("tags")) {
            tags = new ArrayList<>();
            tags.addAll(getArguments().getStringArrayList("tags"));

            for(String tag : tags) {
                ToggleButton tagTB = new ToggleButton(getActivity());
                tagTB.setTextOn(tag);
                tagTB.setTextOff(tag);
                tagTB.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_tag_choose));
                tagTB.setChecked(true);

                tagTB.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(((ToggleButton)v).isChecked()) {
                            ((ToggleButton) v).setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhiteText));
                        } else {
                            ((ToggleButton) v).setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
                        }
                    }
                });

                flowLayout.addView(tagTB);
            }
        }

        getLoaderManager().initLoader(GET_VENUE_TAGS_INDEX, null, this);
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch(id) {
            case GET_VENUE_TAGS_INDEX:
                return new GetVenueTagsLoader(getActivity(), venueId);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        int id = loader.getId();
        switch(id) {
            case GET_VENUE_TAGS_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<List<String>> response = data.asSuccess();
                    List<String> tagsList = response.getObj();

                    for(final String tag : tagsList) {
                        if(tags != null && !tags.contains(tag)) {
                            ToggleButton tagTB = new ToggleButton(getActivity());
                            tagTB.setTextOn(tag);
                            tagTB.setTextOff(tag);
                            tagTB.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_tag_choose));
                            tagTB.setChecked(false);
                            tagTB.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));

                            tagTB.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (((ToggleButton) v).isChecked()) {
                                        ((ToggleButton) v).setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhiteText));
                                    } else {
                                        ((ToggleButton) v).setTextColor(ContextCompat.getColor(getActivity(), R.color.dark));
                                    }
                                }
                            });

                            flowLayout.addView(tagTB);
                        }
                    }
                } else {
                    errorLoadFinish(data);
                }

                break;
            default:
                break;
        }

        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }

    private void errorLoadFinish(BaseResponse data) {
        ErrorMessage errorMessage = data.asError().getObj();
        if(errorMessage != null) {
            Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
            showToast(errorMessage.getError());
        }
    }

    private void showToast(String message){
        Toast toast = Toast.makeText(getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }
}
