package ru.dz.clubadmin.ui.fragments.tables;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.ApiKey;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.EventsList;
import ru.dz.clubadmin.domain.NextDate;
import ru.dz.clubadmin.domain.ReservationInfo;
import ru.dz.clubadmin.domain.ReservationsLists;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.TableInfo;
import ru.dz.clubadmin.domain.models.EventInfo;
import ru.dz.clubadmin.loaders.events.EventsLoader;
import ru.dz.clubadmin.loaders.reservation.AssignTableLoader;
import ru.dz.clubadmin.loaders.reservation.ReservationListLoader;
import ru.dz.clubadmin.loaders.reservation.ReservationsBsLoader;
import ru.dz.clubadmin.loaders.venue.NextEventLoader;
import ru.dz.clubadmin.loaders.venue.PrevEventLoader;
import ru.dz.clubadmin.ui.utils.FragmentUtils;
import ru.dz.clubadmin.ui.widgets.ApprovedReservationListAdapter;
import ru.dz.clubadmin.ui.widgets.EventItemAdapter;

/**
 * Created by Sam (samir@peller.tech) on 24.06.2016.
 */
public class ReservationListFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {
    private static final String TAG = "ReservationListFragment";
    private static final int GET_RESERVATIONS_LIST_INDEX = 0;
    private static final int ASSIGN_TABLE_INDEX = 1;
    private static final int GET_NEXT_EVENT_DAY_INDEX = 2;
    private static final int GET_DATE_EVENT_COUNT_INDEX = 3;
    private static final int GET_PREV_EVENT_DAY_INDEX = 4;
    private static final int DISPLAYED_DATE_TYPE  = 0;
    private static final int REQUEST_DATE_TYPE  = 1;
    private String apiKey;
    private Long venueId;
    private DateTime displayedDate;

    @Inject
    private ApiKeyHelper apiKeyHelper;
    @Inject
    private VenueHelper venueHelper;
    @Inject
    FragmentUtils fragmentUtils;
    @Inject
    private FragmentManager fragmentManager;
    @InjectView(R.id.reservations_listview)
    private ListView reservationsList;
    @Inject
    TablesFragment tablesFragment;
    @InjectView(R.id.reservations_date_next)
    private ImageButton nextDate;
    @InjectView(R.id.reservations_date_prev)
    private ImageButton prevDate;
    @InjectView(R.id.reservations_date)
    private TextView dateText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reservations, container, false);
    }

    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        venueId = venueHelper.getVenue().getId();
        fragmentManager = getFragmentManager();

        nextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLoaderManager().initLoader(GET_NEXT_EVENT_DAY_INDEX, null, ReservationListFragment.this);
            }
        });

        prevDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLoaderManager().initLoader(GET_PREV_EVENT_DAY_INDEX, null, ReservationListFragment.this);
            }
        });

        startLoader();
    }

    private void startLoader(){
        ApiKey key = apiKeyHelper.getApiKey();
        if(key.getApiKey() != null && !key.getApiKey().isEmpty()) {
            Log.i(TAG, "Key not empty");
            this.apiKey = key.getApiKey();

            Bundle args = new Bundle();
            displayedDate = (DateTime) getArguments().getSerializable("displayedDate") ; //"yyyy-MM-dd"
            args.putString("displayedDate", displayedDate.toString("yyyy-MM-dd"));
            args.putString("bsType", getArguments().getString("bsType"));
            args.putLong("eventId", getArguments().getLong("eventId"));
            getActivity().getLoaderManager().initLoader(GET_RESERVATIONS_LIST_INDEX, args, this);
        } else {
            Log.i(TAG, "Key is empty");
        }
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "Find id loader");
        showToast("Wait, try send...");
        switch(id) {
            case GET_RESERVATIONS_LIST_INDEX:
                return new ReservationsBsLoader(getActivity(), String.valueOf(venueId), args.getString("displayedDate"), args.getString("bsType"), args.getLong("eventId"));
            case ASSIGN_TABLE_INDEX:
                return new AssignTableLoader(getActivity(), args.getLong("reservationId"), (TableInfo) getArguments().getSerializable("tableInfo"));
            case GET_NEXT_EVENT_DAY_INDEX:
                showToast("Wait, try send...");
                return new NextEventLoader(getActivity(), venueId, formattedDateText(displayedDate, REQUEST_DATE_TYPE));
            case GET_PREV_EVENT_DAY_INDEX:
                showToast("Wait, try send...");
                return new PrevEventLoader(getActivity(), venueId, formattedDateText(displayedDate, REQUEST_DATE_TYPE));
            case GET_DATE_EVENT_COUNT_INDEX:
                showToast("Wait, try send...");
                return new EventsLoader(getActivity(), displayedDate, venueId);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();

        switch (id) {
            case GET_RESERVATIONS_LIST_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<List<ReservationInfo>> resp = data.asSuccess();
                    List<ReservationInfo> reservationListMessage = resp.getObj();

                    dateText.setText(formattedDateText(displayedDate, DISPLAYED_DATE_TYPE));
                    if(reservationListMessage != null && reservationListMessage.size() > 0) {
                        createAdapter(reservationListMessage);
                    } else {
                        createAdapter(new ArrayList<ReservationInfo>());
                    }
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
            case ASSIGN_TABLE_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<ResponseMessage> resp = data.asSuccess();
                    ResponseMessage response = resp.getObj();

                    Bundle args = new Bundle();
                    args.putLong("eventId", getArguments().getLong("eventId"));
                    args.putSerializable("eventDate", getArguments().getSerializable("displayedDate"));
                    tablesFragment.setArguments(args);
                    fragmentUtils.changeFragmentContent(fragmentManager, tablesFragment, R.id.content, false);
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
            case GET_NEXT_EVENT_DAY_INDEX:
            case GET_PREV_EVENT_DAY_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<NextDate> response = data.asSuccess();
                    NextDate nextDate = response.getObj();

                    displayedDate = DateTime.parse(nextDate.getDate());
                    dateText.setText(formattedDateText(displayedDate, DISPLAYED_DATE_TYPE));
                    getLoaderManager().initLoader(GET_DATE_EVENT_COUNT_INDEX, null, this);
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
            case GET_DATE_EVENT_COUNT_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<EventsList> resp = data.asSuccess();
                    EventsList eventsListMessage = resp.getObj();
                    if (eventsListMessage != null) {
                        Bundle args;
                        switch (eventsListMessage.getEventsList().size()) {
                            case 1:
                                args = new Bundle();
                                args.putString("displayedDate", displayedDate.toString("yyyy-MM-dd"));
                                args.putLong("eventId", eventsListMessage.getEventsList().get(0).getId());
                                getActivity().getLoaderManager().restartLoader(GET_RESERVATIONS_LIST_INDEX, args, this);

                                break;
                            //case size 0, > 1
                            default:
                                createEventAdapter(eventsListMessage.getEventsList());

                                break;
                        }
                    }
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
        }

        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }

    private void createAdapter(List<ReservationInfo> list) {
        ApprovedReservationListAdapter<ReservationInfo> adapter = new ApprovedReservationListAdapter<ReservationInfo>(this.getActivity(),
                R.layout.reservation_list_item, list) {
            @Override
            public void fillItem(final ReservationInfo item, FindViewHolder holder) {
                if(holder != null) {
                    if (item.getGuestInfo().getUserpic() != null && !item.getGuestInfo().getUserpic().isEmpty()) {
                        try {
                            fragmentUtils.getLoaderImage(item.getGuestInfo().getUserpic(),holder.reserveImage);
                        } catch (Exception ex) {
                            if (ex.getMessage() != null)
                                Log.e(TAG, ex.getMessage());
                        }
                    } else {
                        holder.reserveImage.setImageResource(R.drawable.logo_venue);
                    }

                    holder.reserveName.setText(item.getGuestInfo().getFullName());
                    String byText = "by " + item.getBookedBy().getFullName();
                    holder.reservedBy.setText(byText);
                    holder.reserveNote.setText(item.getBookingNote());
                    if(item.getTotalGuests() > 1) {
                        String guestText = "+  " + (item.getTotalGuests() - 1) + " Guests";
                        holder.reserveGuests.setText(guestText);
                    }

                    holder.assign.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle args = new Bundle();
                            args.putLong("reservationId", item.getId());
                            if(getActivity().getLoaderManager().getLoader(ASSIGN_TABLE_INDEX) == null) {
                                getActivity().getLoaderManager().initLoader(ASSIGN_TABLE_INDEX, args, ReservationListFragment.this);
                            } else {
                                getActivity().getLoaderManager().restartLoader(ASSIGN_TABLE_INDEX, args, ReservationListFragment.this);
                            }
                        }
                    });
                }
            }
        };
        adapter.notifyDataSetChanged();

        reservationsList.setAdapter(adapter);
    }

    private void createEventAdapter(final List<EventInfo> eventList) {
        final EventItemAdapter<EventInfo> adapter = new EventItemAdapter<EventInfo>(this.getActivity(),
                R.layout.event_item_adapter, eventList) {
            @Override
            public void fillItem(final EventInfo item, FindViewHolder holder) {
                holder.deleteEvent.setVisibility(View.INVISIBLE);
                holder.eventName.setText(item.getName());
                holder.eventDate.setText("Start at " + item.getStartsAt());

                if (item.getPictureUrl() != null && !item.getPictureUrl().isEmpty()) {
                    try {
                        fragmentUtils.getLoaderImage(item.getPictureUrl(),holder.venueItemImg);
                    } catch (Exception ex) {
                        if(ex.getMessage() != null)
                            Log.e(TAG, ex.getMessage());
                    }
                } else {
                    holder.venueItemImg.setImageResource(R.drawable.logo_venue);
                }

                holder.eventItemLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle args = new Bundle();
                        args.putString("displayedDate", displayedDate.toString("yyyy-MM-dd"));
                        args.putLong("eventId", item.getId());
                        getActivity().getLoaderManager().restartLoader(GET_RESERVATIONS_LIST_INDEX, args, ReservationListFragment.this);
                    }
                });
            }
        };
        adapter.notifyDataSetChanged();

        reservationsList.setAdapter(adapter);
    }

    private String formattedDateText(DateTime date, int format) {
        if(date != null) {
            DateTimeFormatter dateFormat;

            switch(format) {
                case 0:
                    dateFormat = DateTimeFormat.forPattern("EEEE MMM d, yyyy");
                    break;
                case 1:
                    dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd");
                    break;
                default:
                    return "";
            }

            return dateFormat.print(date);
        } else {
            return "";
        }
    }

    private void showToast(String message){
        Toast toast = Toast.makeText(getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }
}
