package ru.dz.clubadmin.ui.widgets;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import ru.dz.clubadmin.R;

/**
 * Created by Sam (samir@peller.tech) on 10.06.2016.
 */
public abstract class StaffListAdapter<T> extends ArrayAdapter<T> {
    private LayoutInflater mInflater;

    @LayoutRes
    private int resource;
    private List<T> list;

    public StaffListAdapter(Context context, @LayoutRes int resource, List<T> list) {
        super(context, resource, list);
        this.resource = resource;
        this.list = list;

        mInflater = LayoutInflater.from(context);
    }

    public View getView(int position, android.view.View convertView,
                        ViewGroup parent) {
        FindViewHolder holder;
        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(resource, parent, false);
            holder = new FindViewHolder();
            holder.staffPerson = (TextView) row.findViewById(R.id.staff_person);
            holder.removeStaffBtn = (ImageButton) row.findViewById(R.id.remove_staff_btn);
            row.setTag(holder);
        } else {
            holder = (FindViewHolder) row.getTag();
        }
        T item = this.list.get(position);
        fillItem(item, holder);
        return row;
    }

    public abstract void fillItem(T item, FindViewHolder holder);

    public class FindViewHolder {
        public TextView staffPerson;
        public ImageButton removeStaffBtn;
    }
}
