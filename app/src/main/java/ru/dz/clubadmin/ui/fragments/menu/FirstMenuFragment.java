package ru.dz.clubadmin.ui.fragments.menu;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.domain.models.Venue;
import ru.dz.clubadmin.ui.fragments.venue.MenuVenueFragment;
import ru.dz.clubadmin.ui.fragments.venue.VenueListFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

public class FirstMenuFragment extends RoboFragment {

    private final String TAG = this.getClass().getSimpleName();

    @Inject
    FragmentUtils fragmentUtils;

    @InjectView(R.id.first_menu)
    NavigationView navigationView;
    @Inject
    private VenueListFragment venueListFragment;
    @Inject
    MenuVenueFragment menuVenueFragment;
    @InjectView(R.id.back_to_venue_list_from_first_menu)
    ImageButton back;
    @InjectView(R.id.title_venue)
    TextView venueName;
    private FragmentManager fragmentManager;
//    @Inject
//    ClickerFragment clickerFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_first_menu, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Bundle args = getArguments();
        fragmentManager = getFragmentManager();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(android.view.MenuItem menuItem) {
                menuItem.setChecked(true);
                args.putInt("menu", menuItem.getItemId());

                switch (menuItem.getItemId()) {
                    case R.id.nav_reservations:
                        changeFragment(args);
                        break;
                    case R.id.nav_clicker:
                        changeFragment(args);
                        break;
                    case R.id.nav_employee:
                        changeFragment(args);
                        break;
                    case R.id.nav_schedule:
                        Log.i(TAG, "MY_SCHEDULE open");
                        changeFragment(args);
                        break;
                    case R.id.nav_events:
                        Log.i(TAG, "EVENTS open");
                        changeFragment(args);
                        break;
                    case R.id.nav_tables:
                        Log.i(TAG, "TABLES open");
                        changeFragment(args);
                        break;
                    case R.id.nav_promotions:
                        Log.i(TAG, "Promotions open");
                        changeFragment(args);
                        break;
                    case R.id.nav_reports:
                        Log.i(TAG, "REPORTS open");
                        break;
                    default:
                        Log.i(TAG, "Menu not fund");
                }
                return true;
            }
        });
        if (args != null) {
            Venue item = (Venue) args.getSerializable("venue");
            if (venueName != null && item != null) {
                venueName.setText(String.valueOf(item.getName()));
            }
        }
        if (back != null) {
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    venueListFragment.setArguments(args);
                    fragmentUtils.changeFragmentContent(fragmentManager, venueListFragment, R.id.venue_list);

                    }
                });
        }
    }

    private void changeFragment(Bundle args) {
        menuVenueFragment.setArguments(args);
        fragmentUtils.changeFragmentContent(fragmentManager, menuVenueFragment, R.id.venue_list);

    }
}
