package ru.dz.clubadmin.ui.widgets;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import ru.dz.clubadmin.R;

import java.util.List;

/**
 * Created by nikitakulagin on 12.05.16.
 */
public abstract class EmployeeListAdapter<T> extends ArrayAdapter<T> {
    private LayoutInflater mInflater;

    @LayoutRes
    private int resource;
    private List<T> list;

    public EmployeeListAdapter(Context context, @LayoutRes int resource, List<T> list) {
        super(context, resource, list);
        this.resource = resource;
        this.list = list;
        mInflater = LayoutInflater.from(context);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(resource, parent, false);
            holder = new ViewHolder();
            holder.employeeName = (TextView) row.findViewById(R.id.employee_name_id);
            holder.employeeRoles = (TextView) row.findViewById(R.id.employee_roles_id);
            holder.workdAt = (TextView) row.findViewById(R.id.employee_since_id);
            holder.photo = (ImageView) row.findViewById(R.id.employee_photo_id);
            holder.employeeLayout = (RelativeLayout) row.findViewById(R.id.employee_id);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        T item = this.list.get(position);
        fillItem(item, holder);
        return row;
    }
    public abstract void fillItem(T item, ViewHolder holder);

    public class ViewHolder {
        public TextView employeeName, employeeRoles, workdAt;
        public ImageView photo;
        public RelativeLayout employeeLayout;
    }
}
