package ru.dz.clubadmin.ui.fragments.venue;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboListFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.models.Venue;
import ru.dz.clubadmin.domain.models.VenueRole;
import ru.dz.clubadmin.loaders.venue.UserVenueLoader;
import ru.dz.clubadmin.ui.fragments.account.MyAccountFragment;
import ru.dz.clubadmin.ui.fragments.menu.FirstMenuFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;
import ru.dz.clubadmin.ui.utils.image.ImageLoader;
import ru.dz.clubadmin.ui.widgets.VenueAdapter;

import java.util.List;

public class VenueListFragment extends RoboListFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {

    private static final String TAG = "VenueListFragment";
    private String apiKey;

    private FragmentManager fragmentManager;
    @Inject
    private VenueInfoFragment venueInfoFragment;
    @Inject
    private FindAddVenueFragment findAddVenueFragment;
    @Inject
    private FirstMenuFragment firstMenuFragment;

    @InjectView(R.id.user_profile_button_id)
    private ImageButton myProfileButton;
    @Inject
    private FragmentUtils fragmentUtils;
    @Inject
    private MyAccountFragment myAccountFragment;
    @InjectView(R.id.add_venue_button_id)
    private ImageButton addVenueButton;
    @InjectView(R.id.emptyVenueLayoutId)
    private RelativeLayout emptyVenueLayout;
    @InjectView(R.id.list_venue_id)
    private RelativeLayout list;
    @Inject
    private ApiKeyHelper apiKeyHelper;
    @Inject
    private ApplicationContext applicationContext;
    @Inject
    private VenueHelper venueHelper;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_venue_list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        this.apiKey = apiKeyHelper.getApiKey().getApiKey();
        if (this.apiKey != null) {
            getLoaderManager().initLoader(R.id.user_venue_list_loader, null, this);
        }
        addVenueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentUtils.changeFragmentContent(fragmentManager, findAddVenueFragment, R.id.venue_list);
            }
        });
        myProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentUtils.changeFragmentContent(fragmentManager, myAccountFragment, R.id.venue_list);
            }
        });
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(this.getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "Start loader");
        switch (id) {
            case R.id.user_venue_list_loader:
                Log.i(TAG, "Find id loader");
                showToast("Loading venues...");
                return new UserVenueLoader(this.getActivity());
            default:
                return null;
        }
    }

    private void loadAdapter(List<Venue> venueList) {

        VenueAdapter<Venue> adapter = new VenueAdapter<Venue>(this.getActivity(),
                R.layout.fragment_venue, venueList) {
            @Override
            public void fillItem(final Venue item, VenueAdapter<Venue>.ViewHolder holder) {
                holder.titleVenue.setText(item.getName());
                String roles = "";
                for(VenueRole role : item.getPreferredRoles()) roles += role.toString()+" ";

                holder.descriptionVenue.setText("My Role: " + roles);

                holder.infoVenue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle args = new Bundle();
                        args.putSerializable("item", item);
                        args.putString("from", "BROWSE_VENUE");
                        venueInfoFragment.setArguments(args);
                        fragmentUtils.changeFragmentContent(fragmentManager, venueInfoFragment, R.id.venue_list);
                    }
                });
                try {
//                    Bitmap bmp = fragmentUtils.getLoaderImage(item.getCoverUrl());
//                    Log.d(TAG, "Height " + bmp.getHeight());
//                    holder.coverVenue.setImageBitmap(bmp);
                    fragmentUtils.getLoaderImage(item.getCoverUrl(),holder.coverVenue);

                } catch (Exception ex) {
                    Log.e(TAG, ex.toString());
                }
                holder.coverVenue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle args = new Bundle();
                        args.putSerializable("venue", item);
                        venueHelper.saveCurrentVenue(item);
                        firstMenuFragment.setArguments(args);
                        fragmentUtils.changeFragmentContent(fragmentManager, firstMenuFragment, R.id.venue_list);
                    }
                });
            }
        };

        setListAdapter(adapter);

    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse baseResponse) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        if (id == R.id.user_venue_list_loader) {
            if (baseResponse.isSuccess()) {
                SuccessResponse<List<Venue>> resp = baseResponse.asSuccess();
                List<Venue> responseMessage = resp.getObj();
                if (responseMessage != null) {
                    if (!responseMessage.isEmpty()) {
                        Log.d(TAG, responseMessage.get(0).getName());
                    } else {
                        Log.d(TAG, "ResponseMessage is Empty");
                    }
                    if (!responseMessage.isEmpty()) {
                        loadAdapter(responseMessage);
                        list.setVisibility(View.VISIBLE);
                        emptyVenueLayout.setVisibility(View.INVISIBLE);
                    } else {
                        emptyVenueLayout.setVisibility(View.VISIBLE);
                        list.setVisibility(View.INVISIBLE);
                    }
                }
            } else {
                ErrorMessage errorMessage = baseResponse.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                showToast(errorMessage.getError());
            }
        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }


}
