package ru.dz.clubadmin.ui.fragments.account;

import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.GridLayout;
import android.widget.TextView;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.database.users.UserInfoHelper;
import ru.dz.clubadmin.domain.UserInfo;
import ru.dz.clubadmin.domain.models.VenueRole;

import java.util.ArrayList;
import java.util.List;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.TRANSPARENT;

public class MyRolesFragment extends RoboFragment {
    private int blue = Color.argb(255,95,156,223);
    private List<VenueRole> myPreferedRoles = new ArrayList<>();
    private int gray = Color.argb(255,246,246,246);
    @InjectView(R.id.my_roles_grid)
    private GridLayout checkBoxLayout;
    @InjectView(R.id.roles_info_text)
    private TextView rolesInfoTextView;
    @Inject
    private UserInfoHelper userInfoHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_roles, null);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        fragmentManager = getFragmentManager();
        final UserInfo user = userInfoHelper.getUserInfo();
        if (rolesInfoTextView!=null) {
            rolesInfoTextView.setBackgroundColor(gray);
            rolesInfoTextView.setText("Please mark prefered roles below");
        }
        checkBoxLayout.measure(GridLayout.LayoutParams.MATCH_PARENT, GridLayout.LayoutParams.WRAP_CONTENT);
        for (final VenueRole role: VenueRole.values()) {
//            Log.i(TAG, role.toString());
            final CheckBox roleButton = new CheckBox(getActivity());
            roleButton.setText(role.toString());
            roleButton.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            roleButton.setTextColor(BLACK);
            roleButton.setButtonDrawable(new StateListDrawable());
            GridLayout.LayoutParams params = new GridLayout.LayoutParams();
            params.width = (this.getResources().getDisplayMetrics().widthPixels/2);
            params.height = 90;
            roleButton.setLayoutParams(params);
            roleButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    roleButton.setBackgroundColor(blue);
                    if (myPreferedRoles.contains(role)) {

                        roleButton.setBackgroundColor(TRANSPARENT);
                        roleButton.setChecked(false);
                        myPreferedRoles.remove(role);
                    } else {
                        roleButton.setChecked(true);
                        roleButton.setBackgroundResource(R.drawable.ck_bg);
                        myPreferedRoles.add(role);
                    }
                }
            });
            checkBoxLayout.addView(roleButton);
        }
        if (user.getPreferredRoles()!=null) {
            myPreferedRoles.addAll(user.getPreferredRoles());
            List<String> roleList = new ArrayList<>();
            for (VenueRole role: user.getPreferredRoles()) {
                roleList.add(role.toString());
            }
            for (int i = 0; i < checkBoxLayout.getChildCount(); i++) {
                CheckBox chkBx = (CheckBox) checkBoxLayout.getChildAt(i);
                if (roleList.contains(chkBx.getText())) {
                    chkBx.setBackgroundResource(R.drawable.ck_bg);
                    chkBx.setChecked(true);
                }
            }
        }
    }
}
