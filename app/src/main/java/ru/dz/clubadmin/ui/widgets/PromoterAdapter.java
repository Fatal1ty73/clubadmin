package ru.dz.clubadmin.ui.widgets;


import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import ru.dz.clubadmin.R;

import java.util.List;

public abstract class PromoterAdapter<T> extends ArrayAdapter<T> {
    private LayoutInflater mInflater;
    private List<T> list;

    @LayoutRes
    private int resource;

    public PromoterAdapter(Context context, @LayoutRes int resource, List<T> list) {
        super(context, resource, list);
        this.resource = resource;
        this.list = list;
        mInflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {
        FindViewHolder holder;
        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(resource, parent, false);
            holder = new FindViewHolder();
            holder.imgPhoto = (ImageView) row.findViewById(R.id.img_photo);
            holder.tvName = (TextView) row.findViewById(R.id.tv_name);
            holder.tvFriends = (TextView) row.findViewById(R.id.tv_friends);
            holder.cbSelected = (CheckBox) row.findViewById(R.id.cb_selected);
            holder.rlFollower = (RelativeLayout) row.findViewById(R.id.rl_follower);
            holder.idPromoter = 0L;
            row.setTag(holder);
        } else {
            holder = (FindViewHolder) row.getTag();
        }
        T item = this.list.get(position);
        fillItem(item, holder);
        return row;
    }


    public abstract void fillItem(T item, FindViewHolder holder);

    public class FindViewHolder {
        public ImageView imgPhoto;
        public TextView tvName, tvFriends;
        public CheckBox cbSelected;
        public Long idPromoter;
        public RelativeLayout rlFollower;
    }

    public List<T> getList() {
        return list;
    }
}
