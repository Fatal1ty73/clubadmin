package ru.dz.clubadmin.ui.fragments.signin;


import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.domain.ApiKey;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.loaders.users.ApiKeyLoader;
import ru.dz.clubadmin.ui.activities.VenueListActivity;
import ru.dz.clubadmin.ui.fragments.auth.RegistrationFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

public class EnterCodeFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {

    private static final String TAG = "EnterCodeFragment";

    private FragmentManager fragmentManager;

    @Inject
    private RegistrationFragment registrationFragment;

    @InjectView(R.id.closeEnterCodeBtn)
    private ImageButton mClose;
    @InjectView(R.id.doneSecurityCodeBtn)
    private Button mDone;
    @InjectView(R.id.securityCodeTxt)
    private EditText mSecurityCodeTxt;

    private String phone;



    @Inject
    FragmentUtils fragmentUtils;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_enter_code, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fragmentManager = getFragmentManager();

        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSecurityCode();
            }
        });
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close(v);
            }
        });

        this.phone = getArguments().getString("phone");
    }

    private void startLoad() {
        Log.i(TAG, "Clicked!");
        getActivity().getLoaderManager().restartLoader(R.id.api_key_loader, null, this);
    }

    private void sendSecurityCode(){
        Log.i(TAG,"Clicked!");
        String securityCode = mSecurityCodeTxt.getText().toString();

        if(securityCode.length()>5) {
            Log.i(TAG,"OK!");
            startLoad();
        } else {
            Log.e(TAG,"Field is empty!");
            Toast toast = Toast.makeText(getActivity(),
                    "Field is empty!", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP, 0, 0);
            toast.show();
        }
    }

    private void showVenuesActivity(String apiKey) {
        Intent intent = new Intent(getActivity(), VenueListActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void showToast(String message){
        Toast toast = Toast.makeText(getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    public void close(View view){
        fragmentUtils.changeFragmentContent(fragmentManager, registrationFragment, R.id.contentPanel);
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.api_key_loader:
                Log.i(TAG, "Find id loader");
                showToast("Wait, try send...");
                String securityCode = mSecurityCodeTxt.getText().toString();
                return new ApiKeyLoader(getActivity(), phone, securityCode);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse baseResponse) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        if (id == R.id.api_key_loader) {

            if (baseResponse.isSuccess()) {
                SuccessResponse<ApiKey> resp = baseResponse.asSuccess();
                ApiKey responseMessage = resp.getObj();
                if (responseMessage != null) {
                    Log.d(TAG, responseMessage.getApiKey());
                    showToast(responseMessage.getApiKey());

                    showVenuesActivity(responseMessage.getApiKey());
                }
            } else {
                ErrorMessage errorMessage = baseResponse.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                showToast(errorMessage.getError());
            }
        }
        getLoaderManager().destroyLoader(id);

    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }
}
