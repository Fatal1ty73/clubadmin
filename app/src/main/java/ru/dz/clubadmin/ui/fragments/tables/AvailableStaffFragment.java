package ru.dz.clubadmin.ui.fragments.tables;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.ApiKey;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.SearchUserInfo;
import ru.dz.clubadmin.domain.StaffAssignment;
import ru.dz.clubadmin.domain.StaffList;
import ru.dz.clubadmin.domain.VenueStaffInfo;
import ru.dz.clubadmin.loaders.reservation.AssignStaffLoader;
import ru.dz.clubadmin.loaders.venue.AvailableStaffLoader;
import ru.dz.clubadmin.ui.utils.FragmentUtils;
import ru.dz.clubadmin.ui.widgets.AvailableStaffListAdapter;

/**
 * Created by Sam (samir@peller.tech) on 20.06.2016.
 */
public class AvailableStaffFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse>{
    private static final String TAG = "AvailableStaffFragment";
    private static final int GET_AVAILABLE_STAFF_INDEX = 0;
    private static final int ASSIGN_STAFF_INDEX = 1;

    @InjectView(R.id.staff_tab_layout)
    TabLayout staffTabLayout;
    @InjectView(R.id.available_staff_list)
    ListView staffList;

    @Inject
    private FragmentManager fragmentManager;
    @Inject
    private VenueHelper venueHelper;
    @Inject
    private ApiKeyHelper apiKeyHelper;
    @Inject
    FragmentUtils fragmentUtils;
    @Inject
    TablesFragment tablesFragment;

    private Long venueId;
    private String apiKey;
    private List<VenueStaffInfo> servers = new ArrayList<>();
    private List<VenueStaffInfo> bussers = new ArrayList<>();
    private List<VenueStaffInfo> hosts = new ArrayList<>();
    //private List<VenueStaffInfo> all = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_available_staff, null);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        venueId = Long.valueOf(venueHelper.getVenue().getId());

        createTabLayout(view);

        ApiKey key = apiKeyHelper.getApiKey();
        if(key.getApiKey() != null && !key.getApiKey().isEmpty()) {
            this.apiKey = key.getApiKey();
            getLoaderManager().initLoader(GET_AVAILABLE_STAFF_INDEX, null, AvailableStaffFragment.this);
        } else {
            Log.i(TAG, "Key is empty");
        }
    }

    private void createTabLayout(View view) {
        staffTabLayout.addTab(staffTabLayout.newTab().setText("Server"));
        staffTabLayout.addTab(staffTabLayout.newTab().setText("Busser"));
        staffTabLayout.addTab(staffTabLayout.newTab().setText("Host"));
        //staffTabLayout.addTab(staffTabLayout.newTab().setText("All"));

        staffTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        createAdapter(servers, "SERVER");
                        break;
                    case 1:
                        createAdapter(bussers, "BUSSER");
                        break;
                    case 2:
                        createAdapter(hosts, "HOST");
                        break;
                    /*case 3:
                        createAdapter(all, "ALL");
                        break;*/
                    default:
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case GET_AVAILABLE_STAFF_INDEX:
                showToast("Wait, try send...");
                return new AvailableStaffLoader(getActivity(), venueId, apiKey);
            case ASSIGN_STAFF_INDEX:
                showToast("Wait, try send...");
                ArrayList<StaffAssignment> assignedStaffList = new ArrayList<>();
                assignedStaffList.add((StaffAssignment) args.getSerializable("assignedStaff"));
                return new AssignStaffLoader(getActivity(), getArguments().getLong("reservationId"), assignedStaffList);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        switch (id) {
            case GET_AVAILABLE_STAFF_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<StaffList> resp = data.asSuccess();
                    StaffList staffList = resp.getObj();

                    servers.addAll(staffList.getServers());
                    bussers.addAll(staffList.getBussers());
                    hosts.addAll(staffList.getHosts());
                    //all.addAll(servers);all.addAll(bussers);all.addAll(hosts);

                    createAdapter(servers, "SERVER");
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
            case ASSIGN_STAFF_INDEX:
                if(data.isSuccess()) {
                    Bundle args = new Bundle();
                    args.putLong("eventId", getArguments().getLong("eventId"));
                    args.putSerializable("eventDate", getArguments().getSerializable("eventDate"));
                    tablesFragment.setArguments(args);
                    fragmentUtils.changeFragmentContent(fragmentManager, tablesFragment, R.id.content, false);
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }

    private void createAdapter(List<VenueStaffInfo> list, final String role) {
        final AvailableStaffListAdapter<VenueStaffInfo> adapter = new AvailableStaffListAdapter<VenueStaffInfo>(this.getActivity(),
                R.layout.fragment_available_staff_list, list) {
            @Override
            public void fillItem(final VenueStaffInfo item, FindViewHolder holder) {
                if (item.getUserpic() != null && !item.getUserpic().isEmpty()) {
                    try {
                        fragmentUtils.getLoaderImage(item.getUserpic(),holder.photo);
                    } catch (Exception ex) {
                        if (ex.getMessage() != null)
                            Log.e(TAG, ex.getMessage());
                    }
                } else {
                    holder.photo.setImageResource(R.drawable.logo_venue);
                }

                holder.name.setText(item.getFullName());
                holder.roles.setText("Roles: " + item.getPreferredRoles().toString());

                holder.employee.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StaffAssignment assignedStaff = new StaffAssignment();
                        assignedStaff.setId(item.getId());
                        assignedStaff.setName(item.getFullName());
                        assignedStaff.setPhone(item.getPhoneNumber());
                        assignedStaff.setRole(role);

                        Bundle args = new Bundle();
                        args.putSerializable("assignedStaff", assignedStaff);
                        getLoaderManager().initLoader(ASSIGN_STAFF_INDEX, args, AvailableStaffFragment.this);
                    }
                });
            }
        };
        adapter.notifyDataSetChanged();

        staffList.setAdapter(adapter);
    }

    private void showToast(String message){
        Toast toast = Toast.makeText(getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }
}
