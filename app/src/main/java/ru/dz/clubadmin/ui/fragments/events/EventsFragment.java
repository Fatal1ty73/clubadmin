package ru.dz.clubadmin.ui.fragments.events;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import com.google.inject.Inject;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.prolificinteractive.materialcalendarview.format.DateFormatTitleFormatter;
import org.joda.time.DateTime;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.EventCalendar;
import ru.dz.clubadmin.domain.EventsList;
import ru.dz.clubadmin.domain.models.EventInfo;
import ru.dz.clubadmin.loaders.events.EventsCalendarLoader;
import ru.dz.clubadmin.loaders.events.EventsLoader;
import ru.dz.clubadmin.ui.fragments.menu.ClickerFragment;
import ru.dz.clubadmin.ui.fragments.reservations.ReservationsFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;
import ru.dz.clubadmin.ui.utils.OneDayDecorator;
import ru.dz.clubadmin.ui.widgets.EventItemAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EventsFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {

    private static final String TAG = "EventsFragment";
    @Inject
    FragmentUtils fragmentUtils;
    @Inject
    private VenueHelper venueHelper;
    @Inject
    ClickerFragment clickerFragment;
    @Inject
    private EditEventFragment editEventFragment;
    private FragmentManager fragmentManager;

    @InjectView(R.id.menu_button_id)
    private ImageButton mainMenuButton;
    @InjectView(R.id.event_calendar)
    private MaterialCalendarView eventCalendar;
    @InjectView(R.id.events_listview)
    private ListView eventsListView;
    @InjectView(R.id.add_event_button)
    private ImageButton addEventButton;
    private DateTime eventCalendarDateRange = DateTime.now();
    private DateTime selectedEventDate = new DateTime();

    private Integer backView;

    @Inject
    private ReservationsFragment reservationsFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_events, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mainMenuButton != null) {
            fragmentUtils.setMenu(mainMenuButton, view);
        }
        fragmentManager = getFragmentManager();
        setCalendarDateRange();
        configureCalendar();
        if(addEventButton != null){
            addEventButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentUtils.changeFragmentContent(fragmentManager, editEventFragment, R.id.content);
                }
            });
        }
    }

    private void setCalendarDateRange() {
        int maximumDayInMonth = DateTime.now().dayOfMonth().getMaximumValue();
        this.eventCalendarDateRange = DateTime.now().dayOfMonth().setCopy(maximumDayInMonth);
    }

    private void configureCalendar() {
        if (this.eventCalendar != null) {
            getActivity().getLoaderManager().restartLoader(R.id.event_calendar_loader, null, EventsFragment.this);
            getActivity().getLoaderManager().restartLoader(R.id.event_list_loader, null, EventsFragment.this);

            DateFormatTitleFormatter dateFormatTitleFormatter = new DateFormatTitleFormatter(new SimpleDateFormat("MMM, yyyy"));
            eventCalendar.setTitleFormatter(dateFormatTitleFormatter);
            eventCalendar.setDateSelected(new Date(), true);
            this.eventCalendar.setOnMonthChangedListener(new OnMonthChangedListener() {
                @Override
                public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
                    Loader<Object> lm = getActivity().getLoaderManager().getLoader(R.id.event_calendar_loader);
                    if (lm.isStarted()) {
                        Log.d(TAG, "Reset loader");
                        getActivity().getLoaderManager().destroyLoader(R.id.event_calendar_loader);
                    }
                    int maximum = new DateTime(date.getDate()).dayOfMonth().getMaximumValue();
                    eventCalendarDateRange = new DateTime(date.getDate()).dayOfMonth().setCopy(maximum);
                    getActivity().getLoaderManager().restartLoader(R.id.event_calendar_loader, null, EventsFragment.this);
                }
            });
            eventCalendar.setOnDateChangedListener(new OnDateSelectedListener() {
                @Override
                public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                    selectedEventDate = new DateTime(date.getDate());
                    Log.d(TAG, "CurrentDate " + date.toString());
                    Loader<Object> lm = getActivity().getLoaderManager().getLoader(R.id.event_list_loader);
                    if (lm.isStarted()) {
                        Log.d(TAG, "Reset loader");
                        getActivity().getLoaderManager().destroyLoader(R.id.event_list_loader);
                    }

                    getActivity().getLoaderManager().restartLoader(R.id.event_list_loader, null, EventsFragment.this);
                }
            });
        }
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(getActivity(),
                message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    private void loadAdapter(final List<EventInfo> eventInfoList) {
        final EventItemAdapter<EventInfo> adapter = new EventItemAdapter<EventInfo>(this.getActivity(),
                R.layout.event_item_adapter, eventInfoList) {
            @Override
            public void fillItem(final EventInfo item, FindViewHolder holder) {
                holder.eventName.setText(item.getName());
                holder.eventDate.setText("Start at " + item.getStartsAt());

                if (item.getPictureUrl() != null && !item.getPictureUrl().isEmpty()) {
                    try {
                        fragmentUtils.getLoaderImage(item.getPictureUrl(),holder.venueItemImg);
                    } catch (Exception ex) {
                        if(ex.getMessage() != null)
                            Log.e(TAG, ex.getMessage());
                    }
                } else {
                    holder.venueItemImg.setImageResource(R.drawable.logo_venue);
                }
                holder.eventItemLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(TAG, "Clicked id " + item.getId());
                        Bundle bundle = new Bundle();
                        Bundle args = getArguments();
                        if(args != null) {
                            backView = args.getInt("backView");
                        }
                        if(backView != null) {
                            bundle.putLong("eventId", item.getId());
                            bundle.putString("eventDate", item.getDate());
                            reservationsFragment.setArguments(bundle);
                            switch (backView) {
                                case R.layout.fragment_reservations:
                                    fragmentUtils.changeFragmentContent(fragmentManager, reservationsFragment, R.id.content);
                                    break;
                                case R.layout.fragment_clicker:
                                    clickerFragment.setArguments(bundle);
                                    fragmentUtils.changeFragmentContent(fragmentManager, clickerFragment, R.id.content);
                                    break;
                            }
                        } else {
                            bundle.putSerializable("event", item);
                            editEventFragment.setArguments(bundle);
                            fragmentUtils.changeFragmentContent(fragmentManager, editEventFragment, R.id.content);
                        }
                    }
                });

                holder.deleteEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(TAG, "EventInfo id " + item.getId());
                    }
                });
            }
        };
        adapter.notifyDataSetChanged();

        eventsListView.setAdapter(adapter);
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        Long venueId = venueHelper.getVenue().getId();
        switch (id) {
            case R.id.event_calendar_loader:
                Log.i(TAG, "Find id loader");
                showToast("Load event calendar...");
                return new EventsCalendarLoader(getActivity(), eventCalendarDateRange.toDate(), venueId.toString());
            case R.id.event_list_loader:
                Log.i(TAG, "Find id loader");
                showToast("Load list events...");
                return new EventsLoader(getActivity(), selectedEventDate, venueId);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse baseResponse) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        switch (id) {
            case R.id.event_calendar_loader: {
                if (baseResponse.isSuccess()) {
                    SuccessResponse<List<EventCalendar>> resp = baseResponse.asSuccess();
                    List<EventCalendar> responseMessage = resp.getObj();
                    if (responseMessage != null) {

                        Log.d(TAG, "Size " + responseMessage.size());
                        for (EventCalendar event : responseMessage) {
                            OneDayDecorator mSelectedDayDecorator = new OneDayDecorator();
                            mSelectedDayDecorator.setDate(event.getDateTime());
                            eventCalendar.addDecorator(mSelectedDayDecorator);
                        }

                    }
                } else {
                    ErrorMessage errorMessage = baseResponse.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }
                break;
            }
            case R.id.event_list_loader: {
                if (baseResponse.isSuccess()) {
                    SuccessResponse<EventsList> resp = baseResponse.asSuccess();
                    EventsList responseMessage = resp.getObj();
                    if (responseMessage != null) {

                        Log.d(TAG, "Size events " + responseMessage.getEventsList().size());
                        loadAdapter(responseMessage.getEventsList());
                    }
                } else {
                    ErrorMessage errorMessage = baseResponse.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    System.out.println(errorMessage.getError());
                    showToast(errorMessage.getError());
                }
                break;
            }
        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {
        int id = loader.getId();
        switch (id) {
            case R.id.event_calendar_loader: {
                Log.d(TAG, "Reset loader");
                showToast("Reset loader");
                break;
            }
            case R.id.event_list_loader: {
                Log.d(TAG, "Reset selected date loader");
                showToast("Reset selected date loader");
                break;
            }
        }
    }
}
