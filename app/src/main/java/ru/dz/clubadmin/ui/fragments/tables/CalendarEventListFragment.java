package ru.dz.clubadmin.ui.fragments.tables;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Toast;

import com.google.inject.Inject;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.EventsList;
import ru.dz.clubadmin.domain.models.EventInfo;
import ru.dz.clubadmin.loaders.events.EventsLoader;
import ru.dz.clubadmin.ui.utils.FragmentUtils;
import ru.dz.clubadmin.ui.widgets.EventItemAdapter;

/**
 * Created by Sam (samir@peller.tech) on 01.06.2016.
 */
public class CalendarEventListFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {
    private static final String TAG ="CalendEventListFragment";
    private static final int LOAD_EVENTS_LIST_DAY_INDEX = 0;
    private static final int LOAD_EVENTS_LIST_PICKERDATE_INDEX = 1;

    private long venueId;
    @Inject
    private ApiKeyHelper apiKeyHelper;
    @Inject
    private VenueHelper venueHelper;
    @Inject
    FragmentUtils fragmentUtils;
    @Inject
    private FragmentManager fragmentManager;
    @Inject
    private TablesFragment tablesFragment;

    @InjectView(R.id.events_listview)
    private ListView eventsListView;
    @InjectView(R.id.datePicker)
    private DatePicker datePicker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_event_list, null);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        venueId = venueHelper.getVenue().getId();

        String apiKey = apiKeyHelper.getApiKey().getApiKey();
        if (apiKey != null) {
            Bundle args = new Bundle();
            if(getArguments().containsKey("date")) {
                DateTime date = (DateTime) getArguments().getSerializable("date");
                args.putSerializable("date", date);
                datePicker.init(date.getYear(), date.getMonthOfYear() - 1, date.getDayOfMonth(), new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        getLoaderManager().initLoader(LOAD_EVENTS_LIST_PICKERDATE_INDEX, null, CalendarEventListFragment.this);
                    }
                });
            } else {
                args.putSerializable("date", DateTime.now());
                datePicker.init(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(), new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        getLoaderManager().initLoader(LOAD_EVENTS_LIST_PICKERDATE_INDEX, null, CalendarEventListFragment.this);
                    }
                });
            }
            getLoaderManager().initLoader(LOAD_EVENTS_LIST_DAY_INDEX, args, this);
        }
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "Start loader");
        switch (id) {
            case LOAD_EVENTS_LIST_DAY_INDEX:
                Log.i(TAG, "Loading events");
                showToast("Loading events...");
                return new EventsLoader(getActivity(), (DateTime) args.getSerializable("date"), venueId);
            case LOAD_EVENTS_LIST_PICKERDATE_INDEX:
                Log.i(TAG, "Loading events");
                showToast("Loading events...");
                return new EventsLoader(getActivity(), new DateTime(getDateFromDatePicker(datePicker)), venueId);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        switch (id) {
            case LOAD_EVENTS_LIST_DAY_INDEX:
            case LOAD_EVENTS_LIST_PICKERDATE_INDEX: {
                if (data.isSuccess()) {
                    SuccessResponse<EventsList> resp = data.asSuccess();
                    EventsList eventsListMessage = resp.getObj();
                    if (eventsListMessage != null) {
                        loadAdapter(eventsListMessage.getEventsList());
                    }
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
            }
        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }

    private void loadAdapter(final List<EventInfo> eventList) {
        final EventItemAdapter<EventInfo> adapter = new EventItemAdapter<EventInfo>(this.getActivity(),
                R.layout.event_item_adapter, eventList) {
            @Override
            public void fillItem(final EventInfo item, FindViewHolder holder) {
                holder.deleteEvent.setVisibility(View.INVISIBLE);
                holder.eventName.setText(item.getName());
                holder.eventDate.setText("Start at " + item.getStartsAt());

                if (item.getPictureUrl() != null && !item.getPictureUrl().isEmpty()) {
                    try {
                        fragmentUtils.getLoaderImage(item.getPictureUrl(),holder.venueItemImg);
                    } catch (Exception ex) {
                        if(ex.getMessage() != null)
                            Log.e(TAG, ex.getMessage());
                    }
                } else {
                    holder.venueItemImg.setImageResource(R.drawable.logo_venue);
                }

                holder.eventItemLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fragmentManager = getFragmentManager();

                        Bundle args = new Bundle();
                        args.putLong("eventId", item.getId());
                        args.putString("eventName", item.getName());
                        args.putSerializable("eventDate", new DateTime(getDateFromDatePicker(datePicker)));
                        tablesFragment.setArguments(args);

                        fragmentUtils.changeFragmentContent(fragmentManager, tablesFragment, R.id.content, false);
                    }
                });
            }
        };
        adapter.notifyDataSetChanged();

        eventsListView.setAdapter(adapter);
    }

    public static Date getDateFromDatePicker(DatePicker datePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(this.getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }
}
