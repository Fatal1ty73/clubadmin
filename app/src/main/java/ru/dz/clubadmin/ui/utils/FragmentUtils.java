package ru.dz.clubadmin.ui.utils;

import android.app.FragmentTransaction;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.IdRes;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import ru.dz.clubadmin.ui.utils.image.ImageLoader;

import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Singleton
public class FragmentUtils {

    @Inject
    private ImageLoader imageLoader;

    private void cleanImageCache(){
        imageLoader.clearCache();
    }

    public void changeFragmentContent(android.app.FragmentManager fragmentManager, android.app.Fragment fragment, @IdRes int containerViewId) {
        changeFragmentContent(fragmentManager, fragment, containerViewId, true);
    }
    public void changeFragmentContent(android.app.FragmentManager fragmentManager, android.app.Fragment fragment, @IdRes int containerViewId, Boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if(addToBackStack) fragmentTransaction.addToBackStack(null);
        cleanImageCache();
        fragmentTransaction.replace(containerViewId, fragment).attach(fragment).commit();
    }

    public void detachFragment(android.app.FragmentManager fragmentManager, android.app.Fragment fragment) {
        cleanImageCache();
        fragmentManager.beginTransaction().detach(fragment).commit();
    }
    public void addFragmentContent(android.app.FragmentManager fragmentManager, android.app.Fragment fragment, @IdRes int containerViewId) {
        cleanImageCache();
        fragmentManager.beginTransaction().add(containerViewId, fragment).commit();
    }

    public void removeFragmentContent(android.app.FragmentManager fragmentManager, android.app.Fragment fragment) {
        cleanImageCache();
        fragmentManager.beginTransaction().remove(fragment).commit();
    }

    public void getLoaderImage(String url, ImageView imageView) throws InterruptedException, ExecutionException, TimeoutException {
        imageLoader.DisplayImage(url, imageView);
//        return new LoadImage().execute(url).get(10, TimeUnit.SECONDS);
    }


    private static class LoadImage extends AsyncTask<String, String, Bitmap> {

        protected Bitmap doInBackground(String... args) {
            Bitmap bitmap = null;
            if(args.length > 0 && args[0] != null && !args[0].isEmpty()) {
                try {
                    bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap bmp) {

        }
    }
    public void setMenu(ImageButton menu, final View currentView) {
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FrameLayout frameLayout = (FrameLayout) currentView.getParent();
                final DrawerLayout drawerLayout = (DrawerLayout) frameLayout.getParent();
                drawerLayout.openDrawer(GravityCompat.START);
                drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
                    @Override
                    public void onDrawerClosed(View drawerView) {
                        frameLayout.setTranslationX(0);
                    }
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        frameLayout.setTranslationX(slideOffset * drawerView.getWidth());
                    }
                });
            }
        });
    }

}
