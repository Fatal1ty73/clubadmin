package ru.dz.clubadmin.ui.fragments.promo;


import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import roboguice.util.Strings;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.Promoter;
import ru.dz.clubadmin.domain.models.Promotion;
import ru.dz.clubadmin.loaders.promo.CreateFeedbackLoader;
import ru.dz.clubadmin.loaders.promo.GetPromotersLoader;
import ru.dz.clubadmin.ui.utils.FragmentUtils;
import ru.dz.clubadmin.ui.widgets.PromoterAdapter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class PromotionsFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {

    private String TAG = "PromotionsFragment";

    @InjectView(R.id.menu_button_id)
    private ImageButton mainMenu;
    @InjectView(R.id.edit_promo_url)
    private EditText editTextPromoUrl;
    @InjectView(R.id.edit_promo_message)
    private EditText editTextPromoMessage;
    @InjectView(R.id.grid_followers)
    private GridView gridFollowers;
    @InjectView(R.id.btn_preview)
    private Button btnPreview;
    @Inject
    private FragmentUtils fragmentUtils;
    @Inject
    private VenueHelper venueHelper;

    private Long venueId = 0L;

    private Map<Long, Promoter> checkedPromoters = new ConcurrentHashMap<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_promotions, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mainMenu != null) {
            fragmentUtils.setMenu(mainMenu, view);
        }
        venueId = venueHelper.getVenue().getId();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.scrollToPosition(0);
        //rvPromoFollowersList.setLayoutManager(layoutManager);
        getLoaderManager().initLoader(R.id.promoters_loader, null, PromotionsFragment.this);
        btnPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editTextPromoUrl.getText().toString().isEmpty()) {
                    getLoaderManager().initLoader(R.id.send_promo_loader, null, PromotionsFragment.this);
                }
            }
        });
        List<Promoter> listPromoters = new ArrayList<Promoter>();
        for (int i = 0; i < 10; i++) {
            Promoter promoter = new Promoter();

            promoter.setId(123L + i);
            promoter.setNumberOfFriends(233 + i);
            promoter.setUserpic("https://lh5.googleusercontent.com/-6_pwkiZp5-A/AAAAAAAAAAI/AAAAAAAAFII/v_jXXN9rS38/photo.jpg");
            promoter.setFullName("Ivan Ivanov " + i);
            listPromoters.add(promoter);
        }
        fillFollowers(listPromoters);
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "Start loader");
        switch (id) {
            case R.id.promoters_loader:
                Log.i(TAG, "Find id GetPromotersLoader");
                return new GetPromotersLoader(this.getActivity(), this.venueId);
            case R.id.send_promo_loader:
                Log.i(TAG, "Find id send_promo_loader");
                if (!editTextPromoUrl.getText().toString().isEmpty() && !editTextPromoMessage.getText().toString().isEmpty()) {
                    Promotion promotion = new Promotion();
                    promotion.setMessage(Strings.toString(editTextPromoMessage.getText().toString()));
                    promotion.setUrl(Strings.toString(editTextPromoUrl.getText().toString()));
                    promotion.setUserIds(new ArrayList<Long>());
                    return new CreateFeedbackLoader(this.getActivity(), promotion);
                } else return null;
            default:
                return null;
        }
    }

    private void fillFollowers(List<Promoter> listPromoters) {

        PromoterAdapter<Promoter> adapter = new PromoterAdapter<Promoter>(this.getActivity(),
                R.layout.promo_followers_adapter, listPromoters) {
            @Override
            public void fillItem(final Promoter item, final PromoterAdapter<Promoter>.FindViewHolder holder) {
                holder.tvFriends.setText(Strings.toString(item.getNumberOfFriends()));
                holder.tvName.setText(Strings.toString(item.getFullName()));
                holder.idPromoter = item.getId();
                try {
                    fragmentUtils.getLoaderImage(Strings.toString(item.getUserpic()),holder.imgPhoto);
//                    ByteArrayOutputStream out = new ByteArrayOutputStream();
//                    img.compress(Bitmap.CompressFormat.JPEG, 10, out);
//                    Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
//
//                    holder.imgPhoto.setImageBitmap(decoded);
                } catch (Exception ex) {
                    if (ex.getMessage() != null)
                        Log.e(TAG, ex.getMessage());
                }

//                holder.rlFollower.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Log.i(TAG, "NAME:" + item.getFullName() + " ID:"+item.getId());
//
//
//                        if(holder.cbSelected.isChecked()){
//                            holder.cbSelected.setChecked(!holder.cbSelected.isChecked());
//                            checkedPromoters.remove(item.getId());
//                        } else {
//                            holder.cbSelected.setChecked(!holder.cbSelected.isChecked());
//                            checkedPromoters.put(item.getId(),item);
//                        }
//                        Log.i(TAG, "Size :" +checkedPromoters.size());
//                    }
//                });
            }
        };
        gridFollowers.setAdapter(adapter);

        gridFollowers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckBox cbSelected = (CheckBox) view.findViewById(R.id.cb_selected);

                Promoter promoter = (Promoter) parent.getItemAtPosition(position);
                Log.i(TAG, "NAME:" + promoter.getFullName() + " ID:" + promoter.getId());
                Log.i(TAG, "SIZE:" + gridFollowers.getCheckedItemIds().length);
                if (cbSelected.isChecked()) {
                    cbSelected.setChecked(!cbSelected.isChecked());
                    checkedPromoters.remove(promoter.getId());
                } else {
                    cbSelected.setChecked(!cbSelected.isChecked());
                    checkedPromoters.put(promoter.getId(), promoter);
                }
            }
        });
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        int id = loader.getId();
        switch (id) {
            case R.id.promoters_loader:
                if (data.isSuccess()) {
                    SuccessResponse<List<Promoter>> response = data.asSuccess();
                    List<Promoter> responseMessage = response.getObj();
                    if (responseMessage != null && responseMessage.isEmpty()) {
                        Log.i(TAG, "Size " + responseMessage.size());
                        fillFollowers(responseMessage);
                    } else Log.i(TAG, "List empty ");

                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                }
                break;
            case R.id.send_promo_loader:
                if (data.isSuccess()) {
                    SuccessResponse<ResponseMessage> response = data.asSuccess();
                    ResponseMessage responseMessage = response.getObj();
                    if (responseMessage != null) {
                        Log.i(TAG, "Response " + responseMessage.getResponse());
                    } else Log.i(TAG, "responseMessage empty ");

                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                }
                break;
        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }
}
