package ru.dz.clubadmin.ui.fragments.account;

import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.database.users.UserInfoHelper;
import ru.dz.clubadmin.domain.UserInfo;
import ru.dz.clubadmin.ui.fragments.venue.VenueListFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;
import ru.dz.clubadmin.ui.widgets.AccountPageAdapter;

public class MyAccountFragment extends RoboFragment {
    @InjectView(R.id.tab_layout)
    private TabLayout tabLayout;
    @InjectView(R.id.pager)
    private ViewPager pager;
    @InjectView(R.id.my_photo)
    private ImageView myPhoto;
    @InjectView(R.id.back)
    private ImageButton backButton;
    @Inject
    private UserInfoHelper userInfoHelper;
    @Inject
    private FragmentUtils fragmentUtils;
    private static final String TAG = "MyAccountFragment";
    private FragmentManager fragmentManager;
    @Inject
    private VenueListFragment listFragment;
    @InjectView(R.id.settings_button_id)
    private ImageButton settingsButton;
    @Inject
    private AccountSettingFragment accountSettingFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_account, null);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        UserInfo user = userInfoHelper.getUserInfo();
        try {
            fragmentUtils.getLoaderImage(user.getUserpic(),myPhoto);
            Log.i(TAG, user.getUserpic());

        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        }
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentUtils.changeFragmentContent(fragmentManager, listFragment, R.id.venue_list);
            }
        });
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentUtils.changeFragmentContent(fragmentManager, accountSettingFragment, R.id.venue_list, false);
            }
        });
        if(tabLayout!=null) {
            tabLayout.addTab(tabLayout.newTab().setText("Profile details"));
            tabLayout.addTab(tabLayout.newTab().setText("My roles"));
            final AccountPageAdapter pageAdapter = new AccountPageAdapter(getFragmentManager(),tabLayout.getTabCount());
            if(pager!=null) {
                pager.setAdapter(pageAdapter);
                pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        pager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
            }
        }

    }

}
