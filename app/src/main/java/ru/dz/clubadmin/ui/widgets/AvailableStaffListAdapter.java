package ru.dz.clubadmin.ui.widgets;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import ru.dz.clubadmin.R;

/**
 * Created by Sam (samir@peller.tech) on 20.06.2016.
 */
public abstract class AvailableStaffListAdapter<T> extends ArrayAdapter<T> {
    private LayoutInflater mInflater;
    private List<T> list;

    @LayoutRes
    private int resource;

    public AvailableStaffListAdapter(Context context, @LayoutRes int resource, List<T> list) {
        super(context, resource, list);
        this.resource = resource;
        this.list = list;

        mInflater = LayoutInflater.from(context);
    }

    public View getView(int position, android.view.View convertView,
                        ViewGroup parent) {
        FindViewHolder holder;
        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(resource, parent, false);
            holder = new FindViewHolder();

            holder.photo = (ImageView) row.findViewById(R.id.employee_photo_id);
            holder.name = (TextView) row.findViewById(R.id.employee_name_id);
            holder.roles = (TextView) row.findViewById(R.id.employee_roles_id);
            holder.employee = (RelativeLayout) row.findViewById(R.id.employee_id);

            row.setTag(holder);
        } else {
            holder = (FindViewHolder) row.getTag();
        }
        T item = this.list.get(position);
        fillItem(item, holder);
        return row;
    }

    public abstract void fillItem(T item, FindViewHolder holder);

    public class FindViewHolder {
        public ImageView photo;
        public TextView name, roles;
        public RelativeLayout employee;
    }
}
