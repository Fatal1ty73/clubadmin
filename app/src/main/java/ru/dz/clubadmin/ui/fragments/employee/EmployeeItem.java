package ru.dz.clubadmin.ui.fragments.employee;

/**
 * Created by nikitakulagin on 13.05.16.
 */
public enum EmployeeItem {
    ALL(0),
    CURRENT(1),
    REQUESTING(2);

    private Integer value;

    EmployeeItem(Integer item) {
        value = item;
    }
    public Integer getValue() {
        return value;
    }
    static public EmployeeItem getItem(Integer value) {
        for (EmployeeItem item: EmployeeItem.values()) {
            if (item.getValue().equals(value)) {
                return item;
            }
        }
        return ALL;
    }
}
