package ru.dz.clubadmin.ui.fragments.tables;


import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.ApiKey;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.EventsList;
import ru.dz.clubadmin.domain.NextDate;
import ru.dz.clubadmin.domain.StaffAssignment;
import ru.dz.clubadmin.domain.TableInfo;
import ru.dz.clubadmin.domain.TableWithSeating;
import ru.dz.clubadmin.loaders.events.EventsLoader;
import ru.dz.clubadmin.loaders.reservation.AssignTableLoader;
import ru.dz.clubadmin.loaders.reservation.ChangeReservationStateLoader;
import ru.dz.clubadmin.loaders.reservation.ChangeSeatingGuestsLoader;
import ru.dz.clubadmin.loaders.reservation.Unassign;
import ru.dz.clubadmin.loaders.reservation.UnassignStaffLoader;
import ru.dz.clubadmin.loaders.tables.TablesWithSeatingLoader;
import ru.dz.clubadmin.loaders.venue.NextEventLoader;
import ru.dz.clubadmin.loaders.venue.PrevEventLoader;
import ru.dz.clubadmin.ui.fragments.reservations.NewReservationFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;
import ru.dz.clubadmin.ui.widgets.SeatingListAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class TablesFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {
    private static final String TAG = "TablesFragment";
    private static final int GET_DATE_EVENT_COUNT_INDEX = 0;
    private static final int GET_EVENT_SEATING_INDEX = 1;
    private static final int GET_NEXT_EVENT_DAY_INDEX = 2;
    private static final int GET_PREV_EVENT_DAY_INDEX = 3;
    private static final int MOVE_RESERVATION_INDEX = 4;
    private static final int UNASSIGN_INDEX = 5;
    private static final int CHANGE_STATE_INDEX = 6;
    private static final int UNASSIGN_STAFF_INDEX = 7;
    private static final int GUEST_CHANGE_INDEX = 8;
    private static final String BAR_SIGNATURE = "BAR";
    private static final String TABLE_SIGNATURE = "TABLE";
    private static final String STANDUP_SIGNATURE = "STANDUP";
    private static final String NO_SHOW_SIGNATURE = "NO_SHOW";
    private static final String RELEASED_SIGNATURE = "RELEASED";
    private static final String ARRIVED_SIGNATURE = "ARRIVED";
    private static final String APPROVED_SIGNATURE = "APPROVED";
    private static final int DISPLAYED_DATE_TYPE  = 0;
    private static final int REQUEST_DATE_TYPE  = 1;
    private static final String STATUS_NO_SHOW_SIGNATURE = "NO_SHOW";
    private static final String GUEST_CHANGE_INC_SIGNATURE = "INC";
    private static final String GUEST_CHANGE_DEC_SIGNATURE = "DEC";
    private static final String GUEST_CHANGE_MALE_SIGNATURE = "MALE";
    private static final String GUEST_CHANGE_FEMALE_SIGNATURE = "FEMALE";
    private String apiKey;

    @Inject
    FragmentUtils fragmentUtils;
    @Inject
    private ApiKeyHelper apiKeyHelper;
    @Inject
    private VenueHelper venueHelper;

    @InjectView(R.id.menu_button_id)
    private ImageButton mainMenuButton;
    @InjectView(R.id.modify_table_imgbtn)
    private ImageButton addTable;
    @InjectView(R.id.calendar_imgbtn)
    private ImageButton calendarBtn;
    @InjectView(R.id.tables_tab_layout)
    private TabLayout tabLayout;
    @InjectView(R.id.seating_listview)
    private ListView seatingListView;
    @InjectView(R.id.tables_date_right)
    private ImageButton nextEvent;
    @InjectView(R.id.date_text)
    private TextView dateText;
    @InjectView(R.id.tables_date_left)
    private ImageButton prevEvent;
    @Inject
    private FragmentManager fragmentManager;
    @Inject
    AddTableFragment addTableFragment;
    @Inject
    CalendarEventListFragment calendarEventListFragment;
    @Inject
    AvailableStaffFragment availableStaffFragment;
    @Inject
    ReservationListFragment reservationListFragment;
    @Inject
    NewReservationFragment newReservationFragment;

    private Long venueId;
    private Long eventId;
    private String eventName;
    private List<TableWithSeating> barList = new ArrayList<>();
    private List<TableWithSeating> tableList = new ArrayList<>();
    private List<TableWithSeating> standUpList = new ArrayList<>();
    private DateTime displayedDate;
    private boolean movingReservation;
    private long movedReservationId;
    SeatingListAdapter.FindViewHolder itemHolder;
    TableWithSeating workingItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tables, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        venueId = (long) venueHelper.getVenue().getId();

        if (mainMenuButton!=null) {
            fragmentUtils.setMenu(mainMenuButton, view);
        }

        fragmentManager = getFragmentManager();
        movingReservation = false;

        addTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentUtils.changeFragmentContent(fragmentManager, addTableFragment, R.id.content, false);
            }
        });
        calendarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putSerializable("date", displayedDate);
                calendarEventListFragment.setArguments(args);

                fragmentUtils.changeFragmentContent(fragmentManager, calendarEventListFragment, R.id.content, false);
            }
        });
        nextEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                getLoaderManager().initLoader(GET_NEXT_EVENT_DAY_INDEX, args, TablesFragment.this);
            }
        });
        prevEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                getLoaderManager().initLoader(GET_PREV_EVENT_DAY_INDEX, args, TablesFragment.this);
            }
        });

        createTabLayout(view);
        startLoader();
    }

    private void createTabLayout(View view) {
        tabLayout.addTab(tabLayout.newTab().setText("Tables"));
        tabLayout.addTab(tabLayout.newTab().setText("Stand-Up"));
        tabLayout.addTab(tabLayout.newTab().setText("Bar"));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        createAdapter(tableList);
                        break;
                    case 1:
                        createAdapter(standUpList);
                        break;
                    case 2:
                        createAdapter(barList);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void startLoader(){
        ApiKey key = apiKeyHelper.getApiKey();
        if(key.getApiKey() != null && !key.getApiKey().isEmpty()) {
            this.apiKey = key.getApiKey();

            Bundle args = new Bundle();

            if(getArguments() != null && getArguments().containsKey("eventId") && getArguments().containsKey("eventDate")) {

                displayedDate = (DateTime) getArguments().getSerializable("eventDate");
                dateText.setText(formattedDateText(displayedDate, DISPLAYED_DATE_TYPE));

                eventId = getArguments().getLong("eventId");
                eventName = getArguments().getString("eventName");
                getLoaderManager().initLoader(GET_EVENT_SEATING_INDEX, null, this);
            } else {
                displayedDate = DateTime.now();
                args.putSerializable("eventDate", displayedDate);
                getLoaderManager().initLoader(GET_DATE_EVENT_COUNT_INDEX, args, this);
            }
        } else {
            Log.i(TAG, "Key is empty");
        }
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case GET_DATE_EVENT_COUNT_INDEX:
                showToast("Wait, try send...");
                return new EventsLoader(getActivity(), (DateTime) args.getSerializable("eventDate"), venueId);
            case GET_EVENT_SEATING_INDEX:
                showToast("Wait, try send...");
                return new TablesWithSeatingLoader(getActivity(), venueId, displayedDate, eventId);
            case GET_NEXT_EVENT_DAY_INDEX:
                showToast("Wait, try send...");
                return new NextEventLoader(getActivity(), venueId, formattedDateText(displayedDate, REQUEST_DATE_TYPE));
            case GET_PREV_EVENT_DAY_INDEX:
                showToast("Wait, try send...");
                return new PrevEventLoader(getActivity(), venueId, formattedDateText(displayedDate, REQUEST_DATE_TYPE));
            case MOVE_RESERVATION_INDEX:
                showToast("Wait, try send...");
                return new AssignTableLoader(getActivity(), args.getLong("reservationId"), (TableInfo) args.getSerializable("tableInfo"));
            case UNASSIGN_INDEX:
                showToast("Wait, try send...");
                return new Unassign(getActivity(), args.getLong("reservationId"));
            case CHANGE_STATE_INDEX:
                showToast("Wait, try send...");
                return new ChangeReservationStateLoader(getActivity(), args.getLong("reservationId"), args.getString("newState"));
            case UNASSIGN_STAFF_INDEX:
                showToast("Wait, try send...");
                ArrayList<StaffAssignment> assignedStaffList = new ArrayList<>();
                assignedStaffList.add((StaffAssignment) args.getSerializable("assignedStaff"));
                return new UnassignStaffLoader(getActivity(), args.getLong("reservationId"), assignedStaffList);
            case GUEST_CHANGE_INDEX:
                showToast("Wait, try send...");
                return new ChangeSeatingGuestsLoader(getActivity(), args.getLong("reservationId"), args.getString("action"), args.getString("gender"));
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        Log.d(TAG, "Load finished");
        int id = loader.getId();
        switch (id) {
            case GET_DATE_EVENT_COUNT_INDEX: {
                if (data.isSuccess()) {
                    SuccessResponse<EventsList> resp = data.asSuccess();
                    EventsList eventsListMessage = resp.getObj();
                    if (eventsListMessage != null) {
                        Bundle args;
                        switch (eventsListMessage.getEventsList().size()) {
                            case 1:
                                dateText.setText(formattedDateText(displayedDate, DISPLAYED_DATE_TYPE));

                                eventId = eventsListMessage.getEventsList().get(0).getId();
                                eventName = eventsListMessage.getEventsList().get(0).getName();
                                getLoaderManager().initLoader(GET_EVENT_SEATING_INDEX, null, this);

                                break;
                            //case size 0, > 1
                            default:
                                args = new Bundle();
                                args.putSerializable("date", displayedDate);
                                calendarEventListFragment.setArguments(args);

                                fragmentUtils.changeFragmentContent(fragmentManager, calendarEventListFragment, R.id.content, false);
                                break;
                        }
                    }
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
            }
            case GET_EVENT_SEATING_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<List<TableWithSeating>> response = data.asSuccess();
                    List<TableWithSeating> responseMessage = response.getObj();

                    separateLists(responseMessage);

                    createAdapter(tableList);
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
            case GET_NEXT_EVENT_DAY_INDEX:
            case GET_PREV_EVENT_DAY_INDEX:
                if (data.isSuccess()) {
                    SuccessResponse<NextDate> response = data.asSuccess();
                    NextDate nextDate = response.getObj();

                    displayedDate = DateTime.parse(nextDate.getDate());
                    Bundle args = new Bundle();
                    args.putSerializable("eventDate", DateTime.parse(nextDate.getDate()));
                    getLoaderManager().initLoader(GET_DATE_EVENT_COUNT_INDEX, args, this);
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
            case MOVE_RESERVATION_INDEX:
                if (data.isSuccess()) {
                    getLoaderManager().initLoader(GET_EVENT_SEATING_INDEX, null, this);
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
            case UNASSIGN_INDEX:
                if (!data.isSuccess()) {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
            case CHANGE_STATE_INDEX:
                if (data.isSuccess()) {
                    if(itemHolder != null && workingItem != null) {
                        setCounterLayer(itemHolder, workingItem);

                        itemHolder = null;
                        workingItem = null;
                    }

                    if(itemHolder != null) {
                        itemHolder.arrivedBtn.setVisibility(View.VISIBLE);
                        itemHolder.counterLayout.setVisibility(View.GONE);

                        itemHolder = null;
                    }
                } else {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
            case UNASSIGN_STAFF_INDEX:
                if (!data.isSuccess()) {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
            case GUEST_CHANGE_INDEX:
                if (!data.isSuccess()) {
                    ErrorMessage errorMessage = data.asError().getObj();
                    Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
                    showToast(errorMessage.getError());
                }

                break;
        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }

    private void separateLists(List<TableWithSeating> responseMessage) {
        barList.clear();
        tableList.clear();
        standUpList.clear();

        for(TableWithSeating item : responseMessage) {
            List<TableWithSeating> barNoShowNo = new ArrayList<>();
            List<TableWithSeating> barNoShowYes = new ArrayList<>();
            List<TableWithSeating> tableNoShowNo = new ArrayList<>();
            List<TableWithSeating> tableNoShowYes = new ArrayList<>();
            List<TableWithSeating> standUpNoShowNo = new ArrayList<>();
            List<TableWithSeating> standUpNoShowYes = new ArrayList<>();
            switch (item.getTableInfo().getBottleService().name()) {
                case BAR_SIGNATURE:
                    if(item.getReservationInfo() != null && item.getReservationInfo().getStatus().equals(STATUS_NO_SHOW_SIGNATURE)) {
                        barNoShowYes.add(item);
                    } else {
                        barNoShowNo.add(item);
                    }
                    break;
                case TABLE_SIGNATURE:
                    if(item.getReservationInfo() != null && item.getReservationInfo().getStatus().equals(STATUS_NO_SHOW_SIGNATURE)) {
                        tableNoShowYes.add(item);
                    } else {
                        tableNoShowNo.add(item);
                    }
                    break;
                case STANDUP_SIGNATURE:
                    if(item.getReservationInfo() != null && item.getReservationInfo().getStatus().equals(STATUS_NO_SHOW_SIGNATURE)) {
                        standUpNoShowYes.add(item);
                    } else {
                        standUpNoShowNo.add(item);
                    }
                    break;
                default:
                    break;
            }
            barList.addAll(barNoShowNo);barList.addAll(barNoShowYes);
            tableList.addAll(tableNoShowNo);tableList.addAll(tableNoShowYes);
            standUpList.addAll(standUpNoShowNo);standUpList.addAll(standUpNoShowYes);
        }
    }

    private void createAdapter(final List<TableWithSeating> eventList) {
        final SeatingListAdapter<TableWithSeating> adapter = new SeatingListAdapter<TableWithSeating>(this.getActivity(),
                R.layout.tables_layout, eventList) {
            @Override
            public void fillItem(final TableWithSeating item, final FindViewHolder holder) {
                String tableType = "";
                switch (item.getTableInfo().getBottleService().name()) {
                    case BAR_SIGNATURE:
                        tableType = "Bar #";
                        break;
                    case TABLE_SIGNATURE:
                        tableType = "Table #";
                        break;
                    case STANDUP_SIGNATURE:
                        tableType = "Stand-Up #";
                        break;
                    default:
                        break;
                }
                tableType += String.valueOf(item.getTableInfo().getPlaceNumber());
                holder.tableType.setText(tableType);

                if(item.getReservationInfo() != null) {
                    holder.emptyTableMenu.setVisibility(View.INVISIBLE);
                    holder.detailLayout.setVisibility(View.VISIBLE);

                    holder.seatingTitle.setText(item.getReservationInfo().getGuestInfo().getFullName());
                    String byDesc = "by " + item.getReservationInfo().getBookedBy().getFullName();
                    holder.seatingDesc.setText(byDesc);
                    if(item.getReservationInfo().getBookingNote() != null && !item.getReservationInfo().getBookingNote().isEmpty()) {
                        holder.seatingNote.setVisibility(View.VISIBLE);
                        holder.seatingNote.setText(item.getReservationInfo().getBookingNote());
                    } else {
                        holder.seatingNote.setVisibility(View.GONE);
                    }

                    if (item.getReservationInfo().getGuestInfo().getUserpic() != null && !item.getReservationInfo().getGuestInfo().getUserpic().isEmpty()) {
                        try {
                            fragmentUtils.getLoaderImage(item.getReservationInfo().getGuestInfo().getUserpic(),holder.seatingImg);
                        } catch (Exception ex) {
                            if (ex.getMessage() != null)
                                Log.e(TAG, ex.getMessage());
                        }
                    } else {
                        holder.seatingImg.setImageResource(R.drawable.logo_venue);
                    }

                    switch(item.getReservationInfo().getStatus().name()) {
                        case "APPROVED":
                            holder.released.setVisibility(View.GONE);
                            holder.counterLayout.setVisibility(View.GONE);
                            holder.arrivedBtn.setVisibility(View.VISIBLE);

                            holder.arrivedBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    itemHolder = holder;
                                    workingItem = item;

                                    Bundle args = new Bundle();
                                    args.putLong("reservationId", item.getReservationInfo().getId());
                                    args.putString("newState", ARRIVED_SIGNATURE);
                                    getLoaderManager().initLoader(CHANGE_STATE_INDEX, args, TablesFragment.this);

                                    holder.arrivedBtn.setVisibility(View.GONE);
                                }
                            });

                            break;
                        case "ARRIVED":
                            holder.arrivedBtn.setVisibility(View.GONE);
                            holder.released.setVisibility(View.GONE);

                            setCounterLayer(holder, item);

                            break;
                        case "RELEASED":
                            holder.arrivedBtn.setVisibility(View.GONE);
                            holder.counterLayout.setVisibility(View.GONE);
                            holder.released.setVisibility(View.VISIBLE);
                            break;
                        case "NO_SHOW":
                            holder.arrivedBtn.setVisibility(View.GONE);
                            holder.counterLayout.setVisibility(View.GONE);
                            holder.released.setVisibility(View.GONE);
                            break;
                    }
                    if(item.getReservationInfo().getStaff() == null || item.getReservationInfo().getStaff().size() == 0) {
                        holder.staffStatus.setText("Staff: No Assignment");
                        holder.staffLL.setVisibility(View.GONE);
                    } else {
                        holder.staffStatus.setText("Staff Assignment");
                        holder.staffLL.setVisibility(View.VISIBLE);

                        holder.staffLL.removeAllViews();
                        for(final StaffAssignment staff : item.getReservationInfo().getStaff()) {
                            final RelativeLayout rel = (RelativeLayout) LayoutInflater.from(getActivity()).inflate(R.layout.staff_fragment, null);
                            ((TextView) rel.getChildAt(0)).setText(staff.getName());
                            rel.getChildAt(1).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    rel.setVisibility(View.GONE);

                                    item.getReservationInfo().getStaff().remove(staff);
                                    if (item.getReservationInfo().getStaff().size() == 0) {
                                        holder.staffStatus.setText("Staff: No Assignment");
                                    }

                                    Bundle args = new Bundle();
                                    args.putLong("reservationId", item.getReservationInfo().getId());
                                    args.putSerializable("assignedStaff", staff);
                                    if(getLoaderManager().getLoader(UNASSIGN_STAFF_INDEX) == null) {
                                        getLoaderManager().initLoader(UNASSIGN_STAFF_INDEX, args, TablesFragment.this);
                                    } else {
                                        getLoaderManager().restartLoader(UNASSIGN_STAFF_INDEX, args, TablesFragment.this);
                                    }
                                }
                            });

                            holder.staffLL.addView(rel);
                        }
                    }
                } else {
                    holder.emptyTableMenu.setVisibility(View.VISIBLE);
                    holder.detailLayout.setVisibility(View.GONE);
                }

                holder.reservedTableMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popup = new PopupMenu(getActivity(), holder.reservedTableMenu);
                        popup.getMenuInflater().inflate(R.menu.reserved_table_menu, popup.getMenu());
                        popup.show();

                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                switch(String.valueOf(menuItem.getTitle())) {
                                    case "Move":
                                        movingReservation = true;
                                        movedReservationId = item.getReservationInfo().getId();

                                        break;
                                    case "Unassign":
                                        Bundle args = new Bundle();
                                        args.putLong("reservationId", item.getReservationInfo().getId());
                                        getLoaderManager().initLoader(UNASSIGN_INDEX, args, TablesFragment.this);

                                        item.setReservationInfo(null);
                                        holder.detailLayout.setVisibility(View.GONE);
                                        holder.emptyTableMenu.setVisibility(View.VISIBLE);
                                        break;
                                    case "No show":
                                        holder.detailLayout.setVisibility(View.GONE);
                                        holder.emptyTableMenu.setVisibility(View.VISIBLE);

                                        args = new Bundle();
                                        args.putLong("reservationId", item.getReservationInfo().getId());
                                        args.putString("newState", NO_SHOW_SIGNATURE);
                                        getLoaderManager().initLoader(CHANGE_STATE_INDEX, args, TablesFragment.this);

                                        break;
                                    case "Release":
                                        holder.detailLayout.setVisibility(View.GONE);
                                        holder.emptyTableMenu.setVisibility(View.VISIBLE);

                                        args = new Bundle();
                                        args.putLong("reservationId", item.getReservationInfo().getId());
                                        args.putString("newState", RELEASED_SIGNATURE);
                                        getLoaderManager().initLoader(CHANGE_STATE_INDEX, args, TablesFragment.this);

                                        break;
                                    case "Assign Staff":
                                        args = new Bundle();
                                        args.putLong("eventId", eventId);
                                        args.putSerializable("eventDate", displayedDate);
                                        args.putLong("reservationId", item.getReservationInfo().getId());
                                        availableStaffFragment.setArguments(args);
                                        fragmentUtils.changeFragmentContent(fragmentManager, availableStaffFragment, R.id.content, false);

                                        break;
                                    default:
                                        break;
                                }
                                return true;
                            }
                        });
                    }
                });

                holder.emptyTableMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popup = new PopupMenu(getActivity(), holder.emptyTableMenu);
                        popup.getMenuInflater().inflate(R.menu.empty_table_menu, popup.getMenu());
                        popup.show();

                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                switch (String.valueOf(menuItem.getTitle())) {
                                    case "Assign":
                                        Bundle args = new Bundle();
                                        args.putSerializable("displayedDate", displayedDate);
                                        args.putString("bsType", item.getTableInfo().getBottleService().name());
                                        args.putLong("eventId", eventId);
                                        args.putSerializable("tableInfo", item.getTableInfo());
                                        reservationListFragment.setArguments(args);
                                        fragmentUtils.changeFragmentContent(fragmentManager, reservationListFragment, R.id.content);

                                        break;
                                    case "New Reservation":
                                        args = new Bundle();
                                        args.putSerializable("displayedDate", displayedDate);
                                        args.putLong("eventId", eventId);
                                        args.putSerializable("tableInfo", item.getTableInfo());
                                        args.putString("eventName", eventName);
                                        newReservationFragment.setArguments(args);
                                        fragmentUtils.changeFragmentContent(fragmentManager, newReservationFragment, R.id.content);

                                        break;
                                    default:
                                        break;
                                }
                                return true;
                            }
                        });
                    }
                });

                holder.main.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(movingReservation) {
                            movingReservation = false;

                            Bundle args = new Bundle();
                            args.putLong("reservationId", movedReservationId);
                            args.putSerializable("tableInfo", item.getTableInfo());

                            getLoaderManager().initLoader(MOVE_RESERVATION_INDEX, args, TablesFragment.this);
                        }
                    }
                });
            }
        };
        adapter.notifyDataSetChanged();

        seatingListView.setAdapter(adapter);
    }

    private String formattedDateText(DateTime date, int format) {
        if(date != null) {
            DateTimeFormatter dateFormat;

            switch(format) {
                case 0:
                    dateFormat = DateTimeFormat.forPattern("EEEE MMM d, yyyy");
                    break;
                case 1:
                    dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd");
                    break;
                default:
                    return "";
            }

            return dateFormat.print(date);
        } else {
            return "";
        }
    }

    private void setCounterLayer(final SeatingListAdapter.FindViewHolder holder, final TableWithSeating item) {
        holder.counterLayout.setVisibility(View.VISIBLE);

        holder.womanCount.setText(String.valueOf(item.getReservationInfo().getArrivedGirls()));
        holder.manCount.setText(String.valueOf(item.getReservationInfo().getArrivedGuys()));
        holder.allCount.setText(String.valueOf(item.getReservationInfo().getArrivedGuests()) + " / " + String.valueOf(item.getReservationInfo().getTotalGuests()));

        holder.womanInc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer incWoman =  (item.getReservationInfo().getArrivedGirls() + 1);
                item.getReservationInfo().setArrivedGirls(incWoman);
                holder.womanCount.setText(String.valueOf(incWoman));

                Integer incAll = (item.getReservationInfo().getArrivedGuests() + 1);
                item.getReservationInfo().setArrivedGuests(incAll);
                holder.allCount.setText(String.valueOf(incAll) + " / " + String.valueOf(item.getReservationInfo().getTotalGuests()));

                Bundle args = new Bundle();
                args.putLong("reservationId", item.getReservationInfo().getId());
                args.putString("action", GUEST_CHANGE_INC_SIGNATURE);
                args.putString("gender", GUEST_CHANGE_FEMALE_SIGNATURE);
                if(getLoaderManager().getLoader(GUEST_CHANGE_INDEX) == null) {
                    getLoaderManager().initLoader(GUEST_CHANGE_INDEX, args, TablesFragment.this);
                } else {
                    getLoaderManager().restartLoader(GUEST_CHANGE_INDEX, args, TablesFragment.this);
                }
            }
        });

        holder.womanDec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getReservationInfo().getArrivedGuests() == 0) {
                    itemHolder = holder;

                    Bundle args = new Bundle();
                    args.putLong("reservationId", item.getReservationInfo().getId());
                    args.putString("newState", APPROVED_SIGNATURE);
                    getLoaderManager().initLoader(CHANGE_STATE_INDEX, args, TablesFragment.this);
                }

                if (item.getReservationInfo().getArrivedGirls() > 0) {
                    Integer decWoman = (item.getReservationInfo().getArrivedGirls() - 1);
                    item.getReservationInfo().setArrivedGirls(decWoman);
                    holder.womanCount.setText(String.valueOf(decWoman));

                    Integer decAll =  (item.getReservationInfo().getArrivedGuests() - 1);
                    item.getReservationInfo().setArrivedGuests(decAll);
                    holder.allCount.setText(String.valueOf(decAll) + " / " + String.valueOf(item.getReservationInfo().getTotalGuests()));

                    Bundle args = new Bundle();
                    args.putLong("reservationId", item.getReservationInfo().getId());
                    args.putString("action", GUEST_CHANGE_DEC_SIGNATURE);
                    args.putString("gender", GUEST_CHANGE_FEMALE_SIGNATURE);
                    if(getLoaderManager().getLoader(GUEST_CHANGE_INDEX) == null) {
                        getLoaderManager().initLoader(GUEST_CHANGE_INDEX, args, TablesFragment.this);
                    } else {
                        getLoaderManager().restartLoader(GUEST_CHANGE_INDEX, args, TablesFragment.this);
                    }
                }
            }
        });

        holder.manInc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer incMan = (item.getReservationInfo().getArrivedGuys() + 1);
                item.getReservationInfo().setArrivedGuys(incMan);
                holder.manCount.setText(String.valueOf(incMan));

                Integer incAll = (item.getReservationInfo().getArrivedGuests() + 1);
                item.getReservationInfo().setArrivedGuests(incAll);
                holder.allCount.setText(String.valueOf(incAll) + " / " + String.valueOf(item.getReservationInfo().getTotalGuests()));

                Bundle args = new Bundle();
                args.putLong("reservationId", item.getReservationInfo().getId());
                args.putString("action", GUEST_CHANGE_INC_SIGNATURE);
                args.putString("gender", GUEST_CHANGE_MALE_SIGNATURE);
                if(getLoaderManager().getLoader(GUEST_CHANGE_INDEX) == null) {
                    getLoaderManager().initLoader(GUEST_CHANGE_INDEX, args, TablesFragment.this);
                } else {
                    getLoaderManager().restartLoader(GUEST_CHANGE_INDEX, args, TablesFragment.this);
                }
            }
        });

        holder.manDec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getReservationInfo().getArrivedGuests() == 0) {
                    itemHolder = holder;

                    Bundle args = new Bundle();
                    args.putLong("reservationId", item.getReservationInfo().getId());
                    args.putString("newState", APPROVED_SIGNATURE);
                    getLoaderManager().initLoader(CHANGE_STATE_INDEX, args, TablesFragment.this);
                }

                if (item.getReservationInfo().getArrivedGuys() > 0) {
                    Integer decMan = (Integer) (item.getReservationInfo().getArrivedGuys() - 1);
                    item.getReservationInfo().setArrivedGuys(decMan);
                    holder.manCount.setText(String.valueOf(decMan));

                    Integer decAll = (Integer) (item.getReservationInfo().getArrivedGuests() - 1);
                    item.getReservationInfo().setArrivedGuests(decAll);
                    holder.allCount.setText(String.valueOf(decAll) + " / " + String.valueOf(item.getReservationInfo().getTotalGuests()));

                    Bundle args = new Bundle();
                    args.putLong("reservationId", item.getReservationInfo().getId());
                    args.putString("action", GUEST_CHANGE_DEC_SIGNATURE);
                    args.putString("gender", GUEST_CHANGE_MALE_SIGNATURE);
                    if(getLoaderManager().getLoader(GUEST_CHANGE_INDEX) == null) {
                        getLoaderManager().initLoader(GUEST_CHANGE_INDEX, args, TablesFragment.this);
                    } else {
                        getLoaderManager().restartLoader(GUEST_CHANGE_INDEX, args, TablesFragment.this);
                    }
                }
            }
        });
    }

    private void showToast(String message){
        Toast toast = Toast.makeText(getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }
}
