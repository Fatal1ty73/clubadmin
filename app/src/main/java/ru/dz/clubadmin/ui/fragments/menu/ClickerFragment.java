package ru.dz.clubadmin.ui.fragments.menu;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.CurrentClickerHumanStats;
import ru.dz.clubadmin.domain.CurrentClickerStats;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.loaders.clicker.ClickerLoader;
import ru.dz.clubadmin.loaders.clicker.UpdateClickerStatsLoader;
import ru.dz.clubadmin.ui.fragments.events.EventsFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikitakulagin on 27.04.16.
 */
public class ClickerFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {

    @Inject
    private EventsFragment eventsFragment;
    @Inject
    FragmentUtils fragmentUtils;
    @InjectView(R.id.man_inc_id)
    ImageButton manIncButton;
    @InjectView(R.id.man_dec_id)
    ImageButton manDecButton;
    @InjectView(R.id.woman_dec_id)
    ImageButton womanDecButton;
    @InjectView(R.id.woman_inc_id)
    ImageButton womanIncButton;
    @InjectView(R.id.woman_counter_id)
    TextView womanCounterText;
    @InjectView(R.id.man_counter_id)
    TextView manCounterText;
    @InjectView(R.id.counter_oval_text_id)
    TextView counterText;
    @InjectView(R.id.total_text_id)
    TextView totalTextInfo;
    @InjectView(R.id.menu_button_id)
    ImageButton mainMenuButton;
    @InjectView(R.id.call_button_id)
    ImageButton callButton;
    @InjectView(R.id.total_women_text_percent_id)
    TextView womenPercentTextView;
    @InjectView(R.id.total_men_text_percent_id)
    TextView menPercentTextView;
    @InjectView(R.id.total_capacity_text_percent_id)
    TextView capacityPercentTextView;
    @InjectView(R.id.women_total_text_id)
    TextView womenTotalTextView;
    @InjectView(R.id.men_total_text_id)
    TextView menTotalTextView;
    @InjectView(R.id.capacity_total_text_id)
    TextView capacityTotalTextView;
    @InjectView(R.id.counter_oval_id)
    ImageView sendCurrentValuesButton;
    @InjectView(R.id.calendar_clicker_button)
    ImageButton eventsButton;
    @Inject
    LokerScreenFragment lokerScreenFragment;
    @InjectView(R.id.clicker_type_spinner_id)
    Spinner clickerType;
    @Inject
    private VenueHelper venueHelper;
    private List<String> arraySpinner = new ArrayList<String>() {{
        add("In Only");
        add("Out Only");
        add("Both");
    }};
    private FragmentManager fragmentManager;
    private Long manCounter = 0L;
    private Long womanCounter = 0L;
    private Long counter = manCounter + womanCounter;
    private Integer totalMen = 0;
    private Integer totalWomen = 0;
    private Integer totalIn = 0;
    private Integer totalOut = 0;
    private Long venueId = null;
    private String TAG = "ClickerFragment";
    private Long eventId = null;
    private CurrentClickerHumanStats stats = new CurrentClickerHumanStats();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_clicker, null);
    }
    private double onePercent() {
        return (new Double(totalMen) + new Double(totalWomen)) / 100 ;
    }
    private long currentMenPercent() {
        if (onePercent() != 0) {
            double doublePercent = totalMen / onePercent();
            Long iPart = (long) doublePercent;
            double rest = doublePercent - iPart;
            if (rest < 0.5) {
                return iPart;
            } else return iPart + 1;
        } else return 0;
    }
    private long currentWomenPercent() {
        if (onePercent() != 0) {
            double doublePercent = totalWomen / onePercent();
            Long iPart = (long) doublePercent;
            double rest = doublePercent - iPart;
            if (rest < 0.5) {
                return iPart;
            } else return iPart + 1;
        } else return 0;
    }
    private void incMan() {
        sendCurrentValuesButton.setBackgroundResource(R.drawable.counter_oval);
        counterText.setVisibility(View.VISIBLE);
        manCounter = manCounter + 1;
        counter = counter + 1;
        manCounterText.setText(manCounter.toString());
        counterText.setText(counter.toString());
    }
    private void decMan() {
        if (totalMen > 0 ) {
            sendCurrentValuesButton.setBackgroundResource(R.drawable.counter_oval);
            counterText.setVisibility(View.VISIBLE);
            manCounter = manCounter - 1;
            counter = counter - 1;
            manCounterText.setText(
                    manCounter.toString()
            );
            counterText.setText(counter.toString());
        }
    }
    private void incWoman() {
        sendCurrentValuesButton.setBackgroundResource(R.drawable.counter_oval);
        counterText.setVisibility(View.VISIBLE);
        womanCounter = womanCounter + 1;
        counter = counter + 1;
        womanCounterText.setText(womanCounter.toString());
        counterText.setText(counter.toString());
    }
    private void decWoman() {
        if (totalWomen > 0 ) {
            sendCurrentValuesButton.setBackgroundResource(R.drawable.counter_oval);
            counterText.setVisibility(View.VISIBLE);
            womanCounter = womanCounter - 1;
            counter = counter - 1;
            counterText.setText(counter.toString());
            womanCounterText.setText(womanCounter.toString());
        }
    }
    private void fillTotalInfo() {
        menPercentTextView.setText(currentMenPercent() + "%");
        Log.d(TAG, "one percent " + onePercent());
        Log.d(TAG, "men percent " + currentMenPercent());
        menTotalTextView.setText("Men \n" + totalMen.toString());
        womenTotalTextView.setText("Women \n" + totalWomen.toString());
        womenPercentTextView.setText(currentWomenPercent() + "%");
        Log.d(TAG, "women percent " + currentWomenPercent());
        totalTextInfo.setText(String.format("Total: %d out / %d in", totalOut, totalIn));
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        fillTotalInfo();
        venueId = Long.valueOf(venueHelper.getVenue().getId());
        womenPercentTextView.setText(String.format("%s", currentWomenPercent()));
        menPercentTextView.setText(String.format("%s", currentMenPercent()));
        Log.i(TAG, "venueId = " + venueId);
        womanCounterText.setText(womanCounter.toString());
        manCounterText.setText(manCounter.toString());
        counterText.setText(counter.toString());
        fragmentManager = getFragmentManager();
        manIncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incMan();
            }
        });
        manDecButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decMan();
            }
        });
        womanIncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incWoman();
            }
        });
        womanDecButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decWoman();
            }
        });
        eventsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putInt("backView", R.layout.fragment_clicker);
                eventsFragment.setArguments(args);
                fragmentUtils.changeFragmentContent(fragmentManager, eventsFragment, R.id.content);
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getActivity(),
                R.layout.dropbox_item, arraySpinner);
        adapter.setDropDownViewResource(R.layout.clicker_spinner_item);
        clickerType.setAdapter(adapter);
        clickerType.setSelection(2);
        clickerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                TextView currentClickerStat = (TextView) parent.getChildAt(0);
//                Object item = parent.getItemAtPosition(position);
//                Spanned textSpan  =  android.text.Html.fromHtml("<u>" + item.toString() + "</u>");
//                ((TextView)view).setText(textSpan);
//                ((TextView)view).setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGreySendText));
                switch (position) {
                    case 1:
                        manIncButton.setClickable(false);
                        manIncButton.setBackgroundResource(R.drawable.man_inc_unclick);
                        womanIncButton.setClickable(false);
                        womanIncButton.setBackgroundResource(R.drawable.woman_inc_unclick);
                        manDecButton.setClickable(true);
                        manDecButton.setBackgroundResource(R.drawable.man_decrement);
                        womanDecButton.setClickable(true);
                        womanDecButton.setBackgroundResource(R.drawable.woman_out_button);
                        break;
                    case 0:
                        manDecButton.setClickable(false);
                        manDecButton.setBackgroundResource(R.drawable.man_dec_unclick);
                        womanDecButton.setClickable(false);
                        womanDecButton.setBackgroundResource(R.drawable.woman_dec_unclick);
                        manIncButton.setClickable(true);
                        manIncButton.setBackgroundResource(R.drawable.man_increment);
                        womanIncButton.setClickable(true);
                        womanIncButton.setBackgroundResource(R.drawable.woman_in_button);
                        break;
                    case 2:
                        manDecButton.setClickable(true);
                        manDecButton.setBackgroundResource(R.drawable.man_decrement);
                        womanDecButton.setClickable(true);
                        womanDecButton.setBackgroundResource(R.drawable.woman_out_button);
                        manIncButton.setClickable(true);
                        manIncButton.setBackgroundResource(R.drawable.man_increment);
                        womanIncButton.setClickable(true);
                        womanIncButton.setBackgroundResource(R.drawable.woman_in_button);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (mainMenuButton!=null) {
            fragmentUtils.setMenu(mainMenuButton, view);
        }
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentUtils.changeFragmentContent(fragmentManager, lokerScreenFragment, R.id.content, false);

            }
        });
        if (venueId != null) {
            getLoaderManager().initLoader(R.id.clicker_loader, null, this);
        }
        sendCurrentValuesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = getArguments();
                if((args != null)&(args.containsKey("eventId"))) {
                    eventId = args.getLong("eventId");
                    Log.d(TAG, "EVENT_ID: " + eventId.toString());
                    stats.setMen(manCounter);
                    stats.setWomen(womanCounter);
                    getLoaderManager().initLoader(R.id.put_clicker_stats_loader, null, ClickerFragment.this);
                    manCounter = 0L;
                    womanCounter = 0L;
                    counter = 0L;
                    menTotalTextView.setText("Men \n" + totalMen.toString());
                    womenTotalTextView.setText("Women \n" + totalWomen.toString());
                    manCounterText.setText(manCounter.toString());
                    womanCounterText.setText(womanCounter.toString());
                    sendCurrentValuesButton.setBackgroundResource(R.drawable.checked_total_oval);
                    counterText.setVisibility(View.INVISIBLE);
//                fillTotalInfo();
                    sendCurrentValuesButton.setClickable(false);
                } else {
                    showToast("pick event");
                }
            }
        });
    }
    private void showToast(String message) {
        Toast toast = Toast.makeText(getActivity(),
                message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "Start loader");
        //TODO Заменить eventId на нужный

        switch (id) {
            case R.id.clicker_loader:
                Log.i(TAG, "Find id ClickerLoader");
                return new ClickerLoader(this.getActivity(), this.venueId, eventId);
            case R.id.put_clicker_stats_loader:
                Log.i(TAG, "Find id PutClickerStatsLoader");
                return new UpdateClickerStatsLoader(this.getActivity(), this.venueId, this.stats, eventId);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        int id = loader.getId();
        if (id == R.id.clicker_loader) {
            if (data.isSuccess()) {
                SuccessResponse<CurrentClickerStats> response = data.asSuccess();
                CurrentClickerStats responseMessage = response.getObj();
                if (responseMessage != null) {
                    Log.d(TAG, responseMessage.getTotalIn().toString());
                }
                loadStats(responseMessage);
            } else {
                ErrorMessage errorMessage = data.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
            }
        }
        if (id == R.id.put_clicker_stats_loader) {
            if (data.isSuccess()) {
                SuccessResponse<ResponseMessage> response = data.asSuccess();
                ResponseMessage msg = response.getObj();
                if (msg != null) {
                    Log.d(TAG, msg.getResponse());
                }
                manCounter = 0L;
                womanCounter = 0L;
                counter = 0L;
//                menTotalTextView.setText("Men \n" + totalMen.toString());
//                womenTotalTextView.setText("Women \n" + totalWomen.toString());
                manCounterText.setText(manCounter.toString());
                womanCounterText.setText(womanCounter.toString());
                sendCurrentValuesButton.setBackgroundResource(R.drawable.checked_total_oval);
                counterText.setVisibility(View.INVISIBLE);
//                fillTotalInfo();
                getLoaderManager().initLoader(R.id.clicker_loader, null, this);
                sendCurrentValuesButton.setClickable(true);
            } else {
                sendCurrentValuesButton.setClickable(true);
                ErrorMessage errorMessage = data.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
            }
        }
        getLoaderManager().destroyLoader(id);
    }
    private void loadStats(CurrentClickerStats stats) {
        totalMen = stats.getMen();
        totalWomen = stats.getWomen();
        totalOut = stats.getTotalOut();
        totalIn = stats.getTotalIn();
        fillTotalInfo();
//        womanCounterText.setText(stats.getWomen().toString());
//        manCounterText.setText(stats.getMen().toString());
    }
    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }
}
