package ru.dz.clubadmin.ui.fragments.tables;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.TableInfo;
import ru.dz.clubadmin.domain.TablesList;
import ru.dz.clubadmin.domain.models.BottleServiceTypeEnum;
import ru.dz.clubadmin.domain.models.CreateTableRequest;
import ru.dz.clubadmin.loaders.tables.CreateTableRequestLoader;
import ru.dz.clubadmin.loaders.tables.DeleteSeatingPlaceLoader;
import ru.dz.clubadmin.loaders.tables.TablesLoader;
import ru.dz.clubadmin.ui.widgets.TableArrayAdapter;

import java.util.ArrayList;
import java.util.List;

public class AddTableFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {
    @InjectView(R.id.bottle_service_spinner_id)
    Spinner bottleServiceSpinner;
    @Inject
    ApiKeyHelper apiKeyHelper;
    private Long venueId = null;
    private String TAG = "AddTableFragment";

    @Inject
    private VenueHelper venueHelper;
    @InjectView(R.id.table_list_view)
    ListView tableListView;
    @InjectView(R.id.accept_employee_id)
    Button button;
    @InjectView(R.id.table_number_id)
    EditText tableNumber;
    private List<TablesList> tablesLists = new ArrayList<>();
    private TablesList tables = new TablesList();
    private TablesList standups = new TablesList();
    private TablesList bar = new TablesList();
    private CreateTableRequest tableRequest;
    private long seatingId;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_table, null);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
            List<String> serviceList = new ArrayList<>();
            for (final BottleServiceTypeEnum serviceType: BottleServiceTypeEnum.values()) {
                serviceList.add(serviceType.toString());
            }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getActivity(),
                R.layout.dropbox_item, serviceList);
        adapter.setDropDownViewResource(R.layout.clicker_spinner_item);
        bottleServiceSpinner.setAdapter(adapter);
        venueId = Long.valueOf(venueHelper.getVenue().getId());
        getLoaderManager().initLoader(R.id.tables_loader, null,AddTableFragment.this);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tableNumber.getText().toString().equals("")) {
                    TableInfo tableInfo = new TableInfo();
                    tableRequest = new CreateTableRequest();
                    tableRequest.setId(0L);
                    tableRequest.setPlaceNumber(Integer.valueOf(tableNumber.getText().toString()));
                    tableRequest.setVenueId(venueId);
                    switch (bottleServiceSpinner.getSelectedItem().toString()) {
                        case "Table":
                            tableRequest.setBottleServiceType(BottleServiceTypeEnum.TABLE);
//                            tableInfo.setBottleService(BottleServiceTypeEnum.TABLE);
//                            tableInfo.setPlaceNumber(Integer.valueOf(tableNumber.getText().toString()));
//                            tables.getTablesList().add(tableInfo);
                            break;
                        case "Standup":
//                            tableInfo.setBottleService(BottleServiceTypeEnum.STANDUP);
//                            tableInfo.setPlaceNumber(Integer.valueOf(tableNumber.getText().toString()));
//                            standups.getTablesList().add(tableInfo);
                            tableRequest.setBottleServiceType(BottleServiceTypeEnum.STANDUP);
                            break;
                        case "Bar":
//                            tableInfo.setBottleService(BottleServiceTypeEnum.BAR);
//                            tableInfo.setPlaceNumber(Integer.valueOf(tableNumber.getText().toString()));
//                            bar.getTablesList().add(tableInfo);
                            tableRequest.setBottleServiceType(BottleServiceTypeEnum.BAR);
                            break;
                    }
                    getLoaderManager().initLoader(R.id.create_table_loader, null, AddTableFragment.this);
                    showToast("Create table request send");
//                    setLoaderData();

                } else {
                  showToast("Please enter table number");
                }
            }
        });
    }
    private void showToast(String message) {
        Toast toast = Toast.makeText(this.getActivity(),
                message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }
    private TableArrayAdapter<TablesList> createAdapter(final List<TablesList> tablesLists, final int layout) {
        return new TableArrayAdapter<TablesList>(this.getActivity(), layout, tablesLists) {
            @Override
            public void fillItem(TablesList item, ViewHolder holder) {
                if (holder != null) {
                    holder.header.setText(item.getTablesList().get(0).getBottleService().toString());
                    for (final TableInfo table: item.getTablesList()) {
                        GridLayout.LayoutParams params = new GridLayout.LayoutParams();
                        params.width = (getActivity().getResources().getDisplayMetrics().widthPixels/3);
                        params.height = 90;
                        final RelativeLayout rel = (RelativeLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_table, null);
                        Log.i("!!", "ADD VIEW");
                        rel.setLayoutParams(params);
                        ((TextView)rel.getChildAt(0)).setText(Html.fromHtml("Table#" + "<b><font color=#000>" + table.getPlaceNumber() + "</font></b>"));
                        ((ImageButton)rel.getChildAt(1)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                seatingId = table.getId();
                                getLoaderManager().initLoader(R.id.delete_seating_place_loader, null, AddTableFragment.this);
                                GridLayout grid = (GridLayout)rel.getParent();
                                LinearLayout lLayout = (LinearLayout) grid.getParent();
                                switch (table.getBottleService()) {
                                    case BAR:
                                        grid.removeView(rel);
                                        if (grid.getChildCount() == 0) {
                                            lLayout.removeViewAt(0);
                                        }
                                        bar.getTablesList().remove(table);
                                        break;
                                    case TABLE:
                                        grid.removeView(rel);
                                        if (grid.getChildCount() == 0) {
                                            lLayout.removeViewAt(0);
                                        }
                                        tables.getTablesList().remove(table);
                                        break;
                                    case STANDUP:
                                        grid.removeView(rel);
                                        if (grid.getChildCount() == 0) {
                                            lLayout.removeViewAt(0);
                                        }
                                        standups.getTablesList().remove(table);
                                        break;
                                }
                            }
                        });
                        holder.tablesGrid.addView(rel);

                        Log.i("!!", String.valueOf(holder.tablesGrid.getChildCount()));
                    }
                }
            }
        };
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.tables_loader:
                return new TablesLoader(this.getActivity(), this.venueId);
            case R.id.create_table_loader:
                return new CreateTableRequestLoader(this.getActivity(), tableRequest);
            case R.id.delete_seating_place_loader:
                return new DeleteSeatingPlaceLoader(this.getActivity(), seatingId);
            default:
                return null;
        }

    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        int id = loader.getId();
        if (id == R.id.tables_loader) {
            if (data.isSuccess()) {
                SuccessResponse<TablesList> response = data.asSuccess();
                TablesList responseMessage = response.getObj();
                loadStats(responseMessage);
            } else {
                ErrorMessage errorMessage = data.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
            }
        }
        if (id == R.id.create_table_loader) {
            if (data.isSuccess()) {
                SuccessResponse<ResponseMessage> response = data.asSuccess();
                ResponseMessage responseMessage = response.getObj();
                getLoaderManager().initLoader(R.id.tables_loader, null,AddTableFragment.this);
            } else {
                ErrorMessage errorMessage = data.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
            }
        }
        if (id == R.id.delete_seating_place_loader) {
            if (data.isSuccess()) {
                getLoaderManager().initLoader(R.id.tables_loader, null,AddTableFragment.this);
            } else {
                ErrorMessage errorMessage = data.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
            }        }
        getLoaderManager().destroyLoader(id);
    }
    public void loadStats(TablesList list) {
        bar.getTablesList().clear();
        tables.getTablesList().clear();
        standups.getTablesList().clear();
        for (TableInfo table: list.getTablesList()) {
            switch (table.getBottleService()) {
                case BAR:
                    bar.getTablesList().add(table);
                    break;
                case TABLE:
                    tables.getTablesList().add(table);
                    break;
                case STANDUP:
                    standups.getTablesList().add(table);
                    break;
            }
        }
        setLoaderData();
    }
    public void setLoaderData() {
        tablesLists.clear();
        if (tables.getTablesList().size()>0) {
            tablesLists.add(tables);
        }
        if (bar.getTablesList().size()>0) {
            tablesLists.add(bar);
        }
        if (standups.getTablesList().size()>0) {
            tablesLists.add(standups);
        }
        tableListView.setAdapter(createAdapter(tablesLists, R.layout.fragment_table_list));
    }
    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }
}
