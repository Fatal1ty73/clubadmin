package ru.dz.clubadmin.ui.fragments.employee;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SearchView;
import com.google.inject.Inject;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import roboguice.util.Strings;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.SuccessResponse;
import ru.dz.clubadmin.database.venues.VenueHelper;
import ru.dz.clubadmin.domain.ErrorMessage;
import ru.dz.clubadmin.domain.VenuePeopleList;
import ru.dz.clubadmin.domain.VenueStaffInfo;
import ru.dz.clubadmin.loaders.venue.EmployeeLoader;
import ru.dz.clubadmin.ui.utils.FragmentUtils;
import ru.dz.clubadmin.ui.utils.SeparatedListAdapter;
import ru.dz.clubadmin.ui.widgets.EmployeeListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikitakulagin on 12.05.16.
 */
public class EmployeeFragment extends RoboFragment implements LoaderManager.LoaderCallbacks<BaseResponse> {
    @InjectView(R.id.employee_tab_layout)
    TabLayout employeesTabLayout;
    @InjectView(R.id.searchEmployeeView)
    SearchView searchEmployee;
    @InjectView(R.id.menu_button_id)
    ImageButton mainMenu;
    @Inject
    FragmentUtils fragmentUtils;
    private List<VenueStaffInfo> requestList;

    private List<VenueStaffInfo> approvedList;

    private List<VenueStaffInfo> previousList;
    @InjectView(R.id.pager)
    private ViewPager viewPager;
    private String TAG = "EmployeeFragment";
    private Long venueId = null;
    @Inject
    private EmployeeDetailsFragment employeeDetailsFragment;
    @Inject
    private EmployeeListFragment listFragment;
    @Inject
    private FragmentManager fragmentManager;
    @Inject
    private VenueHelper venueHelper;
    private DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yy");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_employee_list, null);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        venueId = Long.valueOf(venueHelper.getVenue().getId());
        employeesTabLayout.addTab(employeesTabLayout.newTab().setText("All"));
        employeesTabLayout.addTab(employeesTabLayout.newTab().setText("Current"));
        employeesTabLayout.addTab(employeesTabLayout.newTab().setText("Requesting"));
        getLoaderManager().initLoader(R.id.venue_employees_loader, null, EmployeeFragment.this);
        if (mainMenu!=null) {
            fragmentUtils.setMenu(mainMenu, view);
        }
    }
    private EmployeeListAdapter<VenueStaffInfo> createAdapter(List<VenueStaffInfo> employees, final int layout) {
        return new EmployeeListAdapter<VenueStaffInfo>(this.getActivity(), layout, employees) {
            @Override
            public void fillItem(final VenueStaffInfo item, ViewHolder holder) {
                if (holder != null) {
                    final String employeeStatus;
                    String roles = Strings.join(", ", item.getPreferredRoles());
                    if (layout == R.layout.fragment_current_employee) {
                        holder.employeeRoles.setText("Roles: " + roles);
                        holder.workdAt.setText("Since: " + item.getSince().toString(formatter));
                        employeeStatus = "current";
                    } else {
                        holder.employeeRoles.setText("Requested Roles: \n"  + roles);
                        holder.workdAt.setText(Html.fromHtml("Requested On: " + "<b>" + item.getSince().toString(formatter) + "</b>"));
                        employeeStatus = "requested";
                    }
                    holder.employeeLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle args = new Bundle();
                            args.putSerializable("employee", item);
                            args.putString("status", employeeStatus);
                            employeeDetailsFragment.setArguments(args);
                            fragmentUtils.changeFragmentContent(fragmentManager, employeeDetailsFragment, R.id.content, false);
                        }
                    });
                    holder.employeeName.setText(item.getFullName());
                }
            }
        };
    }
    private SeparatedListAdapter createSepAdapter() {
        return new SeparatedListAdapter(this.getActivity(), R.layout.list_header);
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "Start loader");
        switch (id) {
            case R.id.venue_employees_loader:
                Log.i(TAG, "Find id EmployeesLoader");
                return new EmployeeLoader(this.getActivity(), this.venueId);
            default:
                return null;
        }

    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        int id = loader.getId();
        if (id == R.id.venue_employees_loader) {
            if (data.isSuccess()) {
                SuccessResponse<VenuePeopleList> response = data.asSuccess();
                VenuePeopleList responseMessage = response.getObj();
                loadStats(responseMessage);
            } else {
                ErrorMessage errorMessage = data.asError().getObj();
                Log.e(TAG, "CLIENT_ERROR error:" + errorMessage.getError() + " Fields: " + errorMessage.getFields());
            }
        }
        getLoaderManager().destroyLoader(id);
    }
    private void loadStats(VenuePeopleList employees) {
        requestList = employees.getRequestList();

        approvedList = employees.getApprovedList();

        previousList = employees.getPreviousList();
        final ViewPagerEmployeeAdapter adapter = new ViewPagerEmployeeAdapter(getChildFragmentManager());
        adapter.addFragment(listFragment);
        viewPager.setBackgroundColor(Color.BLACK);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(employeesTabLayout));
        employeesTabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager){
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                adapter.getItem(tab.getPosition());
                viewPager.setCurrentItem(tab.getPosition());
            }
        });
    }
    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }

    private class ViewPagerEmployeeAdapter extends FragmentPagerAdapter {
        private EmployeeListFragment employeeListFragment;
        SeparatedListAdapter adapter = createSepAdapter();
        public EmployeeListAdapter<VenueStaffInfo> currentAdapter = createAdapter(new ArrayList<VenueStaffInfo>(), R.layout.fragment_current_employee);
        public EmployeeListAdapter<VenueStaffInfo> requestedAdapter = createAdapter(new ArrayList<VenueStaffInfo>(), R.layout.fragment_requested_employee);
        public ViewPagerEmployeeAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            EmployeeItem item = EmployeeItem.getItem(position);
            switch (item) {
                case ALL:
                    currentAdapter.clear();
                    currentAdapter.addAll(approvedList);
                    currentAdapter.notifyDataSetChanged();
                    requestedAdapter.clear();
                    requestedAdapter.addAll(requestList);
                    requestedAdapter.notifyDataSetChanged();
                    if (approvedList.size() > 0 ) {
                        adapter.addSection("Current", currentAdapter);
                    }
                    if (requestList.size() > 0) {
                        adapter.addSection("Requested Employees", requestedAdapter);
                    }
                    employeeListFragment.setListAdapter(adapter);
                    break;
                case CURRENT:
                    currentAdapter.clear();
                    currentAdapter.addAll(approvedList);
                    currentAdapter.notifyDataSetChanged();
                    employeeListFragment.setListAdapter(currentAdapter);
                    break;
                case REQUESTING:
                    requestedAdapter.clear();
                    requestedAdapter.addAll(requestList);
                    requestedAdapter.notifyDataSetChanged();
                    employeeListFragment.setListAdapter(requestedAdapter);
                    break;
            }

            return employeeListFragment;
        }

        public void addFragment(EmployeeListFragment fragment) {
            employeeListFragment = fragment;
        }
        @Override
        public int getCount() {
            return (employeeListFragment != null)? 1: 0;
        }
    }
}
