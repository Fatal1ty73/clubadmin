package ru.dz.clubadmin.ui.fragments.account;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import com.google.inject.Inject;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.InjectView;
import ru.dz.clubadmin.R;
import ru.dz.clubadmin.VenueAuthActivity;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.models.UserSettings;
import ru.dz.clubadmin.ui.fragments.auth.AuthFragment;
import ru.dz.clubadmin.ui.utils.FragmentUtils;

public class AccountSettingFragment extends RoboFragment {
    @InjectView(R.id.back_to_account)
    private ImageButton backButton;
    @Inject
    private AuthFragment authActivity;
    @Inject
    private ApiKeyHelper apiKeyHelper;
    @Inject
    private ApplicationContext applicationContext;
    @Inject
    private FragmentUtils fragmentUtils;
    @Inject
    private MyAccountFragment myAccountFragment;
    @InjectView(R.id.post_on_my_behalf_switch_id)
    private Switch postOnMyBehalfSwitch;
    @InjectView(R.id.rsvp_on_my_behalf_switch_id)
    private Switch rsvpOnMyBehalfSwitch;
    @InjectView(R.id.check_in_on_my_behalf_switch_id)
    private Switch checkInOnMyBehalfSwitch;
    @InjectView(R.id.sms_messages_switch_id)
    private Switch smsMessageSwitch;
    @InjectView(R.id.in_app_notification_switch_id)
    private Switch inAppNotificationSwitch;
    private static final String TAG = "AccountSettingFragment";
    private FragmentManager fragmentManager;
    @InjectView(R.id.logout_text_view_id)
    private TextView logoutView;
    @InjectView(R.id.delete_profile_text_view_id)
    private TextView deleteView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings_account, null);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentUtils.changeFragmentContent(fragmentManager, myAccountFragment, R.id.venue_list);
                }
            });
        logoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiKeyHelper.clearApiKey();
                startActivity(new Intent(getActivity(), VenueAuthActivity.class));
                getActivity().finish();
//                fragmentUtils.changeFragmentContent(fragmentManager, authActivity, R.id.venue_list);
            }
        });
        UserSettings settings = new UserSettings();
        settings.setCheckInOnMyBehalf(true);
        settings.setInAppNotification(false);
        settings.setPostOnMyBehalf(true);
        settings.setRsvpOnMyBehalf(false);
        settings.setSmsMessages(true);
        fillSwitch(settings);
        deleteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rsvpOnMyBehalfSwitch.setChecked(true);
            }
        });

    }
    private void fillSwitch(UserSettings settings) {
        postOnMyBehalfSwitch.setChecked(settings.isPostOnMyBehalf());
        rsvpOnMyBehalfSwitch.setChecked(settings.isRsvpOnMyBehalf());
        checkInOnMyBehalfSwitch.setChecked(settings.isCheckInOnMyBehalf());
        smsMessageSwitch.setChecked(settings.isSmsMessages());
        inAppNotificationSwitch.setChecked(settings.isInAppNotification());
    }

    @Override
    public void onDestroyView() {
        //todo запрос на сервер с обновлением UserSettings
        super.onDestroyView();
    }
}
