package ru.dz.clubadmin.ui.widgets;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridLayout;
import android.widget.TextView;
import ru.dz.clubadmin.R;

import java.util.List;

/**
 * Created by nikitakulagin on 18.05.16.
 */
public abstract class TableArrayAdapter<T> extends ArrayAdapter<T> {
    private LayoutInflater mInflater;

    @LayoutRes
    private int resource;
    private List<T> list;

    public TableArrayAdapter(Context context, @LayoutRes int resource, List<T> list) {
        super(context, resource, list);
        this.resource = resource;
        this.list = list;
        mInflater = LayoutInflater.from(context);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(resource, parent, false);
            holder = new ViewHolder();
            holder.tablesGrid = (GridLayout) row.findViewById(R.id.tables_layout);
            holder.header = (TextView) row.findViewById(R.id.table_type_header);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        T item = this.list.get(position);
        fillItem(item, holder);
        return row;
    }
    public abstract void fillItem(T item, ViewHolder holder);

    public class ViewHolder {
        public GridLayout tablesGrid;
        public TextView header;
    }
}
