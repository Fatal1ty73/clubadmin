package ru.dz.clubadmin.database.users;


import android.support.annotation.NonNull;
import com.google.inject.Singleton;
import io.realm.Realm;
import io.realm.RealmResults;
import ru.dz.clubadmin.database.BaseHelper;
import ru.dz.clubadmin.domain.ApiKey;

@Singleton
public class ApiKeyHelper extends BaseHelper {

    public void saveApiKey(ApiKey apiKey) {
        Realm realm = getRealm();
        realm.beginTransaction();
        realm.clear(ApiKey.class);
        realm.copyToRealm(apiKey);
        realm.commitTransaction();
    }

    public void clearApiKey() {
        Realm realm = getRealm();
        realm.beginTransaction();
        realm.clear(ApiKey.class);
        realm.commitTransaction();
    }

    @NonNull
    public ApiKey getApiKey() {
        Realm realm = getRealm();
        RealmResults<ApiKey> results = realm.allObjects(ApiKey.class);
        if(!results.isEmpty()){
            return results.last();
        } else {
            return new ApiKey();
        }

    }
}
