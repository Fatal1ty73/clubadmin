package ru.dz.clubadmin.database;

import com.google.inject.Inject;
import io.realm.Realm;
import ru.dz.clubadmin.di.controller.ApplicationContext;


public class BaseHelper {
    @Inject
    private ApplicationContext applicationContext;

    protected Realm getRealm() {
        return applicationContext.provideRealm();
    }
}
