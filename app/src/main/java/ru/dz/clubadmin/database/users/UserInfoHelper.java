package ru.dz.clubadmin.database.users;


import android.support.annotation.NonNull;
import com.google.inject.Singleton;
import io.realm.Realm;
import io.realm.RealmResults;
import ru.dz.clubadmin.database.BaseHelper;
import ru.dz.clubadmin.domain.UserInfo;
@Singleton
public class UserInfoHelper extends BaseHelper {

    public void saveUserInfoContent(UserInfo response) {
        Realm realm = getRealm();
        realm.beginTransaction();
        realm.clear(UserInfo.class);
        realm.copyToRealm(response);
        realm.commitTransaction();
    }

    @NonNull
    public UserInfo getUserInfo() {
        Realm realm = getRealm();
        RealmResults<UserInfo> results = realm.allObjects(UserInfo.class);
        if(!results.isEmpty()){
            return results.last();
        } else {
            return new UserInfo();
        }
    }
}
