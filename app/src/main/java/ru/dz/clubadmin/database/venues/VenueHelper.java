package ru.dz.clubadmin.database.venues;

import android.support.annotation.NonNull;
import com.google.inject.Singleton;
import io.realm.Realm;
import io.realm.RealmResults;
import ru.dz.clubadmin.database.BaseHelper;
import ru.dz.clubadmin.domain.models.Venue;

/**
 * Created by nikitakulagin on 15.05.16.
 */
@Singleton
public class VenueHelper extends BaseHelper {

    public void saveCurrentVenue(Venue venue) {
        Realm realm = getRealm();
        realm.beginTransaction();
        realm.clear(Venue.class);
        realm.copyToRealm(venue);
        realm.commitTransaction();
    }

    public void clearVenue() {
        Realm realm = getRealm();
        realm.beginTransaction();
        realm.clear(Venue.class);
        realm.commitTransaction();
    }

    @NonNull
    public Venue getVenue() {
        Realm realm = getRealm();
        RealmResults<Venue> results = realm.allObjects(Venue.class);
        if (!results.isEmpty()) {
            return results.last();
        } else {
         return new Venue();
        }
    }
}
