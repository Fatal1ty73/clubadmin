package ru.dz.clubadmin.loaders.reservation;

import android.content.Context;

import com.google.inject.Inject;

import java.io.IOException;

import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ReservationsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.loaders.BaseLoader;

/**
 * Created by Sam (samir@peller.tech) on 08.07.2016.
 */
public class Unassign extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private long reservationId;

    public Unassign(Context context, long reservationId) {
        super(context);

        this.reservationId = reservationId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ReservationsService service = applicationContext.getReservationService();
        Response<ResponseMessage> messageResponse = service.unassignTable(reservationId, apiKey).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
