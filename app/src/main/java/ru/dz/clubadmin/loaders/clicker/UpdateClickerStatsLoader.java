package ru.dz.clubadmin.loaders.clicker;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ClickerService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.CurrentClickerHumanStats;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

/**
 * Created by nikitakulagin on 06.05.16.
 */
public class UpdateClickerStatsLoader extends BaseLoader {
    public Long getVenueId() {
        return venueId;
    }

    public void setVenueId(Long venueId) {
        this.venueId = venueId;
    }

    private Long venueId;
    private Long eventId;
    private CurrentClickerHumanStats stats;
    public UpdateClickerStatsLoader(Context context, Long venueId, CurrentClickerHumanStats stats, Long eventId) {
        super(context);
        this.stats = stats;
        this.venueId = venueId;
        this.eventId = eventId;
    }

    @Inject
    ApplicationContext applicationContext;
    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ClickerService service = applicationContext.getClickerService();
        Response<ResponseMessage> response = service.updateStats(apiKey, venueId,eventId, stats).execute();
        return BaseResponse.fromResponse(response);
    }
}
