package ru.dz.clubadmin.loaders.tables;

import android.content.Context;

import com.google.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.TablesService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.TableWithSeating;
import ru.dz.clubadmin.loaders.BaseLoader;

/**
 * Created by Sam (samir@peller.tech) on 27.05.2016.
 */
public class TablesWithSeatingLoader extends BaseLoader{
    private long venueId;
    private String date;
    private long eventId;

    @Inject
    private ApplicationContext applicationContext;

    public TablesWithSeatingLoader(Context context, long venueId, DateTime date, long eventId){
        super(context);

        this.venueId = venueId;
        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd");
        this.date = df.print(date);
        this.eventId = eventId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        TablesService service = applicationContext.getTablesService();
        Response<List<TableWithSeating>> response = service.getTablesWithSeating(venueId, date, eventId, apiKey).execute();

        return BaseResponse.fromResponse(response);
    }
}
