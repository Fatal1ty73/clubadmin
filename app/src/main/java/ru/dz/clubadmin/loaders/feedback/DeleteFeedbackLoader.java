package ru.dz.clubadmin.loaders.feedback;


import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.FeedbackService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class DeleteFeedbackLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;
    private Long feedbackId;
    public DeleteFeedbackLoader(Context context,  Long feedbackId) {
        super(context);
        this.feedbackId = feedbackId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        FeedbackService service = applicationContext.getFeedbackService();
        Response<ResponseMessage> messageResponse = service.deleteFeedback(this.feedbackId, apiKey).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
