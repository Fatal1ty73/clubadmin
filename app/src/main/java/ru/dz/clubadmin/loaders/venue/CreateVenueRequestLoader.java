package ru.dz.clubadmin.loaders.venue;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.VenueService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.VenueRequest;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class CreateVenueRequestLoader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;

    private final Long id;
    private final VenueRequest venueRequest;

    public CreateVenueRequestLoader(Context context, Long id, VenueRequest venueRequest) {
        super(context);
        this.id = id;
        this.venueRequest = venueRequest;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        VenueService service = applicationContext.getVenueService();
        Response<ResponseMessage> messageResponse = service.createVenueRequest(id, apiKey, venueRequest).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
