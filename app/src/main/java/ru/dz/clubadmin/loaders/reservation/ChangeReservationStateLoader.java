package ru.dz.clubadmin.loaders.reservation;

import android.content.Context;

import com.google.inject.Inject;

import java.io.IOException;

import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ReservationsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.loaders.BaseLoader;

/**
 * Created by Sam (samir@peller.tech) on 11.07.2016.
 */
public class ChangeReservationStateLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private long reservationId;
    private String newState;

    public ChangeReservationStateLoader(Context context, long reservationId, String newState) {
        super(context);

        this.reservationId = reservationId;
        this.newState = newState;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ReservationsService service = applicationContext.getReservationService();
        Response<ResponseMessage> messageResponse = service.changeReservationState(reservationId, getApiKey(), newState).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
