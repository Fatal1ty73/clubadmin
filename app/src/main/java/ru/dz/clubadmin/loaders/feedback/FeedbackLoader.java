package ru.dz.clubadmin.loaders.feedback;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.FeedbackService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.models.FeedbackList;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;


public class FeedbackLoader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;

    private String reservationId;

    public FeedbackLoader(Context context, String reservationId) {
        super(context);
        this.reservationId = reservationId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        FeedbackService service = applicationContext.getFeedbackService();
        Response<FeedbackList> messageResponse = service.getFeedback(apiKey, this.reservationId).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
