package ru.dz.clubadmin.loaders.users;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.PromoService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.models.Promoter;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;
import java.util.List;

/**
 * Created by nikitakulagin on 24.04.16.
 */
//public class PromotersLoader {
public class PromotersLoader extends BaseLoader {
    private Long venueId;

    @Inject
    private ApplicationContext applicationContext;

    public PromotersLoader(Context context, Long venueId) {
        super(context);
        this.venueId = venueId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        PromoService service = applicationContext.getPromoService();
        Response<List<Promoter>> response = service.getPromoters(apiKey, venueId).execute();
        return BaseResponse.fromResponse(response);
//
    }


}
