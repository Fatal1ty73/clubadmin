package ru.dz.clubadmin.loaders.schedule;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ScheduleService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ScheduleInfo;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class SheduleLoader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;

    private Long eventId;
    private String venueId;


    public SheduleLoader(Context context, Long eventId, String venueId) {
        super(context);
        this.eventId = eventId;
        this.venueId = venueId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ScheduleService service = applicationContext.getScheduleService();
        Response<ScheduleInfo> messageResponse = service.getUserSchedule(apiKey, venueId,  eventId).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
