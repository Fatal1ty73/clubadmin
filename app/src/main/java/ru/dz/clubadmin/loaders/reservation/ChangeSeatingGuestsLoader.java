package ru.dz.clubadmin.loaders.reservation;


import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ReservationsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.Gender;
import ru.dz.clubadmin.domain.models.ReservationAction;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class ChangeSeatingGuestsLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;
    private Long reservationId;
    private String reservationAction;
    private String gender;

    public ChangeSeatingGuestsLoader(Context context, long reservationId, String reservationAction, String gender) {
        super(context);
        this.reservationId = reservationId;
        this.reservationAction = reservationAction;
        this.gender = gender;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ReservationsService service = applicationContext.getReservationService();
        Response<ResponseMessage> messageResponse = service.changeSeatedGuests(this.reservationId, apiKey, this.reservationAction, this.gender).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
