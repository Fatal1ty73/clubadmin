package ru.dz.clubadmin.loaders.reports;


import android.content.Context;
import com.google.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ReportsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.models.ClickerReportPoint;
import ru.dz.clubadmin.domain.models.ReportTypeEnum;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;
import java.util.List;

public class ClickerReportLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;
    private Long venueId;
    private String dateFrom;
    private String dateTo;
    private ReportTypeEnum reportType;

    public ClickerReportLoader(Context context, Long venueId, DateTime dateFrom, DateTime dateTo, ReportTypeEnum reportType) {
        super(context);
        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd");

        this.venueId = venueId;
        this.dateFrom =  df.print(dateFrom);
        this.dateTo =  df.print(dateTo);
        this.reportType = reportType;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ReportsService service = applicationContext.getReportsService();
        Response<List<ClickerReportPoint>> messageResponse = service.getClickerReport(
                apiKey,
                this.venueId,
                this.dateTo,
                this.dateFrom,
                this.reportType).execute();
        return BaseResponse.fromResponse(messageResponse);
    }

}
