package ru.dz.clubadmin.loaders.feedback;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.FeedbackService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.FeedbackInfo;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class CreateFeedbackLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;
    private FeedbackInfo feedback;
    public CreateFeedbackLoader(Context context, FeedbackInfo feedback) {
        super(context);
        this.feedback = feedback;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        FeedbackService service = applicationContext.getFeedbackService();
        Response<ResponseMessage> messageResponse = service.createFeedback(apiKey, this.feedback).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
