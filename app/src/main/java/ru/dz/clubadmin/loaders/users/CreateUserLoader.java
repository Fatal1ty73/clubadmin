package ru.dz.clubadmin.loaders.users;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.UsersService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.User;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;


public class CreateUserLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;
    private User user;

    public CreateUserLoader(Context context, User user) {
        super(context);
        this.user = user;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        UsersService service = this.applicationContext.getUsersService();
        Response<ResponseMessage> messageResponse = service.createUser(this.user).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}