package ru.dz.clubadmin.loaders.search;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.SearchService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.SearchUserInfo;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;
import java.util.List;

public class SearchUserLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private String phonePart;
    private String venueId;
    private String namePart;

    public SearchUserLoader(Context context, String namePart, String phonePart, String venueId) {
        super(context);
        this.phonePart = phonePart;
        this.namePart = namePart;
        this.venueId = venueId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        SearchService service = applicationContext.getSearchService();
        Response<List<SearchUserInfo>> response = service.searchUser(apiKey, namePart, phonePart,venueId).execute();
        return BaseResponse.fromResponse(response);
    }
}
