package ru.dz.clubadmin.loaders.users;


import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.UsersService;
import ru.dz.clubadmin.database.users.UserInfoHelper;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.UserInfo;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class CurrentUserLoader extends BaseLoader {


    @Inject
    private UserInfoHelper userInfoHelper;

    @Inject
    private ApplicationContext applicationContext;

    public CurrentUserLoader(Context context) {
        super(context);
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        UsersService service = applicationContext.getUsersService();
        Response<UserInfo> messageResponse = service.getCurrentUser(apiKey).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
    @Override
    public void save(Object object) {
        UserInfo userInfo = (UserInfo) object;
        if (userInfo != null) {
            userInfoHelper.saveUserInfoContent(userInfo);
        }
    }
}
