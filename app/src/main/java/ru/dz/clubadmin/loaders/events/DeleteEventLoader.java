package ru.dz.clubadmin.loaders.events;


import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.EventsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class DeleteEventLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private Long eventId;

    public DeleteEventLoader(Context context, Long eventId) {
        super(context);
        this.eventId = eventId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        EventsService service = applicationContext.getEventService();
        Response<ResponseMessage> messageResponse = service.deleteEvent(this.eventId, apiKey).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
