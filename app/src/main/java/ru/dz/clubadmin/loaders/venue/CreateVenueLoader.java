package ru.dz.clubadmin.loaders.venue;


import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.VenueService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.models.Venue;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class CreateVenueLoader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;

    private final Venue venue;

    public CreateVenueLoader(Context context, Venue venue) {
        super(context);
        this.venue = venue;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        VenueService service = applicationContext.getVenueService();
        Response<Venue> messageResponse = service.createVenue(apiKey,venue).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
