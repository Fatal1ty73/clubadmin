package ru.dz.clubadmin.loaders.users;


import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.UsersService;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ApiKey;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class ApiKeyLoader extends BaseLoader {
    @Inject
    private ApiKeyHelper apiKeyHelper;

    @Inject
    private ApplicationContext applicationContext;
    private String phone;
    private String code;

    public ApiKeyLoader(Context context, String phone, String code) {
        super(context);
        this.phone = phone;
        this.code = code;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        UsersService service = this.applicationContext.getUsersService();
        Response<ApiKey> messageResponse = service.getApiKey(phone, code).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
    @Override
    public void save(Object object){
        ApiKey apiKey = (ApiKey) object;
        if (apiKey != null) {
            apiKeyHelper.saveApiKey(apiKey);
        }

    }
}
