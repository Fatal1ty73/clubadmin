package ru.dz.clubadmin.loaders.venue;

import android.content.Context;

import com.google.inject.Inject;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.VenueService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.loaders.BaseLoader;

/**
 * Created by Sam (samir@peller.tech) on 21.07.2016.
 */
public class GetVenueTagsLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private final Long idVenue;

    public GetVenueTagsLoader(Context context, Long idVenue) {
        super(context);
        this.idVenue = idVenue;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        VenueService service = applicationContext.getVenueService();
        Response<List<String>> messageResponse = service.getTags(idVenue, apiKey).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
