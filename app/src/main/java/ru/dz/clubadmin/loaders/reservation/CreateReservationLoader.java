package ru.dz.clubadmin.loaders.reservation;

import android.content.Context;

import com.google.inject.Inject;

import java.io.IOException;

import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ReservationsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ReservationInfo;
import ru.dz.clubadmin.loaders.BaseLoader;

/**
 * Created by Sam (samir@peller.tech) on 01.07.2016.
 */
public class CreateReservationLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private ReservationInfo reservationInfo;
    boolean approved;

    public CreateReservationLoader(Context context, ReservationInfo reservationInfo, boolean approved) {
        super(context);

        this.reservationInfo = reservationInfo;
        this.approved = approved;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ReservationsService service = applicationContext.getReservationService();
        Response<ReservationInfo> response = service.createReservation(getApiKey(), approved, reservationInfo).execute();
        return BaseResponse.fromResponse(response);
    }
}
