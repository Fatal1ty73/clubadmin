package ru.dz.clubadmin.loaders.clicker;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ClickerService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.CurrentClickerStats;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

/**
 * Created by nikitakulagin on 04.05.16.
 */
public class ClickerLoader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;

    private Long venueId;
    private Long eventId;

    public Long getVenueId() {
        return venueId;
    }

    public void setVenueId(Long venueId) {
        this.venueId = venueId;
    }



    public ClickerLoader(Context context, Long venueId, Long eventId) {
        super(context);
        this.venueId = venueId;
        this.eventId = eventId;
    }


    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ClickerService service = applicationContext.getClickerService();
        Response<CurrentClickerStats> response = service.getCurrentClickerStats(apiKey, venueId, eventId).execute();
        return BaseResponse.fromResponse(response);
    }
}
