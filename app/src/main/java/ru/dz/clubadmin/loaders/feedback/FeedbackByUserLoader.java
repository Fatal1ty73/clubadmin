package ru.dz.clubadmin.loaders.feedback;


import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.FeedbackService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.models.FeedbackList;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class FeedbackByUserLoader extends BaseLoader {
    private String reservationId;
    private String userId;

    public FeedbackByUserLoader(Context context, String reservationId, String userId) {
        super(context);
        this.reservationId = reservationId;
        this.userId = userId;
    }

    @Inject
    private ApplicationContext applicationContext;


    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        FeedbackService service = applicationContext.getFeedbackService();
        Response<FeedbackList> messageResponse = service.getFeedbackByUserId(apiKey, this.reservationId, this.userId).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
