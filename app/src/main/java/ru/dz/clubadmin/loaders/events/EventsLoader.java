package ru.dz.clubadmin.loaders.events;


import android.content.Context;
import com.google.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.EventsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.EventsList;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EventsLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private String date;
    private Long venueId;

    public EventsLoader(Context context, DateTime date, Long venueId) {
        super(context);
        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd");
        this.date = df.print(date);
        this.venueId = venueId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        EventsService service = applicationContext.getEventService();
        Response<EventsList> messageResponse = service.getEvents(apiKey, this.date, this.venueId).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
