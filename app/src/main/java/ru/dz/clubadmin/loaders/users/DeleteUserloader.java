package ru.dz.clubadmin.loaders.users;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.UsersService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class DeleteUserloader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;
    private final Long userId;

    public DeleteUserloader(Context context, Long userId) {
        super(context);
        this.userId = userId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        UsersService service = applicationContext.getUsersService();
        Response<ResponseMessage> messageResponse = service.deleteUser(userId.toString()).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}