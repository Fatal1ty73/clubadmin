package ru.dz.clubadmin.loaders.clicker;


import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ClickerService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.AlertClicker;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class SendAlertLoader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;

    private AlertClicker alertClicker;

    public SendAlertLoader(Context context, AlertClicker alertClicker) {
        super(context);
        this.alertClicker = alertClicker;
    }


    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ClickerService service = applicationContext.getClickerService();
        Response<ResponseMessage> response = service.sendAlert(apiKey, alertClicker).execute();
        return BaseResponse.fromResponse(response);
    }
}
