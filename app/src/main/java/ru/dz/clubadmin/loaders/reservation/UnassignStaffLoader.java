package ru.dz.clubadmin.loaders.reservation;

import android.content.Context;

import com.google.inject.Inject;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ReservationsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.StaffAssignment;
import ru.dz.clubadmin.loaders.BaseLoader;

/**
 * Created by Sam (samir@peller.tech) on 11.07.2016.
 */
public class UnassignStaffLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private long reservationId;
    private ArrayList<StaffAssignment> staffAssignment;

    public UnassignStaffLoader(Context context, long reservationId, ArrayList<StaffAssignment> staffAssignment) {
        super(context);

        this.reservationId = reservationId;
        this.staffAssignment = staffAssignment;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ReservationsService service = applicationContext.getReservationService();
        Response<ResponseMessage> messageResponse = service.unassignStaffBatch(reservationId, apiKey, staffAssignment).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
