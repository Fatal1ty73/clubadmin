package ru.dz.clubadmin.loaders.users;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.UsersService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class SecurityCodeLoader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;

    private String phoneNumber;

    public SecurityCodeLoader(Context context, String phone) {
        super(context);
        phoneNumber = phone;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        UsersService service = applicationContext.getUsersService();
        Response<ResponseMessage> messageResponse = service.getSecurityCode(phoneNumber).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
