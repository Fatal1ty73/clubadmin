package ru.dz.clubadmin.loaders.reservation;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ReservationsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ReservationInfo;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;
import java.util.List;

/**
 * Created by Sam (samir@peller.tech) on 19.07.2016.
 */
public class ReservationsBsLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private String venueId;
    private String date;
    private String bsType;
    private long eventId;

    public ReservationsBsLoader(Context context, String venueId, String date, String bsType, long eventId) {
        super(context);
        this.venueId = venueId;
        this.date = date;
        this.bsType = bsType;
        this.eventId = eventId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ReservationsService service = applicationContext.getReservationService();
        Response<List<ReservationInfo>> messageResponse = service.getReservationsBs(apiKey, venueId, date, bsType, eventId).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}