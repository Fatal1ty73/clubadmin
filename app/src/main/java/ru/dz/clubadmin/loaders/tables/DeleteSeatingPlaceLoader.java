package ru.dz.clubadmin.loaders.tables;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.TablesService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;


public class DeleteSeatingPlaceLoader extends BaseLoader {
    private long id;
    @Inject
    private ApplicationContext applicationContext;
    public DeleteSeatingPlaceLoader(Context context, Long id) {
        super(context);
        this.id = id;
    }
    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        TablesService service = applicationContext.getTablesService();
        Response<ResponseMessage> messageResponse = service.deleteSeatingPlace(this.id, apiKey).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
