package ru.dz.clubadmin.loaders.users;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.UsersService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.models.UserSettings;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;


public class UserSettingsLoader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;

    public UserSettingsLoader(Context context) {
        super(context);
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        UsersService service = applicationContext.getUsersService();
        Response<UserSettings> messageResponse = service.getUserSettings(apiKey).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
