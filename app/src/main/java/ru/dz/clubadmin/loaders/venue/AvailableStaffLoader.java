package ru.dz.clubadmin.loaders.venue;

import android.content.Context;

import com.google.inject.Inject;

import java.io.IOException;

import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.VenueService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.StaffList;
import ru.dz.clubadmin.loaders.BaseLoader;

/**
 * Created by Sam (samir@peller.tech) on 20.06.2016.
 */
public class AvailableStaffLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;
    private Long venueId;
    private final String apiKey;

    public AvailableStaffLoader(Context context, Long venueId, String apiKey) {
        super(context);
        this.venueId = venueId;
        this.apiKey = apiKey;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        VenueService service = applicationContext.getVenueService();
        Response<StaffList> messageResponse = service.getAvailableStaffForReservation(venueId, apiKey).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
