package ru.dz.clubadmin.loaders.promo;


import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.PromoService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.Promotion;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class CreateFeedbackLoader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;
    private Promotion promotion;

    public CreateFeedbackLoader(Context context, Promotion promotion) {
        super(context);
        this.promotion = promotion;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        PromoService service = applicationContext.getPromoService();
        Response<ResponseMessage> response = service.createFeedback(apiKey, promotion).execute();
        return BaseResponse.fromResponse(response);
    }
}
