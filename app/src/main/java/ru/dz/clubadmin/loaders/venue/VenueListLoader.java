package ru.dz.clubadmin.loaders.venue;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.VenueService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.models.Venue;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;
import java.util.List;


public class VenueListLoader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;


    public VenueListLoader(Context context) {
        super(context);
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        VenueService service = applicationContext.getVenueService();
        Response<List<Venue>> response = service.getAllVenues(apiKey).execute();
        return BaseResponse.fromResponse(response);
    }
}
