package ru.dz.clubadmin.loaders.venue;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.VenueService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.VenuePeopleList;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

/**
 * Created by nikitakulagin on 15.05.16.
 */
public class EmployeeLoader extends BaseLoader {

    private Long venueId;
    public Long getVenueId() {
        return venueId;
    }

    public void setVenueId(Long venueId) {
        this.venueId = venueId;
    }

    @Inject
    private ApplicationContext applicationContext;

    public EmployeeLoader(Context context, Long venueId) {
        super(context);
        this.venueId = venueId;
     }
    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        VenueService service = applicationContext.getVenueService();
        Response<VenuePeopleList> messageResponse = service.getVenueRequestsAndEmployees(venueId, apiKey).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
