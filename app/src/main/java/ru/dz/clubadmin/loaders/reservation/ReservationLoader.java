package ru.dz.clubadmin.loaders.reservation;


import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ReservationsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ReservationInfo;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

public class ReservationLoader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;

    private long reservationId;

    public ReservationLoader(Context context, long reservationId) {
        super(context);
        this.reservationId = reservationId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ReservationsService service = applicationContext.getReservationService();
        Response<ReservationInfo> messageResponse = service.getReservation(apiKey, reservationId).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
