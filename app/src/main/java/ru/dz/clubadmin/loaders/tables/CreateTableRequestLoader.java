package ru.dz.clubadmin.loaders.tables;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.TablesService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.domain.models.CreateTableRequest;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

/**
 * Created by nikitakulagin on 20.05.16.
 */
public class CreateTableRequestLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private CreateTableRequest table;
    public CreateTableRequestLoader(Context context, CreateTableRequest table) {
        super(context);
        this.table = table;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        TablesService service = applicationContext.getTablesService();
        Response<ResponseMessage> response = service.createTable(apiKey, table).execute();
        return BaseResponse.fromResponse(response);
    }
}
