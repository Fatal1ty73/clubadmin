package ru.dz.clubadmin.loaders.reservation;

import android.content.Context;

import com.google.inject.Inject;

import java.io.IOException;

import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ReservationsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ReservationInfo;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.loaders.BaseLoader;

public class UpdateReservationLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private ReservationInfo reservationInfo;

    public UpdateReservationLoader(Context context, ReservationInfo reservationInfo) {
        super(context);

        this.reservationInfo = reservationInfo;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ReservationsService service = applicationContext.getReservationService();
        Response<ResponseMessage> response = service.updateReservation(getApiKey(), reservationInfo).execute();
        return BaseResponse.fromResponse(response);
    }
}