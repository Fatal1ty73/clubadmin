package ru.dz.clubadmin.loaders.tables;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.TablesService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.TablesList;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;

/**
 * Created by nikitakulagin on 19.05.16.
 */
public class TablesLoader extends BaseLoader {
    private Long venueId;
    public TablesLoader(Context context, Long venueId) {
        super(context);
        this.venueId = venueId;
    }
    @Inject
    ApplicationContext applicationContext;
    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        TablesService service = applicationContext.getTablesService();
        Response<TablesList> response = service.getSeatingPlaceList(apiKey, venueId).execute();
        return BaseResponse.fromResponse(response);
    }
}
