package ru.dz.clubadmin.loaders;


import android.content.Context;
import android.util.Log;
import com.google.inject.Inject;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.response.RequestStatus;
import ru.dz.clubadmin.database.users.ApiKeyHelper;
import ru.dz.clubadmin.di.controller.RoboAsyncTaskLoader;
import ru.dz.clubadmin.domain.ApiKey;

import java.io.IOException;

public abstract class BaseLoader extends RoboAsyncTaskLoader<BaseResponse> {

    @Inject
    private ApiKeyHelper apiKeyHelper;

    protected String getApiKey() {
        ApiKey apiKey = apiKeyHelper.getApiKey();
        if(apiKey.getApiKey() != null)
            return apiKey.getApiKey();
        else
            return "";
    }

    public BaseLoader(Context context) {
        super(context);
    }


    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public BaseResponse loadInBackground() {
        try {
            BaseResponse baseResponse = apiCall(getApiKey());
            if (baseResponse.getRequestResult() == RequestStatus.SUCCESS) {
                save(baseResponse.getObj());
                onSuccess();
            } else {
                onError();
            }
            return baseResponse;
        } catch (Throwable t) {
            return BaseResponse.fromThrowable(t);
        }
    }

    protected void onSuccess() {
        Log.i("BaseLoader", "responseCursor non empty");
    }

    protected void onError() {
        Log.e("BaseLoader", "responseCursor is empty");
    }

    protected abstract BaseResponse apiCall(String apiKey) throws IOException;

    public void save(Object object) {
    }
}