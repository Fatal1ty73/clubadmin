package ru.dz.clubadmin.loaders.events;


import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.EventsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.EventCalendar;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EventsCalendarLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private String date;
    private String venueId;

    public EventsCalendarLoader(Context context, Date date, String venueId) {
        super(context);
        this.venueId = venueId;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        this.date = df.format(date);
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        EventsService service = applicationContext.getEventService();
        Response<List<EventCalendar>> messageResponse = service.getEventsCalendar(apiKey, this.date, this.venueId).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
