package ru.dz.clubadmin.loaders.venue;

import android.content.Context;

import com.google.inject.Inject;

import java.io.IOException;

import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.VenueService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ResponseMessage;
import ru.dz.clubadmin.loaders.BaseLoader;

/**
 * Created by Sam (samir@peller.tech) on 22.07.2016.
 */
public class DeleteVenueTagLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private final Long idVenue;
    private final String tagName;

    public DeleteVenueTagLoader(Context context, Long idVenue, String tagName) {
        super(context);
        this.idVenue = idVenue;
        this.tagName = tagName;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        VenueService service = applicationContext.getVenueService();
        Response<ResponseMessage> messageResponse = service.deleteTag(idVenue, apiKey, tagName).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
