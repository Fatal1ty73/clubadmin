package ru.dz.clubadmin.loaders.venue;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.VenueService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.NextDate;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;


public class NextEventLoader extends BaseLoader {

    @Inject
    private ApplicationContext applicationContext;

    private final Long idVenue;
    private final String from;

    public NextEventLoader(Context context, Long idVenue, String from) {
        super(context);
        this.idVenue = idVenue;
        this.from = from;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        VenueService service = applicationContext.getVenueService();
        Response<NextDate> messageResponse = service.getNextEventDate(idVenue, apiKey, from).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}
