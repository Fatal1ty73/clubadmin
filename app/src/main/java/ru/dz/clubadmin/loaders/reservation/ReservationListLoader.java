package ru.dz.clubadmin.loaders.reservation;

import android.content.Context;
import com.google.inject.Inject;
import retrofit2.Response;
import ru.dz.clubadmin.api.response.BaseResponse;
import ru.dz.clubadmin.api.services.ReservationsService;
import ru.dz.clubadmin.di.controller.ApplicationContext;
import ru.dz.clubadmin.domain.ReservationsLists;
import ru.dz.clubadmin.loaders.BaseLoader;

import java.io.IOException;


public class ReservationListLoader extends BaseLoader {
    @Inject
    private ApplicationContext applicationContext;

    private String venueId;
    private String date;
    private long eventId;

    public ReservationListLoader(Context context, String venueId, String date, long eventId) {
        super(context);
        this.venueId = venueId;
        this.date = date;
        this.eventId = eventId;
    }

    @Override
    protected BaseResponse apiCall(String apiKey) throws IOException {
        ReservationsService service = applicationContext.getReservationService();
        Response<ReservationsLists> messageResponse = service.getReservations(apiKey, venueId, date, eventId).execute();
        return BaseResponse.fromResponse(messageResponse);
    }
}