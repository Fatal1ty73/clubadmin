package ru.dz.clubadmin.domain.models;

import io.realm.RealmObject;

/**
 * Created by arkady on 07/03/16.
 */

public class Place extends RealmObject {


    private Integer id;


    private Venue venue;


    private String bottleServiceType;


    private Integer placeNumber;

    public Place() {
    }

    public Place(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public void setBottleService(BottleServiceTypeEnum bottleServiceType) {
        this.bottleServiceType = bottleServiceType.name();
    }

    public BottleServiceTypeEnum getBottleService() {
        return BottleServiceTypeEnum.valueOf(this.bottleServiceType);
    }

    public Integer getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(Integer placeNumber) {
        this.placeNumber = placeNumber;
    }
}
