package ru.dz.clubadmin.domain;

/**
 * Created by nikitakulagin on 04.05.16.
 */
public class CurrentClickerStats {
    private int totalIn;
    private int totalOut;
    private int men;
    private int women;
    private int capacity;

    public CurrentClickerStats() {
    }

    public CurrentClickerStats(Integer totalIn, Integer totalOut, Integer men, Integer women, Integer capacity) {
        this.totalIn = totalIn;
        this.totalOut = totalOut;
        this.men = men;
        this.women = women;
        this.capacity = capacity;
    }

    public Integer getTotalIn() {
        return totalIn;
    }

    public void setTotalIn(Integer totalIn) {
        this.totalIn = totalIn;
    }

    public Integer getTotalOut() {
        return totalOut;
    }

    public void setTotalOut(Integer totalOut) {
        this.totalOut = totalOut;
    }

    public Integer getMen() {
        return men;
    }

    public void setMen(Integer men) {
        this.men = men;
    }

    public Integer getWomen() {
        return women;
    }

    public void setWomen(Integer women) {
        this.women = women;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }
}
