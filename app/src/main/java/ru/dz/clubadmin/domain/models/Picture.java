package ru.dz.clubadmin.domain.models;

import io.realm.RealmObject;

/**
 * Picture
 */
public class Picture extends RealmObject {

    private Long id = null;
    private String link = null;

    public Picture() {
    }

    public Picture(Long id, String link) {
        this.id = id;
        this.link = link;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}

