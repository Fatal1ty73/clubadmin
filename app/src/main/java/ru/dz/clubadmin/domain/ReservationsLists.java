package ru.dz.clubadmin.domain;

import io.realm.RealmList;
import io.realm.RealmObject;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arkady on 08/03/16.
 */
public class ReservationsLists extends RealmObject {

	private RealmList<ReservationInfo> guestlist = new RealmList<>();
	private RealmList<ReservationInfo> pending = new RealmList<>();
	private RealmList<ReservationInfo> approved = new RealmList<>();
	private RealmList<ReservationInfo> arrived = new RealmList<>();

	public List<ReservationInfo> getGuestlist() {
		if(guestlist != null && !guestlist.isEmpty())
			return guestlist;
		else
			return new ArrayList<>();
	}

	public void setGuestlist(List<ReservationInfo> guestlist) {
		RealmList<ReservationInfo> reservationInfoRealmList = new RealmList<>();
		for(ReservationInfo reservationInfo: guestlist) reservationInfoRealmList.add(reservationInfo);
		this.guestlist = reservationInfoRealmList;
	}

	public List<ReservationInfo> getPending() {
		if(pending == null)
			return new ArrayList<>();
		else
			return pending;
	}

	public void setPending(List<ReservationInfo> pending) {
		RealmList<ReservationInfo> reservationInfoRealmList = new RealmList<>();
		for(ReservationInfo reservationInfo: pending) reservationInfoRealmList.add(reservationInfo);
		this.pending = reservationInfoRealmList;
	}

	public List<ReservationInfo> getApproved() {
		if(approved == null)
			return new ArrayList<>();
		else
			return approved;
	}

	public void setApproved(List<ReservationInfo> approved) {
		RealmList<ReservationInfo> reservationInfoRealmList = new RealmList<>();
		for(ReservationInfo reservationInfo: approved) reservationInfoRealmList.add(reservationInfo);
		this.approved = reservationInfoRealmList;
	}

	public List<ReservationInfo> getArrived() {
		if(arrived == null)
			return new ArrayList<>();
		else
			return arrived;
	}

	public void setArrived(List<ReservationInfo> arrived) {
		RealmList<ReservationInfo> reservationInfoRealmList = new RealmList<>();
		for(ReservationInfo reservationInfo: arrived) reservationInfoRealmList.add(reservationInfo);
		this.arrived = reservationInfoRealmList;
	}
}
