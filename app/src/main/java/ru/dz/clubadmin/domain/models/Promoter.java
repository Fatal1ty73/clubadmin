package ru.dz.clubadmin.domain.models;

import io.realm.RealmObject;

import java.io.Serializable;

//public class Promoter implements Serializable {
public class Promoter extends RealmObject implements Serializable {

    private Long id;
    private String fullName;
    private String userpic;
    private Integer numberOfFriends;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserpic() {
        return userpic;
    }

    public void setUserpic(String userpic) {
        this.userpic = userpic;
    }

    public Integer getNumberOfFriends() {
        return numberOfFriends;
    }

    public void setNumberOfFriends(Integer numberOfFriends) {
        this.numberOfFriends = numberOfFriends;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

}
