package ru.dz.clubadmin.domain;


import io.realm.RealmList;
import io.realm.RealmObject;
import ru.dz.clubadmin.domain.models.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class ReservationInfo extends RealmObject implements Serializable {

    private Long id = null;
    private VisitorInfo guestInfo = null;
    private Integer guestsNumber = null;
    private Integer totalGuests = null;
    private Integer arrivedGuests = null;
    private Integer arrivedGirls = null;
    private Integer arrivedGuys = null;
    private Boolean notifyMgmtOnArrival = false;
    private String bookingNote = null;
    private Boolean complimentGirls = false;
    private Boolean complimentGuys = false;
    private Integer complimentGuysQty = null;
    private Integer complimentGirlsQty = null;
    private Boolean reducedGirls = false;
    private Boolean reducedGuys = false;
    private Integer reducedGuysQty = null;
    private Integer reducedGirlsQty = null;
    private Boolean mustEnter = false;

    private String groupType = null;
    private Integer bottleMin = null;
    private Integer minSpend = null;

    private String status = null;
    private Long eventId = null;
    private RealmList<RealmString> tags = new RealmList<>();
    private RealmList<RealmString> photos = new RealmList<>();
    private RealmList<StaffAssignment> staff = new RealmList<>();
    private TableInfo tableInfo = null;
    private UserInfo bookedBy = null;

    private String bottleService = null;
    private Integer totalSpent = null;
    private Float rating = null;
    private Long venueId = null;
    private String reservationDate = null;
    private Integer feedbackCount = null;
    private String arrivalTime = null;
    private String estimatedArrivalTime = null;
    private String statusChangeTime = null;
    private Boolean deleted = false;
    private RealmList<PayInfoTO>  payees = new RealmList<>();
    private FeedbackInfo completionFeedback = null;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VisitorInfo getGuestInfo() {
        return guestInfo;
    }

    public void setGuestInfo(VisitorInfo guestInfo) {
        this.guestInfo = guestInfo;
    }

    public Integer getGuestsNumber() {
        return guestsNumber;
    }

    public void setGuestsNumber(Integer guestsNumber) {
        this.guestsNumber = guestsNumber;
    }

    public Integer getTotalGuests() {
        return totalGuests;
    }

    public void setTotalGuests(Integer totalGuests) {
        this.totalGuests = totalGuests;
    }

    public Integer getArrivedGuests() {
        return arrivedGuests;
    }

    public void setArrivedGuests(Integer arrivedGuests) {
        this.arrivedGuests = arrivedGuests;
    }

    public Integer getArrivedGirls() {
        return arrivedGirls;
    }

    public void setArrivedGirls(Integer arrivedGirls) {
        this.arrivedGirls = arrivedGirls;
    }

    public Integer getArrivedGuys() {
        return arrivedGuys;
    }

    public void setArrivedGuys(Integer arrivedGuys) {
        this.arrivedGuys = arrivedGuys;
    }

    public Boolean getNotifyMgmtOnArrival() {
        return notifyMgmtOnArrival;
    }

    public void setNotifyMgmtOnArrival(Boolean notifyMgmtOnArrival) {
        this.notifyMgmtOnArrival = notifyMgmtOnArrival;
    }

    public String getBookingNote() {
        return bookingNote;
    }

    public void setBookingNote(String bookingNote) {
        this.bookingNote = bookingNote;
    }

    public Boolean getComplimentGirls() {
        return complimentGirls;
    }

    public void setComplimentGirls(Boolean complimentGirls) {
        this.complimentGirls = complimentGirls;
    }

    public Boolean getComplimentGuys() {
        return complimentGuys;
    }

    public void setComplimentGuys(Boolean complimentGuys) {
        this.complimentGuys = complimentGuys;
    }

    public Integer getComplimentGuysQty() {
        return complimentGuysQty;
    }

    public void setComplimentGuysQty(Integer complimentGuysQty) {
        this.complimentGuysQty = complimentGuysQty;
    }

    public Integer getComplimentGirlsQty() {
        return complimentGirlsQty;
    }

    public void setComplimentGirlsQty(Integer complimentGirlsQty) {
        this.complimentGirlsQty = complimentGirlsQty;
    }

    public Boolean getReducedGirls() {
        return reducedGirls;
    }

    public void setReducedGirls(Boolean reducedGirls) {
        this.reducedGirls = reducedGirls;
    }

    public Boolean getReducedGuys() {
        return reducedGuys;
    }

    public void setReducedGuys(Boolean reducedGuys) {
        this.reducedGuys = reducedGuys;
    }

    public Integer getReducedGuysQty() {
        return reducedGuysQty;
    }

    public void setReducedGuysQty(Integer reducedGuysQty) {
        this.reducedGuysQty = reducedGuysQty;
    }

    public Integer getReducedGirlsQty() {
        return reducedGirlsQty;
    }

    public void setReducedGirlsQty(Integer reducedGirlsQty) {
        this.reducedGirlsQty = reducedGirlsQty;
    }

    public Boolean getMustEnter() {
        return mustEnter;
    }

    public void setMustEnter(Boolean mustEnter) {
        this.mustEnter = mustEnter;
    }

    public GroupType getGroupType() {
        if (this.groupType != null) {
            return GroupType.valueOf(this.groupType);
        } else return null;

    }

    public void setGroupType(GroupType groupType) {
        this.groupType = groupType.name();
    }

    public Integer getBottleMin() {
        return bottleMin;
    }

    public void setBottleMin(Integer bottleMin) {
        this.bottleMin = bottleMin;
    }

    public Integer getMinSpend() {
        return minSpend;
    }

    public void setMinSpend(Integer minSpend) {
        this.minSpend = minSpend;
    }

    public ReservationStatus getStatus() {
        if (this.status != null) {
            return ReservationStatus.valueOf(this.status);
        } else return null;
    }

    public void setStatus(ReservationStatus status) {
        this.status = status.name();
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public List<String> getTags() {
        ArrayList<String> tagsList = new ArrayList<>();
        if (!this.tags.isEmpty()) {
            for (RealmString tag : this.tags) tagsList.add(tag.getString());
        }
        return tagsList;
    }

    public void setTags(List<String> tags) {
        RealmList<RealmString> tagsList = new RealmList<>();
        for (String tag : tags) tagsList.add(new RealmString(tag));
        this.tags = tagsList;
    }

    public List<String> getPhotos() {
        ArrayList<String> photosList = new ArrayList<>();
        if (!this.photos.isEmpty()) {
            for (RealmString photo : this.photos) photosList.add(photo.getString());
        }
        return photosList;
    }

    public void setPhotos(List<String> photos) {
        RealmList<RealmString> photoList = new RealmList<>();
        for (String photo : photos) photoList.add(new RealmString(photo));
        this.photos = photoList;
    }

    public List<StaffAssignment> getStaff() {
        return staff;
    }

    public void setStaff(List<StaffAssignment> staff) {
        RealmList<StaffAssignment> staffAssignments = new RealmList<>();
        for (StaffAssignment staffAssignment : staff) staffAssignments.add(staffAssignment);
        this.staff = staffAssignments;
    }

    public BottleServiceTypeEnum getBottleService() {
        if (this.bottleService != null) {
            return BottleServiceTypeEnum.valueOf(this.bottleService);
        } else return null;

    }
    public void setBottleService(BottleServiceTypeEnum groupType) {
        this.bottleService = groupType.name();
    }


    public TableInfo getTableInfo() {
        return tableInfo;
    }

    public void setTableInfo(TableInfo tableInfo) {
        this.tableInfo = tableInfo;
    }

    public UserInfo getBookedBy() {
        return bookedBy;
    }

    public void setBookedBy(UserInfo bookedBy) {
        this.bookedBy = bookedBy;
    }

    public void setBottleService(String bottleService) {
        this.bottleService = bottleService;
    }

    public Integer getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(Integer totalSpent) {
        this.totalSpent = totalSpent;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Long getVenueId() {
        return venueId;
    }

    public void setVenueId(Long venueId) {
        this.venueId = venueId;
    }

    public String getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(String reservationDate) {
        this.reservationDate = reservationDate;
    }

    public Integer getFeedbackCount() {
        return feedbackCount;
    }

    public void setFeedbackCount(Integer feedbackCount) {
        this.feedbackCount = feedbackCount;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getEstimatedArrivalTime() {
        return estimatedArrivalTime;
    }

    public void setEstimatedArrivalTime(String estimatedArrivalTime) {
        this.estimatedArrivalTime = estimatedArrivalTime;
    }

    public String getStatusChangeTime() {
        return statusChangeTime;
    }

    public void setStatusChangeTime(String statusChangeTime) {
        this.statusChangeTime = statusChangeTime;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<PayInfoTO> getPayees() {
        return payees;
    }

    public void setPayees(List<PayInfoTO> payees) {
        RealmList<PayInfoTO> payeesList = new RealmList<>();
        for (PayInfoTO payInfoTO : payees) payeesList.add(payInfoTO);
        this.payees = payeesList;
    }

    public FeedbackInfo getCompletionFeedback() {
        return completionFeedback;
    }

    public void setCompletionFeedback(FeedbackInfo completionFeedback) {
        this.completionFeedback = completionFeedback;
    }
}
