package ru.dz.clubadmin.domain.models;


public class TagTO {
    private String tag;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
