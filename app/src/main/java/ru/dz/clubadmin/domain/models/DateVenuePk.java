package ru.dz.clubadmin.domain.models;


import io.realm.RealmObject;

/**
 * Created by arkady on 11/03/16.
 */

public class DateVenuePk extends RealmObject {

	
	private Integer venueId;
	
	private LocalEventTime date;

	public DateVenuePk() {
	}

	public DateVenuePk(Integer venueId, LocalEventTime date) {
		this.venueId = venueId;
		this.date = date;
	}

	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	public LocalEventTime getDate() {
		return date;
	}

	public void setDate(LocalEventTime date) {
		this.date = date;
	}

	
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DateVenuePk that = (DateVenuePk) o;

		if (!venueId.equals(that.venueId)) return false;
		return date.equals(that.date);
	}

	
	public int hashCode() {
		int result = venueId.hashCode();
		result = 31 * result + date.hashCode();
		return result;
	}

	
	public String toString() {
		return "DateVenuePk{" +
				"venueId=" + venueId +
				", date=" + date +
				'}';
	}
}
