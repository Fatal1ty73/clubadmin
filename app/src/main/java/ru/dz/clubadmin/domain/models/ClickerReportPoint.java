package ru.dz.clubadmin.domain.models;



/**
 * ClickerReportPoint
 */
public class ClickerReportPoint   {
  
  private Long men = null;
  private Long woman = null;
  private String date = null;
  private String time = null;



  /**
   **/
  public ClickerReportPoint men(Long men) {
    this.men = men;
    return this;
  }
  public Long getMen() {
    return men;
  }
  public void setMen(Long men) {
    this.men = men;
  }


  /**
   **/
  public ClickerReportPoint woman(Long woman) {
    this.woman = woman;
    return this;
  }

  public Long getWoman() {
    return woman;
  }
  public void setWoman(Long woman) {
    this.woman = woman;
  }


  /**
   **/
  public ClickerReportPoint date(String date) {
    this.date = date;
    return this;
  }

  public String getDate() {
    return date;
  }
  public void setDate(String date) {
    this.date = date;
  }


  /**
   **/
  public ClickerReportPoint time(String time) {
    this.time = time;
    return this;
  }

  public String getTime() {
    return time;
  }
  public void setTime(String time) {
    this.time = time;
  }



  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClickerReportPoint {\n");
    
    sb.append("    men: ").append(toIndentedString(men)).append("\n");
    sb.append("    woman: ").append(toIndentedString(woman)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    time: ").append(toIndentedString(time)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ClickerReportPoint that = (ClickerReportPoint) o;

    if (men != null ? !men.equals(that.men) : that.men != null) return false;
    if (woman != null ? !woman.equals(that.woman) : that.woman != null) return false;
    if (date != null ? !date.equals(that.date) : that.date != null) return false;
    return time != null ? time.equals(that.time) : that.time == null;

  }

  @Override
  public int hashCode() {
    int result = men != null ? men.hashCode() : 0;
    result = 31 * result + (woman != null ? woman.hashCode() : 0);
    result = 31 * result + (date != null ? date.hashCode() : 0);
    result = 31 * result + (time != null ? time.hashCode() : 0);
    return result;
  }
  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

