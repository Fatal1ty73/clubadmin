package ru.dz.clubadmin.domain;


import io.realm.RealmObject;

public class NextDate extends RealmObject {

	private String date;

	public NextDate() {
	}

	public NextDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
