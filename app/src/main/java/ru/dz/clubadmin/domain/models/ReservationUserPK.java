package ru.dz.clubadmin.domain.models;

import io.realm.RealmObject;

/**
 * ReservationUserPK
 */
public class ReservationUserPK extends RealmObject {

    private Long reservationId = null;
    private Long userId = null;
    private String role = null;

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public VenueRole getRole() {
        return VenueRole.valueOf(this.role);
    }

    public void setRole(VenueRole role) {
        this.role = role.name();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ReservationUserPK {\n");

        sb.append("    reservationId: ").append(toIndentedString(reservationId)).append("\n");
        sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
        sb.append("    role: ").append(toIndentedString(role)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

