package ru.dz.clubadmin.domain.models;


import ru.dz.clubadmin.domain.SearchUserInfo;

public class ReservationStaffInfo {
    private SearchUserInfo server;
    private SearchUserInfo busser;
    private SearchUserInfo host;

    public ReservationStaffInfo() {
    }

    public ReservationStaffInfo(SearchUserInfo server, SearchUserInfo busser, SearchUserInfo host) {
        this.server = server;
        this.busser = busser;
        this.host = host;
    }

    public SearchUserInfo getServer() {
        return server;
    }

    public void setServer(SearchUserInfo server) {
        this.server = server;
    }

    public SearchUserInfo getBusser() {
        return busser;
    }

    public void setBusser(SearchUserInfo busser) {
        this.busser = busser;
    }

    public SearchUserInfo getHost() {
        return host;
    }

    public void setHost(SearchUserInfo host) {
        this.host = host;
    }
}
