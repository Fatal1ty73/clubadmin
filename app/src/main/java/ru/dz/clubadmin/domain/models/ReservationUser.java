package ru.dz.clubadmin.domain.models;

import io.realm.RealmObject;

/**
 * ReservationUser
 */
public class ReservationUser extends RealmObject {

    private ReservationUserPK pk = null;
    private Reservation reservation = null;
    private User user = null;


    public ReservationUserPK getPk() {
        return pk;
    }

    public void setPk(ReservationUserPK pk) {
        this.pk = pk;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ReservationUser {\n");

        sb.append("    pk: ").append(toIndentedString(pk)).append("\n");
        sb.append("    reservation: ").append(toIndentedString(reservation)).append("\n");
        sb.append("    user: ").append(toIndentedString(user)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

