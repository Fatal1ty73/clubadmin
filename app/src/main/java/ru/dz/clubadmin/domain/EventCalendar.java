package ru.dz.clubadmin.domain;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EventCalendar {
    private String date;
    private Integer numberOfEvents;

    public EventCalendar(String date, Integer numberOfEvents) {
        this.date = date;
        this.numberOfEvents = numberOfEvents;
    }

    public String getDate() {
        return date;
    }
    public Date getDateTime() {
        if(!this.date.isEmpty()){
            try{
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                return df.parse(this.date);
            } catch (Exception ex){
                System.out.println(ex.getMessage());
                return new Date();
            }
        }else
            return new Date();
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getNumberOfEvents() {
        return numberOfEvents;
    }

    public void setNumberOfEvents(Integer numberOfEvents) {
        this.numberOfEvents = numberOfEvents;
    }
}
