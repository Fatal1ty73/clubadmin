package ru.dz.clubadmin.domain.models;

import ru.dz.clubadmin.domain.TableInfo;
import ru.dz.clubadmin.domain.UserInfo;
import ru.dz.clubadmin.domain.VisitorInfo;

import java.util.ArrayList;
import java.util.List;


/**
 * EODReservationInfo
 */
public class EODReservationInfo {

    private Long id = null;
    private VisitorInfo guestInfo = null;
    private Integer guestsNumber = null;
    private Integer totalGuests = null;
    private Integer arrivedGuests = null;
    private Integer arrivedGirls = null;
    private Integer arrivedGuys = null;
    private Boolean notifyMgmtOnArrival = false;
    private String bookingNote = null;
    private Boolean complimentGirls = false;
    private Boolean complimentGuys = false;
    private Integer complimentGuysQty = null;
    private Integer complimentGirlsQty = null;
    private Boolean reducedGirls = false;
    private Boolean reducedGuys = false;
    private Integer reducedGuysQty = null;
    private Integer reducedGirlsQty = null;
    private Boolean mustEnter = false;
    private GroupType groupType = null;
    private Integer bottleMin = null;
    private Integer minSpend = null;
    private ReservationStatus status = null;
    private Long eventId = null;
    private List<String> tags = new ArrayList<String>();
    private List<String> photos = new ArrayList<String>();
    private List<StaffAssignment> staff = new ArrayList<StaffAssignment>();
    private TableInfo tableInfo = null;
    private UserInfo bookedBy = null;
    private BottleServiceTypeEnum bottleService = null;
    private Integer totalSpent = null;
    private Float rating = null;
    private Long venueId = null;
    private String reservationDate = null;
    private Integer feedbackCount = null;
    private String arrivalTime = null;
    private String estimatedArrivalTime = null;
    private String statusChangeTime = null;
    private Boolean deleted = false;
    private List<PayInfoTO> payees = new ArrayList<PayInfoTO>();
    private FeedbackInfo completionFeedback = null;
    private UserInfo compleredBy = null;
    private UserInfo confirmedBy = null;

    public EODReservationInfo() {
    }

    public EODReservationInfo(Long id, VisitorInfo guestInfo, Integer guestsNumber, Integer totalGuests, Integer arrivedGuests, Integer arrivedGirls, Integer arrivedGuys, Boolean notifyMgmtOnArrival, String bookingNote, Boolean complimentGirls, Boolean complimentGuys, Integer complimentGuysQty, Integer complimentGirlsQty, Boolean reducedGirls, Boolean reducedGuys, Integer reducedGuysQty, Integer reducedGirlsQty, Boolean mustEnter, GroupType groupType, Integer bottleMin, Integer minSpend, ReservationStatus status, Long eventId, List<String> tags, List<String> photos, List<StaffAssignment> staff, TableInfo tableInfo, UserInfo bookedBy, BottleServiceTypeEnum bottleService, Integer totalSpent, Float rating, Long venueId, String reservationDate, Integer feedbackCount, String arrivalTime, String estimatedArrivalTime, String statusChangeTime, Boolean deleted, List<PayInfoTO> payees, FeedbackInfo completionFeedback, UserInfo compleredBy, UserInfo confirmedBy) {
        this.id = id;
        this.guestInfo = guestInfo;
        this.guestsNumber = guestsNumber;
        this.totalGuests = totalGuests;
        this.arrivedGuests = arrivedGuests;
        this.arrivedGirls = arrivedGirls;
        this.arrivedGuys = arrivedGuys;
        this.notifyMgmtOnArrival = notifyMgmtOnArrival;
        this.bookingNote = bookingNote;
        this.complimentGirls = complimentGirls;
        this.complimentGuys = complimentGuys;
        this.complimentGuysQty = complimentGuysQty;
        this.complimentGirlsQty = complimentGirlsQty;
        this.reducedGirls = reducedGirls;
        this.reducedGuys = reducedGuys;
        this.reducedGuysQty = reducedGuysQty;
        this.reducedGirlsQty = reducedGirlsQty;
        this.mustEnter = mustEnter;
        this.groupType = groupType;
        this.bottleMin = bottleMin;
        this.minSpend = minSpend;
        this.status = status;
        this.eventId = eventId;
        this.tags = tags;
        this.photos = photos;
        this.staff = staff;
        this.tableInfo = tableInfo;
        this.bookedBy = bookedBy;
        this.bottleService = bottleService;
        this.totalSpent = totalSpent;
        this.rating = rating;
        this.venueId = venueId;
        this.reservationDate = reservationDate;
        this.feedbackCount = feedbackCount;
        this.arrivalTime = arrivalTime;
        this.estimatedArrivalTime = estimatedArrivalTime;
        this.statusChangeTime = statusChangeTime;
        this.deleted = deleted;
        this.payees = payees;
        this.completionFeedback = completionFeedback;
        this.compleredBy = compleredBy;
        this.confirmedBy = confirmedBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VisitorInfo getGuestInfo() {
        return guestInfo;
    }

    public void setGuestInfo(VisitorInfo guestInfo) {
        this.guestInfo = guestInfo;
    }

    public Integer getGuestsNumber() {
        return guestsNumber;
    }

    public void setGuestsNumber(Integer guestsNumber) {
        this.guestsNumber = guestsNumber;
    }

    public Integer getTotalGuests() {
        return totalGuests;
    }

    public void setTotalGuests(Integer totalGuests) {
        this.totalGuests = totalGuests;
    }

    public Integer getArrivedGuests() {
        return arrivedGuests;
    }

    public void setArrivedGuests(Integer arrivedGuests) {
        this.arrivedGuests = arrivedGuests;
    }

    public Integer getArrivedGirls() {
        return arrivedGirls;
    }

    public void setArrivedGirls(Integer arrivedGirls) {
        this.arrivedGirls = arrivedGirls;
    }

    public Integer getArrivedGuys() {
        return arrivedGuys;
    }

    public void setArrivedGuys(Integer arrivedGuys) {
        this.arrivedGuys = arrivedGuys;
    }

    public Boolean getNotifyMgmtOnArrival() {
        return notifyMgmtOnArrival;
    }

    public void setNotifyMgmtOnArrival(Boolean notifyMgmtOnArrival) {
        this.notifyMgmtOnArrival = notifyMgmtOnArrival;
    }

    public String getBookingNote() {
        return bookingNote;
    }

    public void setBookingNote(String bookingNote) {
        this.bookingNote = bookingNote;
    }

    public Boolean getComplimentGirls() {
        return complimentGirls;
    }

    public void setComplimentGirls(Boolean complimentGirls) {
        this.complimentGirls = complimentGirls;
    }

    public Boolean getComplimentGuys() {
        return complimentGuys;
    }

    public void setComplimentGuys(Boolean complimentGuys) {
        this.complimentGuys = complimentGuys;
    }

    public Integer getComplimentGuysQty() {
        return complimentGuysQty;
    }

    public void setComplimentGuysQty(Integer complimentGuysQty) {
        this.complimentGuysQty = complimentGuysQty;
    }

    public Integer getComplimentGirlsQty() {
        return complimentGirlsQty;
    }

    public void setComplimentGirlsQty(Integer complimentGirlsQty) {
        this.complimentGirlsQty = complimentGirlsQty;
    }

    public Boolean getReducedGirls() {
        return reducedGirls;
    }

    public void setReducedGirls(Boolean reducedGirls) {
        this.reducedGirls = reducedGirls;
    }

    public Boolean getReducedGuys() {
        return reducedGuys;
    }

    public void setReducedGuys(Boolean reducedGuys) {
        this.reducedGuys = reducedGuys;
    }

    public Integer getReducedGuysQty() {
        return reducedGuysQty;
    }

    public void setReducedGuysQty(Integer reducedGuysQty) {
        this.reducedGuysQty = reducedGuysQty;
    }

    public Integer getReducedGirlsQty() {
        return reducedGirlsQty;
    }

    public void setReducedGirlsQty(Integer reducedGirlsQty) {
        this.reducedGirlsQty = reducedGirlsQty;
    }

    public Boolean getMustEnter() {
        return mustEnter;
    }

    public void setMustEnter(Boolean mustEnter) {
        this.mustEnter = mustEnter;
    }

    public GroupType getGroupType() {
        return groupType;
    }

    public void setGroupType(GroupType groupType) {
        this.groupType = groupType;
    }

    public Integer getBottleMin() {
        return bottleMin;
    }

    public void setBottleMin(Integer bottleMin) {
        this.bottleMin = bottleMin;
    }

    public Integer getMinSpend() {
        return minSpend;
    }

    public void setMinSpend(Integer minSpend) {
        this.minSpend = minSpend;
    }

    public ReservationStatus getStatus() {
        return status;
    }

    public void setStatus(ReservationStatus status) {
        this.status = status;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public List<StaffAssignment> getStaff() {
        return staff;
    }

    public void setStaff(List<StaffAssignment> staff) {
        this.staff = staff;
    }

    public TableInfo getTableInfo() {
        return tableInfo;
    }

    public void setTableInfo(TableInfo tableInfo) {
        this.tableInfo = tableInfo;
    }

    public UserInfo getBookedBy() {
        return bookedBy;
    }

    public void setBookedBy(UserInfo bookedBy) {
        this.bookedBy = bookedBy;
    }

    public BottleServiceTypeEnum getBottleService() {
        return bottleService;
    }

    public void setBottleService(BottleServiceTypeEnum bottleService) {
        this.bottleService = bottleService;
    }

    public Integer getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(Integer totalSpent) {
        this.totalSpent = totalSpent;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Long getVenueId() {
        return venueId;
    }

    public void setVenueId(Long venueId) {
        this.venueId = venueId;
    }

    public String getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(String reservationDate) {
        this.reservationDate = reservationDate;
    }

    public Integer getFeedbackCount() {
        return feedbackCount;
    }

    public void setFeedbackCount(Integer feedbackCount) {
        this.feedbackCount = feedbackCount;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getEstimatedArrivalTime() {
        return estimatedArrivalTime;
    }

    public void setEstimatedArrivalTime(String estimatedArrivalTime) {
        this.estimatedArrivalTime = estimatedArrivalTime;
    }

    public String getStatusChangeTime() {
        return statusChangeTime;
    }

    public void setStatusChangeTime(String statusChangeTime) {
        this.statusChangeTime = statusChangeTime;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<PayInfoTO> getPayees() {
        return payees;
    }

    public void setPayees(List<PayInfoTO> payees) {
        this.payees = payees;
    }

    public FeedbackInfo getCompletionFeedback() {
        return completionFeedback;
    }

    public void setCompletionFeedback(FeedbackInfo completionFeedback) {
        this.completionFeedback = completionFeedback;
    }

    public UserInfo getCompleredBy() {
        return compleredBy;
    }

    public void setCompleredBy(UserInfo compleredBy) {
        this.compleredBy = compleredBy;
    }

    public UserInfo getConfirmedBy() {
        return confirmedBy;
    }

    public void setConfirmedBy(UserInfo confirmedBy) {
        this.confirmedBy = confirmedBy;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class EODReservationInfo {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    guestInfo: ").append(toIndentedString(guestInfo)).append("\n");
        sb.append("    guestsNumber: ").append(toIndentedString(guestsNumber)).append("\n");
        sb.append("    totalGuests: ").append(toIndentedString(totalGuests)).append("\n");
        sb.append("    arrivedGuests: ").append(toIndentedString(arrivedGuests)).append("\n");
        sb.append("    arrivedGirls: ").append(toIndentedString(arrivedGirls)).append("\n");
        sb.append("    arrivedGuys: ").append(toIndentedString(arrivedGuys)).append("\n");
        sb.append("    notifyMgmtOnArrival: ").append(toIndentedString(notifyMgmtOnArrival)).append("\n");
        sb.append("    bookingNote: ").append(toIndentedString(bookingNote)).append("\n");
        sb.append("    complimentGirls: ").append(toIndentedString(complimentGirls)).append("\n");
        sb.append("    complimentGuys: ").append(toIndentedString(complimentGuys)).append("\n");
        sb.append("    complimentGuysQty: ").append(toIndentedString(complimentGuysQty)).append("\n");
        sb.append("    complimentGirlsQty: ").append(toIndentedString(complimentGirlsQty)).append("\n");
        sb.append("    reducedGirls: ").append(toIndentedString(reducedGirls)).append("\n");
        sb.append("    reducedGuys: ").append(toIndentedString(reducedGuys)).append("\n");
        sb.append("    reducedGuysQty: ").append(toIndentedString(reducedGuysQty)).append("\n");
        sb.append("    reducedGirlsQty: ").append(toIndentedString(reducedGirlsQty)).append("\n");
        sb.append("    mustEnter: ").append(toIndentedString(mustEnter)).append("\n");
        sb.append("    groupType: ").append(toIndentedString(groupType)).append("\n");
        sb.append("    bottleMin: ").append(toIndentedString(bottleMin)).append("\n");
        sb.append("    minSpend: ").append(toIndentedString(minSpend)).append("\n");
        sb.append("    status: ").append(toIndentedString(status)).append("\n");
        sb.append("    eventId: ").append(toIndentedString(eventId)).append("\n");
        sb.append("    tags: ").append(toIndentedString(tags)).append("\n");
        sb.append("    photos: ").append(toIndentedString(photos)).append("\n");
        sb.append("    staff: ").append(toIndentedString(staff)).append("\n");
        sb.append("    tableInfo: ").append(toIndentedString(tableInfo)).append("\n");
        sb.append("    bookedBy: ").append(toIndentedString(bookedBy)).append("\n");
        sb.append("    bottleService: ").append(toIndentedString(bottleService)).append("\n");
        sb.append("    totalSpent: ").append(toIndentedString(totalSpent)).append("\n");
        sb.append("    rating: ").append(toIndentedString(rating)).append("\n");
        sb.append("    venueId: ").append(toIndentedString(venueId)).append("\n");
        sb.append("    reservationDate: ").append(toIndentedString(reservationDate)).append("\n");
        sb.append("    feedbackCount: ").append(toIndentedString(feedbackCount)).append("\n");
        sb.append("    arrivalTime: ").append(toIndentedString(arrivalTime)).append("\n");
        sb.append("    estimatedArrivalTime: ").append(toIndentedString(estimatedArrivalTime)).append("\n");
        sb.append("    statusChangeTime: ").append(toIndentedString(statusChangeTime)).append("\n");
        sb.append("    deleted: ").append(toIndentedString(deleted)).append("\n");
        sb.append("    payees: ").append(toIndentedString(payees)).append("\n");
        sb.append("    completionFeedback: ").append(toIndentedString(completionFeedback)).append("\n");
        sb.append("    compleredBy: ").append(toIndentedString(compleredBy)).append("\n");
        sb.append("    confirmedBy: ").append(toIndentedString(confirmedBy)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

