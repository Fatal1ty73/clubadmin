package ru.dz.clubadmin.domain.models;

import io.realm.RealmList;
import io.realm.RealmObject;

import java.util.ArrayList;
import java.util.List;


public class Reservation extends RealmObject {


    private Long id;
    private String guestFullName;
    private String token = null;
    private User guest;
    private User completedBy = null;
    private User confirmedBy = null;
    private String bookingNote = null;
    private Integer guestsNumber = null;
    private Integer guysSeated = null;
    private Integer girlsSeated = null;
    private Boolean notifyMgmtOnArrival = false;
    private Boolean complimentGirls = false;
    private Boolean complimentGuys = false;
    private Integer complimentGuysQty = null;
    private Integer complimentGirlsQty = null;
    private Boolean reducedGirls = false;
    private Boolean reducedGuys = false;
    private Integer reducedGuysQty = null;
    private Integer reducedGirlsQty = null;
    private Boolean mustEnter = false;
    private Boolean deleted = false;
    private String groupType;
    private Integer bottleMin = null;
    private Integer minSpend = null;
    private RealmList<ReservationUser> staff = new RealmList<>();
    private RealmList<Picture> photos = new RealmList<>();
    private Place table = null;
    private User bookedBy = null;
    private RealmList<PayInfo> payees = new RealmList<>();
    private RealmList<RealmString> tags = new RealmList<>();

    private String bottleService;
    private Integer totalSpent;
    private LocalEventTime arrivalTime;
    private LocalEventTime estimatedArrivalTime = null;
    private LocalEventTime statusChangeTime = null;
    private RealmList<FeedbackInfo> feedbackList = new RealmList<>();
    private Feedback completionFeedback = null;
    private String status;
    private EventInstance eventInstance;
    private Place releasedFrom = null;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGuestFullName() {
        return guestFullName;
    }

    public void setGuestFullName(String guestFullName) {
        this.guestFullName = guestFullName;
    }

    public Integer getGuestsNumber() {
        return guestsNumber;
    }

    public void setGuestsNumber(Integer guestsNumber) {
        this.guestsNumber = guestsNumber;
    }

    public Boolean getNotifyMgmtOnArrival() {
        return notifyMgmtOnArrival;
    }

    public void setNotifyMgmtOnArrival(Boolean notifyMgmtOnArrival) {
        this.notifyMgmtOnArrival = notifyMgmtOnArrival;
    }

    public Boolean getComplimentGirls() {
        return complimentGirls;
    }

    public void setComplimentGirls(Boolean complimentGirls) {
        this.complimentGirls = complimentGirls;
    }

    public Boolean getReducedGirls() {
        return reducedGirls;
    }

    public void setReducedGirls(Boolean reducedGirls) {
        this.reducedGirls = reducedGirls;
    }

    public Boolean getMustEnter() {
        return mustEnter;
    }

    public void setMustEnter(Boolean mustEnter) {
        this.mustEnter = mustEnter;
    }

    public void setGroupType(GroupType groupType) {
        this.groupType = groupType.name();
    }

    public GroupType getGroupType() {
        return GroupType.valueOf(this.groupType);
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public Integer getBottleMin() {
        return bottleMin;
    }

    public void setBottleMin(Integer bottleMin) {
        this.bottleMin = bottleMin;
    }

    public Integer getMinSpend() {
        return minSpend;
    }

    public void setMinSpend(Integer minSpend) {
        this.minSpend = minSpend;
    }

    public Place getPlace() {
        return table;
    }

    public void setPlace(Place table) {
        this.table = table;
    }

    public User getBookedBy() {
        return bookedBy;
    }

    public void setBookedBy(User bookedBy) {
        this.bookedBy = bookedBy;
    }

    public void setBottleService(BottleServiceTypeEnum bottleServiceType) {
        this.bottleService = bottleServiceType.name();
    }

    public BottleServiceTypeEnum getBottleService() {
        return BottleServiceTypeEnum.valueOf(this.bottleService);
    }

    public void setBottleService(String bottleService) {
        this.bottleService = bottleService;
    }

    public Integer getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(Integer totalSpent) {
        this.totalSpent = totalSpent;
    }

    public String getBookingNote() {
        return bookingNote;
    }

    public void setBookingNote(String bookingNote) {
        this.bookingNote = bookingNote;
    }

    public User getGuest() {
        return guest;
    }

    public void setGuest(User guest) {
        this.guest = guest;
    }

    public ReservationStatus getStatus() {
        return ReservationStatus.valueOf(this.status);
    }

    public void setStatus(ReservationStatus status) {
        this.status = status.name();
    }

    public LocalEventTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalEventTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public List<FeedbackInfo> getFeedbackList() {
        return feedbackList;
    }

    public void setFeedbackList(List<FeedbackInfo> feedbackList) {
        RealmList<FeedbackInfo> feedbackRealmList = new RealmList<>();
        for (FeedbackInfo feedback : feedbackList) feedbackRealmList.add(feedback);
        this.feedbackList = feedbackRealmList;
    }

    public EventInstance getEventInstance() {
        return eventInstance;
    }

    public void setEventInstance(EventInstance eventInstance) {
        this.eventInstance = eventInstance;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getCompletedBy() {
        return completedBy;
    }

    public void setCompletedBy(User completedBy) {
        this.completedBy = completedBy;
    }

    public User getConfirmedBy() {
        return confirmedBy;
    }

    public void setConfirmedBy(User confirmedBy) {
        this.confirmedBy = confirmedBy;
    }

    public Integer getGuysSeated() {
        return guysSeated;
    }

    public void setGuysSeated(Integer guysSeated) {
        this.guysSeated = guysSeated;
    }

    public Integer getGirlsSeated() {
        return girlsSeated;
    }

    public void setGirlsSeated(Integer girlsSeated) {
        this.girlsSeated = girlsSeated;
    }

    public Boolean getComplimentGuys() {
        return complimentGuys;
    }

    public void setComplimentGuys(Boolean complimentGuys) {
        this.complimentGuys = complimentGuys;
    }

    public Integer getComplimentGuysQty() {
        return complimentGuysQty;
    }

    public void setComplimentGuysQty(Integer complimentGuysQty) {
        this.complimentGuysQty = complimentGuysQty;
    }

    public Integer getComplimentGirlsQty() {
        return complimentGirlsQty;
    }

    public void setComplimentGirlsQty(Integer complimentGirlsQty) {
        this.complimentGirlsQty = complimentGirlsQty;
    }

    public Boolean getReducedGuys() {
        return reducedGuys;
    }

    public void setReducedGuys(Boolean reducedGuys) {
        this.reducedGuys = reducedGuys;
    }

    public Integer getReducedGuysQty() {
        return reducedGuysQty;
    }

    public void setReducedGuysQty(Integer reducedGuysQty) {
        this.reducedGuysQty = reducedGuysQty;
    }

    public Integer getReducedGirlsQty() {
        return reducedGirlsQty;
    }

    public void setReducedGirlsQty(Integer reducedGirlsQty) {
        this.reducedGirlsQty = reducedGirlsQty;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<ReservationUser> getStaff() {
        return staff;
    }

    public void setStaff(List<ReservationUser> staffs) {
        RealmList<ReservationUser> staffList = new RealmList<>();
        for (ReservationUser staff : staffs) staffList.add(staff);
        this.staff = staffList;
    }

    public List<Picture> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Picture> photos) {
        RealmList<Picture> photoList = new RealmList<>();
        for (Picture picture : photos) photoList.add(picture);
        this.photos = photoList;
    }

    public Place getReservationTable() {
        return table;
    }

    public void setReservationTable(Place table) {
        this.table = table;
    }

    public RealmList<PayInfo> getPayees() {
        return payees;
    }

    public void setPayees(List<PayInfo> payees) {
        RealmList<PayInfo> payList = new RealmList<>();
        for (PayInfo payInfo : payees) payList.add(payInfo);
        this.payees = payList;
    }

    public List<String> getTags() {
        ArrayList<String> tagsList = new ArrayList<>();
        if (!this.tags.isEmpty()) {
            for (RealmString tag : this.tags) tagsList.add(tag.getString());
        }
        return tagsList;
    }

    public void setTags(List<String> tags) {
        RealmList<RealmString> tagsList = new RealmList<>();
        for (String tag : tags) tagsList.add(new RealmString(tag));
        this.tags = tagsList;
    }

    public LocalEventTime getEstimatedArrivalTime() {
        return estimatedArrivalTime;
    }

    public void setEstimatedArrivalTime(LocalEventTime estimatedArrivalTime) {
        this.estimatedArrivalTime = estimatedArrivalTime;
    }

    public LocalEventTime getStatusChangeTime() {
        return statusChangeTime;
    }

    public void setStatusChangeTime(LocalEventTime statusChangeTime) {
        this.statusChangeTime = statusChangeTime;
    }

    public Feedback getCompletionFeedback() {
        return completionFeedback;
    }

    public void setCompletionFeedback(Feedback completionFeedback) {
        this.completionFeedback = completionFeedback;
    }

    public Place getReleasedFrom() {
        return releasedFrom;
    }

    public void setReleasedFrom(Place releasedFrom) {
        this.releasedFrom = releasedFrom;
    }
}