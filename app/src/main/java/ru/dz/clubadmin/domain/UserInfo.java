package ru.dz.clubadmin.domain;


import io.realm.RealmList;
import io.realm.RealmObject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import ru.dz.clubadmin.domain.models.FacebookInfo;
import ru.dz.clubadmin.domain.models.PromotionCompany;
import ru.dz.clubadmin.domain.models.RealmString;
import ru.dz.clubadmin.domain.models.VenueRole;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arkady on 12/02/16.
 */

public class UserInfo extends RealmObject {

	private Long id;

	private String fullName;

	private String email;

	private String userpic;

	private String phoneNumber;

	private String birthday;

	private RealmList<RealmString> preferredRoles;

	private boolean hasFacebookProfile;

	private FacebookInfo facebookInfo;

	private PromotionCompany promotionCompany;

	private boolean isAdmin;

	public UserInfo() {
	}

	public UserInfo(Long id, String fullName, String email, String userpic, String phoneNumber, String birthday, RealmList<RealmString> preferredRoles, boolean hasFacebookProfile, FacebookInfo facebookInfo, PromotionCompany promotionCompany, Boolean isAdmin) {
		this.id = id;
		this.fullName = fullName;
		this.email = email;
		this.userpic = userpic;
		this.phoneNumber = phoneNumber;
		this.birthday = birthday;
		this.preferredRoles = preferredRoles;
		this.hasFacebookProfile = hasFacebookProfile;
		this.facebookInfo = facebookInfo;
		this.promotionCompany = promotionCompany;
		this.isAdmin = isAdmin;
	}


	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public DateTime getBirthdayDateTime() {
		if(this.birthday!= null) {
			return DateTime.parse(birthday);
		} else return null;
	}

	public void setBirthdayDateTime(DateTime birthday) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
		String str = fmt.print(birthday);
		this.birthday = str;
	}

	public List<VenueRole> getPreferredRoles() {
		ArrayList<VenueRole> preferredRolesString = new ArrayList<>();
		if(this.preferredRoles != null) {
			if (!this.preferredRoles.isEmpty()) {
				for (RealmString role : this.preferredRoles)
					preferredRolesString.add(VenueRole.valueOf(role.getString()));
			}
		}
		return preferredRolesString;
	}

	public void setPreferredRoles(List<VenueRole> preferredRoles) {
		RealmList<RealmString> preferredRolesString = new RealmList<>();
		for(VenueRole role : preferredRoles) preferredRolesString.add(new RealmString(role.name()));
		this.preferredRoles = preferredRolesString;
	}

	public String getUserpic() {
		return userpic;
	}

	public void setUserpic(String userpic) {
		this.userpic = userpic;
	}

	public void setHasFacebookProfile(boolean hasFacebookProfile) {
		this.hasFacebookProfile = hasFacebookProfile;
	}

	public boolean isHasFacebookProfile() {
		return hasFacebookProfile;
	}

	public FacebookInfo getFacebookInfo() {
		return facebookInfo;
	}

	public void setFacebookInfo(FacebookInfo facebookInfo) {
		this.facebookInfo = facebookInfo;
	}

	public PromotionCompany getPromotionCompany() {
		return promotionCompany;
	}

	public void setPromotionCompany(PromotionCompany promotionCompany) {
		this.promotionCompany = promotionCompany;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
}
