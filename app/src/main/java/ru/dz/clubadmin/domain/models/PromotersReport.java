package ru.dz.clubadmin.domain.models;

import java.util.ArrayList;
import java.util.List;


/**
 * PromotersReport
 */
public class PromotersReport   {
  
  private List<ReservationItem> reservationItems = new ArrayList<ReservationItem>();
  private List<Promoter> promoters = new ArrayList<Promoter>();

  
  /**
   **/
  public PromotersReport reservationItems(List<ReservationItem> reservationItems) {
    this.reservationItems = reservationItems;
    return this;
  }

  public List<ReservationItem> getReservationItems() {
    return reservationItems;
  }
  public void setReservationItems(List<ReservationItem> reservationItems) {
    this.reservationItems = reservationItems;
  }


  /**
   **/
  public PromotersReport promoters(List<Promoter> promoters) {
    this.promoters = promoters;
    return this;
  }

  public List<Promoter> getPromoters() {
    return promoters;
  }
  public void setPromoters(List<Promoter> promoters) {
    this.promoters = promoters;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    PromotersReport that = (PromotersReport) o;

    if (reservationItems != null ? !reservationItems.equals(that.reservationItems) : that.reservationItems != null)
      return false;
    return promoters != null ? promoters.equals(that.promoters) : that.promoters == null;

  }

  @Override
  public int hashCode() {
    int result = reservationItems != null ? reservationItems.hashCode() : 0;
    result = 31 * result + (promoters != null ? promoters.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PromotersReport {\n");
    
    sb.append("    reservationItems: ").append(toIndentedString(reservationItems)).append("\n");
    sb.append("    promoters: ").append(toIndentedString(promoters)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

