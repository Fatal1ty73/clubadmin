package ru.dz.clubadmin.domain.models;

import io.realm.RealmObject;

/**
 * Created by arkady on 16/02/16.
 */

public class UserVenuePK extends RealmObject {

    Long userId;

    Long venueId;


    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserVenuePK that = (UserVenuePK) o;

        if (!userId.equals(that.userId)) return false;
        return venueId.equals(that.venueId);
    }


    public int hashCode() {
        int result = userId.hashCode();
        result = 31 * result + venueId.hashCode();
        return result;
    }
}
