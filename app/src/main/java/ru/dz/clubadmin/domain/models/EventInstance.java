package ru.dz.clubadmin.domain.models;

import io.realm.RealmList;
import io.realm.RealmObject;

import java.util.List;

/**
 * Created by arkady on 11/03/16.
 */

public class EventInstance extends RealmObject {


    private DateVenuePk dateVenuePk = new DateVenuePk();


    private Venue venue;


    private RealmList<Reservation> reservations;

    public EventInstance() {
    }

    public EventInstance(DateVenuePk dateVenuePk) {
        this.dateVenuePk = dateVenuePk;
    }

    public DateVenuePk getDateVenuePk() {
        return dateVenuePk;
    }

    public void setDateVenuePk(DateVenuePk dateVenuePk) {
        this.dateVenuePk = dateVenuePk;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        RealmList<Reservation> reservationRealmList = new RealmList<>();
        for(Reservation reservation: reservations) reservationRealmList.add(reservation);
        this.reservations = reservationRealmList;
    }


    public String toString() {
        return "EventInstance{" +
                "dateVenuePk=" + dateVenuePk +
                ", venue=" + venue +
                ", reservations=" + reservations +
                '}';
    }
}
