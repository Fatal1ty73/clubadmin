package ru.dz.clubadmin.domain.models;


import io.realm.RealmList;
import io.realm.RealmObject;
import ru.dz.clubadmin.domain.UserInfo;

import java.util.ArrayList;
import java.util.List;

public class FeedbackInfo extends RealmObject {
    private Long id;
    private UserInfo author;
    private FeedbackReservationInfo shortReservationInfo;
    private String time;
    private String staffAssignment;
    private String message;
    private Integer rating;
    private String authorRole;
    private RealmList<RealmString> tags;


    public List<String> getTags() {
        ArrayList<String> tagsArray = new ArrayList<>();
        for(RealmString tag : this.tags) tagsArray.add(tag.getString());
        return tagsArray;
    }

    public void setTags(List<String> tagsList) {
        RealmList<RealmString> tagsArray = new RealmList<>();
        for(String tag : tagsList) tagsArray.add(new RealmString(tag));
        this.tags = tagsArray;
    }

    public VenueRole getAuthorRole() {
        return VenueRole.valueOf(authorRole) ;
    }

    public void setAuthorRole(VenueRole authorRole) {
        this.authorRole = authorRole.name();
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStaffAssignment() {
        return staffAssignment;
    }

    public void setStaffAssignment(String staffAssignment) {
        this.staffAssignment = staffAssignment;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public FeedbackReservationInfo getShortReservationInfo() {
        return shortReservationInfo;
    }

    public void setShortReservationInfo(FeedbackReservationInfo shortReservationInfo) {
        this.shortReservationInfo = shortReservationInfo;
    }

    public UserInfo getAuthor() {
        return author;
    }

    public void setAuthor(UserInfo author) {
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}
