package ru.dz.clubadmin.domain;

import io.realm.RealmList;
import io.realm.RealmObject;

import java.util.List;

/**
 * Created by arkady on 20/03/16.
 */
public class SeatingPlaceList extends RealmObject {

	RealmList<TableInfo> table = new RealmList<>();
	RealmList<TableInfo> standup = new RealmList<>();
	RealmList<TableInfo> bar = new RealmList<>();

	public List<TableInfo> getTableInfo() {
		return table;
	}

	public void setTableInfo(List<TableInfo> table) {
		this.table = new RealmList<>();
		this.table.addAll(table);
	}

	public List<TableInfo> getStandup() {
		return standup;
	}

	public void setStandup(List<TableInfo> standup) {
		this.standup = new RealmList<>();
		this.standup.addAll(standup);
	}

	public List<TableInfo> getBar() {
		return bar;
	}

	public void setBar(List<TableInfo> bar) {
		this.bar = new RealmList<>();
		this.bar.addAll(bar);
	}
}
