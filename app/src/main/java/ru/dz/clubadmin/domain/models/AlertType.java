package ru.dz.clubadmin.domain.models;


public enum AlertType {
    POLICE("Police"),
    FIRE("Fire"),
    LIQUOR_BOARD("Liquor board"),
    MLS("MLS"),
    CAPACITY_REACHED("Capacity reached"),
    VIP_ARRIVAL("Vip arrival");

    private String name;
    AlertType(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return name;
    }
}
