package ru.dz.clubadmin.domain.models;

import io.realm.RealmObject;

import java.io.Serializable;

/**
 * Created by arkady on 09/03/16.
 */


public class EventInfo extends RealmObject implements Serializable {

	
	private Long id;
	private String name;
	private String description;
	private String fbEventUrl;
	private String startsAt;
	
	private String date;
	
	private Integer venueId;
	private Boolean repeatable;
	private String pictureUrl;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFbEventUrl() {
		return fbEventUrl;
	}

	public void setFbEventUrl(String fbEventUrl) {
		this.fbEventUrl = fbEventUrl;
	}


	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	public Boolean getRepeatable() {
		return repeatable;
	}

	public void setRepeatable(Boolean repeatable) {
		this.repeatable = repeatable;
	}

	public String getStartsAt() {
		return startsAt;
	}

	public void setStartsAt(String startsAt) {
		this.startsAt = startsAt;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
}