package ru.dz.clubadmin.domain;


import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Sam (samir@peller.tech) on 27.05.2016.
 */
public class TableWithSeating extends RealmObject {
    private TableInfo tableInfo;
    private ReservationInfo reservationInfo;

    public TableWithSeating() {}

    public TableWithSeating(TableInfo tableInfo, ReservationInfo reservationInfo, RealmList<StaffAssignment> staffList) {
        this.tableInfo = tableInfo;
        this.reservationInfo = reservationInfo;
    }

    public TableInfo getTableInfo() {
        return tableInfo;
    }

    public void setTableInfo(TableInfo tableInfo) {
        this.tableInfo = tableInfo;
    }

    public ReservationInfo getReservationInfo() {
        return reservationInfo;
    }

    public void setReservationInfo(ReservationInfo reservationInfo) {
        this.reservationInfo = reservationInfo;
    }
}
