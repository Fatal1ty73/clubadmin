package ru.dz.clubadmin.domain.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by arkady on 15/02/16.
 */
public enum VenueRole {

	MANAGER("Manager"),
	OFFICE("Office"),
	PROMOTER("Promoter"),
	HEAD_DOORMAN("Head Doorman"),
	SERVER("Server"),
	BARBACK("Barback"),
	GUEST_LIST("Guest List"),
	WILL_CALL("Will Call"),
	SECURITY("Security"),
	ASSISTANT_MANAGER("Assistant Manager"),
	VIP_HOST("VIP Host"),
	HEAD_BUSSER("Head Busser"),
	BARTENDER("Bartender"),
	BUSSER("Busser"),
	FRONT_DOOR_CASH("Front Door Cash"),
	COAT_CHECK("Coat Check"),
	FRONT_DOOR_SECURITY("Front Door Security"),
	INVENTORY("INVENTORY");

	private String name;
	private static Map<String, String> roleToNameMapping;

	VenueRole(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	public static Map<String, String> all() {
		if (roleToNameMapping == null) {
			initMapping();
		}

		return roleToNameMapping;
	}

	private static void initMapping() {
		roleToNameMapping = new HashMap<>();
		for (VenueRole s : values()) {
			roleToNameMapping.put(s.name(), s.name);
		}
	}
}
