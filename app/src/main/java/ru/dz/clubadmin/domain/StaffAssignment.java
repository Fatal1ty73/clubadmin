package ru.dz.clubadmin.domain;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by Sam (samir@peller.tech) on 27.05.2016.
 */
public class StaffAssignment extends RealmObject implements Serializable{
    private long id;
    private String name;
    private String phone;
    private String role;

    public StaffAssignment() {}

    public StaffAssignment(long id, String name, String phone, String role) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
