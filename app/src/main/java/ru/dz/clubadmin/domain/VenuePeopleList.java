package ru.dz.clubadmin.domain;

import io.realm.RealmList;
import io.realm.RealmObject;

import java.util.List;

/**
 * Created by arkady on 01/03/16.
 */
public class VenuePeopleList extends RealmObject {

	
	RealmList<VenueStaffInfo> requestList;

	RealmList<VenueStaffInfo> approvedList;

	RealmList<VenueStaffInfo> previousList;

	public RealmList<VenueStaffInfo> getPreviousList() {
		return previousList;
	}

	public void setPreviousList(RealmList<VenueStaffInfo> previousList) {
		this.previousList = previousList;
	}

	public VenuePeopleList() {
		requestList = new RealmList<>();
		approvedList = new RealmList<>();
		previousList = new RealmList<>();
	}

	public List<VenueStaffInfo> getRequestList() {
		return requestList;
	}

	public void setRequestList(List<VenueStaffInfo> requestList) {
		RealmList<VenueStaffInfo> venueStaffInfoRealmList = new RealmList<>();
		for(VenueStaffInfo reservation: requestList) venueStaffInfoRealmList.add(reservation);
		this.requestList = venueStaffInfoRealmList;
	}

	public List<VenueStaffInfo> getApprovedList() {
		return approvedList;
	}

	public void setApprovedList(List<VenueStaffInfo> approvedList) {
		RealmList<VenueStaffInfo> venueStaffInfoRealmList = new RealmList<>();
		for(VenueStaffInfo reservation: approvedList) venueStaffInfoRealmList.add(reservation);
		this.approvedList = venueStaffInfoRealmList;
	}
}
