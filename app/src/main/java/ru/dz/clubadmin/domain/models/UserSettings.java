package ru.dz.clubadmin.domain.models;


import io.realm.RealmObject;

public class UserSettings extends RealmObject {
    private boolean postOnMyBehalf;
    private boolean rsvpOnMyBehalf;
    private boolean checkInOnMyBehalf;
    private boolean smsMessages;
    private boolean inAppNotification;

    public UserSettings() {
    }

    public UserSettings(boolean postOnMyBehalf, boolean rsvpOnMyBehalf, boolean checkInOnMyBehalf, boolean smsMessages, boolean inAppNotification) {
        this.postOnMyBehalf = postOnMyBehalf;
        this.rsvpOnMyBehalf = rsvpOnMyBehalf;
        this.checkInOnMyBehalf = checkInOnMyBehalf;
        this.smsMessages = smsMessages;
        this.inAppNotification = inAppNotification;
    }

    public boolean isPostOnMyBehalf() {
        return postOnMyBehalf;
    }

    public void setPostOnMyBehalf(boolean postOnMyBehalf) {
        this.postOnMyBehalf = postOnMyBehalf;
    }

    public boolean isRsvpOnMyBehalf() {
        return rsvpOnMyBehalf;
    }

    public void setRsvpOnMyBehalf(boolean rsvpOnMyBehalf) {
        this.rsvpOnMyBehalf = rsvpOnMyBehalf;
    }

    public boolean isCheckInOnMyBehalf() {
        return checkInOnMyBehalf;
    }

    public void setCheckInOnMyBehalf(boolean checkInOnMyBehalf) {
        this.checkInOnMyBehalf = checkInOnMyBehalf;
    }

    public boolean isSmsMessages() {
        return smsMessages;
    }

    public void setSmsMessages(boolean smsMessages) {
        this.smsMessages = smsMessages;
    }

    public boolean isInAppNotification() {
        return inAppNotification;
    }

    public void setInAppNotification(boolean inAppNotification) {
        this.inAppNotification = inAppNotification;
    }
}
