package ru.dz.clubadmin.domain;

import ru.dz.clubadmin.domain.models.EventInfo;

import java.util.List;


public class EventsList {
    private List<EventInfo> eventsList;

    public EventsList(List<EventInfo> eventsList) {
        this.eventsList = eventsList;
    }

    public List<EventInfo> getEventsList() {
        return eventsList;
    }

    public void setEventsList(List<EventInfo> eventsList) {
        this.eventsList = eventsList;
    }
}
