package ru.dz.clubadmin.domain;

/**
 * Created by nikitakulagin on 06.05.16.
 */
public class CurrentClickerHumanStats {
    private long men;
    private long women;

    public CurrentClickerHumanStats() {
    }

    public CurrentClickerHumanStats(long men, long women) {
        this.men = men;
        this.women = women;
    }

    public long getMen() {
        return men;
    }

    public void setMen(long men) {
        this.men = men;
    }

    public long getWomen() {
        return women;
    }

    public void setWomen(long women) {
        this.women = women;
    }
}
