package ru.dz.clubadmin.domain.models;


public enum ReportTypeEnum {
    DAILY,WEEKLY,MONTHLY,INTERVAL
}
