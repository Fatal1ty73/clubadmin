package ru.dz.clubadmin.domain.models;


public class StaffAssignment {
    private Long id;
    private String name;
    private String phone;
    private VenueRole role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public VenueRole getRole() {
        return role;
    }

    public void setRole(VenueRole role) {
        this.role = role;
    }
}
