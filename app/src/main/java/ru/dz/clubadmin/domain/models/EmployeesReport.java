package ru.dz.clubadmin.domain.models;

import ru.dz.clubadmin.domain.SearchUserInfo;

import java.util.ArrayList;
import java.util.List;


/**
 * EmployeesReport
 */
public class EmployeesReport   {
  
  private List<ReservationItem> reservationItems = new ArrayList<ReservationItem>();
  private List<SearchUserInfo> employees = new ArrayList<SearchUserInfo>();

  
  /**
   **/
  public EmployeesReport reservationItems(List<ReservationItem> reservationItems) {
    this.reservationItems = reservationItems;
    return this;
  }

  public List<ReservationItem> getReservationItems() {
    return reservationItems;
  }
  public void setReservationItems(List<ReservationItem> reservationItems) {
    this.reservationItems = reservationItems;
  }


  /**
   **/
  public EmployeesReport employees(List<SearchUserInfo> employees) {
    this.employees = employees;
    return this;
  }
  

  public List<SearchUserInfo> getEmployees() {
    return employees;
  }
  public void setEmployees(List<SearchUserInfo> employees) {
    this.employees = employees;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    EmployeesReport that = (EmployeesReport) o;

    if (reservationItems != null ? !reservationItems.equals(that.reservationItems) : that.reservationItems != null)
      return false;
    return employees != null ? employees.equals(that.employees) : that.employees == null;

  }

  @Override
  public int hashCode() {
    int result = reservationItems != null ? reservationItems.hashCode() : 0;
    result = 31 * result + (employees != null ? employees.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EmployeesReport {\n");
    
    sb.append("    reservationItems: ").append(toIndentedString(reservationItems)).append("\n");
    sb.append("    employees: ").append(toIndentedString(employees)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

