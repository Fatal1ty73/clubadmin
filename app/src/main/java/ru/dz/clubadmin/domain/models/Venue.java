package ru.dz.clubadmin.domain.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arkady on 15/02/16.
 */


public class Venue extends RealmObject implements Serializable  {


    private Long id;

    private String name;

    private String address;

    private String placeId;

    private String coverUrl;

    private String logoUrl;

    private Integer capacity;

    private String timeZone = DateTime.now().getZone().getID(); //TODO get timezone from venue address

    private RealmList<RealmString> tags;

    private String requestStatus;

    private RealmList<RealmString> preferredRoles;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }


    public void setRequestStatus(VenueRequestStatus requestStatus) {
        this.requestStatus = requestStatus.name();
    }

    public VenueRequestStatus getRequestStatus() {
        if(this.requestStatus != null) {
            return VenueRequestStatus.valueOf(this.requestStatus);
        } else return null;
    }

    public List<VenueRole> getPreferredRoles() {
        ArrayList<VenueRole> preferredRolesString = new ArrayList<>();
        if (this.preferredRoles != null) {
            for (RealmString role : this.preferredRoles) preferredRolesString.add(VenueRole.valueOf(role.getString()));
        }
        return preferredRolesString;
    }

    public void setPreferredRoles(List<VenueRole> preferredRoles) {
        RealmList<RealmString> preferredRolesString = new RealmList<>();
        for(VenueRole role : preferredRoles) preferredRolesString.add(new RealmString(role.name()));
        this.preferredRoles = preferredRolesString;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public List<String>  getTags() {
        ArrayList<String> tagsList = new ArrayList<>();
        if (this.tags != null) {
            for (RealmString tag : this.tags) tagsList.add(tag.getString());
        }
        return tagsList;
    }

    public void setTags(List<String> tags) {
        RealmList<RealmString> tagList = new RealmList<>();
        for(String tag : tags) tagList.add(new RealmString(tag));
        this.tags = tagList;
    }
}
