package ru.dz.clubadmin.domain.models;

/**
 * Created by arkady on 08/03/16.
 */
public enum ReservationStatus {
	PENDING("PENDING"),
	APPROVED("APPROVED"),
	ARRIVED("ARRIVED"),
	PRE_RELEASED("PRE_RELEASED"),
	RELEASED("RELEASED"),
	COMPLETED("COMPLETED"),
	NO_SHOW("NO_SHOW"),
	CONFIRMED_COMPLETE("CONFIRMED_COMPLETE");

	private String value;

	ReservationStatus(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}}