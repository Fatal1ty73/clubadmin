package ru.dz.clubadmin.domain.models;

import io.realm.RealmList;
import io.realm.RealmObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Feedback
 */
public class Feedback extends RealmObject {

    private Long id = null;
    private User user = null;
    private User author = null;
    private String authorRole = null;
    private Reservation reservation = null;
    private String message = null;
    private String staffAssignment = null;
    private Integer stars = null;
    private LocalEventTime time = null;
    private Boolean meta = false;
    private RealmList<RealmString> tags = new RealmList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public VenueRole getAuthorRole() {
        if (this.authorRole != null) {
            return VenueRole.valueOf(this.authorRole);
        } else return null;
    }

    public void setAuthorRole(VenueRole authorRole) {
        this.authorRole = authorRole.name();
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStaffAssignment() {
        return staffAssignment;
    }

    public void setStaffAssignment(String staffAssignment) {
        this.staffAssignment = staffAssignment;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public LocalEventTime getTime() {
        return time;
    }

    public void setTime(LocalEventTime time) {
        this.time = time;
    }

    public Boolean getMeta() {
        return meta;
    }

    public void setMeta(Boolean meta) {
        this.meta = meta;
    }

    public List<String> getTags() {
        ArrayList<String> tagsArray = new ArrayList<>();
        for (RealmString tag : this.tags) tagsArray.add(tag.getString());
        return tagsArray;
    }

    public void setTags(List<String> tagsList) {
        RealmList<RealmString> tagsArray = new RealmList<>();
        for (String tag : tagsList) tagsArray.add(new RealmString(tag));
        this.tags = tagsArray;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Feedback {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    user: ").append(toIndentedString(user)).append("\n");
        sb.append("    author: ").append(toIndentedString(author)).append("\n");
        sb.append("    authorRole: ").append(toIndentedString(authorRole)).append("\n");
        sb.append("    reservation: ").append(toIndentedString(reservation)).append("\n");
        sb.append("    message: ").append(toIndentedString(message)).append("\n");
        sb.append("    staffAssignment: ").append(toIndentedString(staffAssignment)).append("\n");
        sb.append("    stars: ").append(toIndentedString(stars)).append("\n");
        sb.append("    time: ").append(toIndentedString(time)).append("\n");
        sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
        sb.append("    tags: ").append(toIndentedString(tags)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

