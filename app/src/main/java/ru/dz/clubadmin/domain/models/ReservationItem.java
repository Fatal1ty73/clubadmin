package ru.dz.clubadmin.domain.models;

/**
 * ReservationItem
 */
public class ReservationItem   {
  
  private String userpic = null;
  private String fullName = null;
  private String reservedBy = null;
  private Long reservedById = null;
  private String arrived = null;
  private String date = null;
  private Integer rating = null;
  private Integer minSpend = null;
  private Integer actualSpent = null;
  private Integer guestsBooked = null;
  private Integer guestsActual = null;


  private BottleServiceTypeEnum bottleServiceType = null;
  private Boolean isActualReservation = false;
  private Integer guysSeated = null;
  private Integer girlsSeated = null;

  
  /**
   **/
  public ReservationItem userpic(String userpic) {
    this.userpic = userpic;
    return this;
  }

  public String getUserpic() {
    return userpic;
  }
  public void setUserpic(String userpic) {
    this.userpic = userpic;
  }


  /**
   **/
  public ReservationItem fullName(String fullName) {
    this.fullName = fullName;
    return this;
  }

  public String getFullName() {
    return fullName;
  }
  public void setFullName(String fullName) {
    this.fullName = fullName;
  }


  /**
   **/
  public ReservationItem reservedBy(String reservedBy) {
    this.reservedBy = reservedBy;
    return this;
  }

  public String getReservedBy() {
    return reservedBy;
  }
  public void setReservedBy(String reservedBy) {
    this.reservedBy = reservedBy;
  }


  /**
   **/
  public ReservationItem reservedById(Long reservedById) {
    this.reservedById = reservedById;
    return this;
  }

  public Long getReservedById() {
    return reservedById;
  }
  public void setReservedById(Long reservedById) {
    this.reservedById = reservedById;
  }


  /**
   **/
  public ReservationItem arrived(String arrived) {
    this.arrived = arrived;
    return this;
  }

  public String getArrived() {
    return arrived;
  }
  public void setArrived(String arrived) {
    this.arrived = arrived;
  }


  /**
   **/
  public ReservationItem date(String date) {
    this.date = date;
    return this;
  }
  

  public String getDate() {
    return date;
  }
  public void setDate(String date) {
    this.date = date;
  }


  /**
   **/
  public ReservationItem rating(Integer rating) {
    this.rating = rating;
    return this;
  }

  public Integer getRating() {
    return rating;
  }
  public void setRating(Integer rating) {
    this.rating = rating;
  }


  /**
   **/
  public ReservationItem minSpend(Integer minSpend) {
    this.minSpend = minSpend;
    return this;
  }
  

  public Integer getMinSpend() {
    return minSpend;
  }
  public void setMinSpend(Integer minSpend) {
    this.minSpend = minSpend;
  }


  /**
   **/
  public ReservationItem actualSpent(Integer actualSpent) {
    this.actualSpent = actualSpent;
    return this;
  }
  

  public Integer getActualSpent() {
    return actualSpent;
  }
  public void setActualSpent(Integer actualSpent) {
    this.actualSpent = actualSpent;
  }


  /**
   **/
  public ReservationItem guestsBooked(Integer guestsBooked) {
    this.guestsBooked = guestsBooked;
    return this;
  }
  

  public Integer getGuestsBooked() {
    return guestsBooked;
  }
  public void setGuestsBooked(Integer guestsBooked) {
    this.guestsBooked = guestsBooked;
  }


  /**
   **/
  public ReservationItem guestsActual(Integer guestsActual) {
    this.guestsActual = guestsActual;
    return this;
  }
  

  public Integer getGuestsActual() {
    return guestsActual;
  }
  public void setGuestsActual(Integer guestsActual) {
    this.guestsActual = guestsActual;
  }


  /**
   **/
  public ReservationItem bottleServiceType(BottleServiceTypeEnum bottleServiceType) {
    this.bottleServiceType = bottleServiceType;
    return this;
  }
  

  public BottleServiceTypeEnum getBottleServiceType() {
    return bottleServiceType;
  }
  public void setBottleServiceType(BottleServiceTypeEnum bottleServiceType) {
    this.bottleServiceType = bottleServiceType;
  }


  /**
   **/
  public ReservationItem isActualReservation(Boolean isActualReservation) {
    this.isActualReservation = isActualReservation;
    return this;
  }

  public Boolean getIsActualReservation() {
    return isActualReservation;
  }
  public void setIsActualReservation(Boolean isActualReservation) {
    this.isActualReservation = isActualReservation;
  }


  /**
   **/
  public ReservationItem guysSeated(Integer guysSeated) {
    this.guysSeated = guysSeated;
    return this;
  }

  public Integer getGuysSeated() {
    return guysSeated;
  }
  public void setGuysSeated(Integer guysSeated) {
    this.guysSeated = guysSeated;
  }


  /**
   **/
  public ReservationItem girlsSeated(Integer girlsSeated) {
    this.girlsSeated = girlsSeated;
    return this;
  }

  public Integer getGirlsSeated() {
    return girlsSeated;
  }
  public void setGirlsSeated(Integer girlsSeated) {
    this.girlsSeated = girlsSeated;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ReservationItem that = (ReservationItem) o;

    if (userpic != null ? !userpic.equals(that.userpic) : that.userpic != null) return false;
    if (fullName != null ? !fullName.equals(that.fullName) : that.fullName != null) return false;
    if (reservedBy != null ? !reservedBy.equals(that.reservedBy) : that.reservedBy != null) return false;
    if (reservedById != null ? !reservedById.equals(that.reservedById) : that.reservedById != null) return false;
    if (arrived != null ? !arrived.equals(that.arrived) : that.arrived != null) return false;
    if (date != null ? !date.equals(that.date) : that.date != null) return false;
    if (rating != null ? !rating.equals(that.rating) : that.rating != null) return false;
    if (minSpend != null ? !minSpend.equals(that.minSpend) : that.minSpend != null) return false;
    if (actualSpent != null ? !actualSpent.equals(that.actualSpent) : that.actualSpent != null) return false;
    if (guestsBooked != null ? !guestsBooked.equals(that.guestsBooked) : that.guestsBooked != null) return false;
    if (guestsActual != null ? !guestsActual.equals(that.guestsActual) : that.guestsActual != null) return false;
    if (bottleServiceType != that.bottleServiceType) return false;
    if (isActualReservation != null ? !isActualReservation.equals(that.isActualReservation) : that.isActualReservation != null)
      return false;
    if (guysSeated != null ? !guysSeated.equals(that.guysSeated) : that.guysSeated != null) return false;
    return girlsSeated != null ? girlsSeated.equals(that.girlsSeated) : that.girlsSeated == null;

  }

  @Override
  public int hashCode() {
    int result = userpic != null ? userpic.hashCode() : 0;
    result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
    result = 31 * result + (reservedBy != null ? reservedBy.hashCode() : 0);
    result = 31 * result + (reservedById != null ? reservedById.hashCode() : 0);
    result = 31 * result + (arrived != null ? arrived.hashCode() : 0);
    result = 31 * result + (date != null ? date.hashCode() : 0);
    result = 31 * result + (rating != null ? rating.hashCode() : 0);
    result = 31 * result + (minSpend != null ? minSpend.hashCode() : 0);
    result = 31 * result + (actualSpent != null ? actualSpent.hashCode() : 0);
    result = 31 * result + (guestsBooked != null ? guestsBooked.hashCode() : 0);
    result = 31 * result + (guestsActual != null ? guestsActual.hashCode() : 0);
    result = 31 * result + (bottleServiceType != null ? bottleServiceType.hashCode() : 0);
    result = 31 * result + (isActualReservation != null ? isActualReservation.hashCode() : 0);
    result = 31 * result + (guysSeated != null ? guysSeated.hashCode() : 0);
    result = 31 * result + (girlsSeated != null ? girlsSeated.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ReservationItem {\n");
    
    sb.append("    userpic: ").append(toIndentedString(userpic)).append("\n");
    sb.append("    fullName: ").append(toIndentedString(fullName)).append("\n");
    sb.append("    reservedBy: ").append(toIndentedString(reservedBy)).append("\n");
    sb.append("    reservedById: ").append(toIndentedString(reservedById)).append("\n");
    sb.append("    arrived: ").append(toIndentedString(arrived)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    rating: ").append(toIndentedString(rating)).append("\n");
    sb.append("    minSpend: ").append(toIndentedString(minSpend)).append("\n");
    sb.append("    actualSpent: ").append(toIndentedString(actualSpent)).append("\n");
    sb.append("    guestsBooked: ").append(toIndentedString(guestsBooked)).append("\n");
    sb.append("    guestsActual: ").append(toIndentedString(guestsActual)).append("\n");
    sb.append("    bottleServiceType: ").append(toIndentedString(bottleServiceType)).append("\n");
    sb.append("    isActualReservation: ").append(toIndentedString(isActualReservation)).append("\n");
    sb.append("    guysSeated: ").append(toIndentedString(guysSeated)).append("\n");
    sb.append("    girlsSeated: ").append(toIndentedString(girlsSeated)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

