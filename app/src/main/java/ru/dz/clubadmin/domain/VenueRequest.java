package ru.dz.clubadmin.domain;


import io.realm.RealmList;
import io.realm.RealmObject;
import ru.dz.clubadmin.domain.models.RealmString;
import ru.dz.clubadmin.domain.models.VenueRole;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arkady on 20/02/16.
 */
public class VenueRequest extends RealmObject {
	
	private Long userId;

	private RealmList<RealmString> preferredRoles;



	public VenueRequest() {
	}

	public VenueRequest(Long userId, List<VenueRole> preferredRoles) {
		RealmList<RealmString> preferredRolesString = new RealmList<>();
		for(VenueRole role : preferredRoles) {
			if(role !=null) {
				preferredRolesString.add(new RealmString(role.name()));
			}
		}
		this.userId = userId;
		this.preferredRoles = preferredRolesString;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<VenueRole> getPreferredRoles() {
		ArrayList<VenueRole> preferredRolesString = new ArrayList<>();
		for(RealmString role : this.preferredRoles) preferredRolesString.add(VenueRole.valueOf(role.getString()));
		return preferredRolesString;
	}

	public void setPreferredRoles(List<VenueRole> preferredRoles) {
		RealmList<RealmString> preferredRolesString = new RealmList<>();
		for(VenueRole role : preferredRoles) {
			if(role !=null) {
				preferredRolesString.add(new RealmString(role.name()));
			}
		}
		this.preferredRoles = preferredRolesString;
	}
}
