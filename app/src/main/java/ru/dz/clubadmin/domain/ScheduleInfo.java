package ru.dz.clubadmin.domain;


import ru.dz.clubadmin.domain.models.EventInfo;

import java.util.List;

public class ScheduleInfo {

    private List<EventInfo> eventInfos;
    private List<ReservationInfo> reservationInfos;

    public List<EventInfo> getEventInfos() {
        return eventInfos;
    }

    public void setEventInfos(List<EventInfo> eventInfos) {
        this.eventInfos = eventInfos;
    }

    public List<ReservationInfo> getReservationInfos() {
        return reservationInfos;
    }

    public void setReservationInfos(List<ReservationInfo> reservationInfos) {
        this.reservationInfos = reservationInfos;
    }


}
