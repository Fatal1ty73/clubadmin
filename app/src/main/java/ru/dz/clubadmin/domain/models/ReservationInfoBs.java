package ru.dz.clubadmin.domain.models;

/**
 * Created by emil on 21.04.16.
 */
public class ReservationInfoBs {
    private int id;
    private String guestName;
    private int guestsNumber;

    public ReservationInfoBs() {
    }

    public ReservationInfoBs(int id, String guestName, int guestsNumber) {
        this.id = id;
        this.guestName = guestName;
        this.guestsNumber = guestsNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public int getGuestsNumber() {
        return guestsNumber;
    }

    public void setGuestsNumber(int guestsNumber) {
        this.guestsNumber = guestsNumber;
    }
}
