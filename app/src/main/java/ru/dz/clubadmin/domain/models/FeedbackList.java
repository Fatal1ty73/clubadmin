package ru.dz.clubadmin.domain.models;


import java.util.List;

public class FeedbackList {
    private List<FeedbackInfo> feedbackList;

    public List<FeedbackInfo> getFeedbackList() {
        return feedbackList;
    }

    public void setFeedbackList(List<FeedbackInfo> feedbackList) {
        this.feedbackList = feedbackList;
    }


}
