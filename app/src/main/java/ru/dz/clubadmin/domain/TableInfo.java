package ru.dz.clubadmin.domain;


import java.io.Serializable;

import io.realm.RealmObject;
import ru.dz.clubadmin.domain.models.BottleServiceTypeEnum;
import ru.dz.clubadmin.domain.models.Place;

/**
 * Created by arkady on 14/03/16.
 */
public class TableInfo extends RealmObject implements Serializable {
    private Integer id;
    private Integer placeNumber;
    private String bottleServiceType;
    private boolean closed;

    public TableInfo() {
    }

    public TableInfo(Place place) {
        this();
        if (place == null) return;
        this.id = place.getId();
        this.placeNumber = place.getPlaceNumber();
        this.bottleServiceType = place.getBottleService().name();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(Integer placeNumber) {
        this.placeNumber = placeNumber;
    }

    public void setBottleService(BottleServiceTypeEnum bottleServiceType) {
        this.bottleServiceType = bottleServiceType.name();
    }

    public BottleServiceTypeEnum getBottleService() {
        return BottleServiceTypeEnum.valueOf(this.bottleServiceType);
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }
}
