package ru.dz.clubadmin.domain.models;


import io.realm.RealmList;
import io.realm.RealmObject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arkady on 15/01/16.
 */


public class User extends RealmObject {


    private Long id;


    private String fullName;


    private String email;


    private String userpic;


    private String phoneNumber;


    private String birthday;


    private String securityCode;


    private String apiKey;


    private FacebookInfo facebookInfo;


    private RealmList<FeedbackInfo> feedbackList = new RealmList<>();

    //TODO Доделать с ролями
    private RealmList<RealmString> preferredRoles;
//    private JsonNode preferredRoles;

    public List<VenueRole> getPreferredRoles() {
        ArrayList<VenueRole> preferredRolesString = new ArrayList<>();
        for(RealmString role : this.preferredRoles) preferredRolesString.add(VenueRole.valueOf(role.getString()));
        return preferredRolesString;
    }

    public void setPreferredRoles(List<VenueRole> preferredRoles) {
        RealmList<RealmString> preferredRolesString = new RealmList<>();
        for(VenueRole role : preferredRoles) preferredRolesString.add(new RealmString(role.name()));
        this.preferredRoles = preferredRolesString;
    }

    public User() {
    }

    public User(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public DateTime getBirthdayDateTime() {
        return DateTime.parse(birthday);
    }

    public void setBirthdayDateTime(DateTime birthday) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        String str = fmt.print(birthday);
        this.birthday = str;
    }

    public FacebookInfo getFacebookInfo() {
        return facebookInfo;
    }

    public void setFacebookInfo(FacebookInfo facebookInfo) {
        this.facebookInfo = facebookInfo;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserpic() {
        return userpic;
    }

    public void setUserpic(String userpic) {
        this.userpic = userpic;
    }

    public List<FeedbackInfo> getFeedbackList() {
        return feedbackList;
    }

    public void setFeedbackList(List<FeedbackInfo> feedbackList) {
        RealmList<FeedbackInfo> feedbackRealmList = new RealmList<>();
        for(FeedbackInfo feedback: feedbackList) feedbackRealmList.add(feedback);
        this.feedbackList = feedbackRealmList;
    }



//
//    public List<VenueRole> getPreferredRoles() {
//        List<VenueRole> result = null;
//        if (preferredRoles != null) {
//            result = Collections.arrayToList(Json.fromJson(preferredRoles, VenueRole[].class));
//        }
//        return result;
//    }
//
//    public void setPreferredRoles(List<VenueRole> preferredRoles) {
//        if (preferredRoles != null) {
//            this.preferredRoles = Json.toJson(preferredRoles);
//        }
//    }


    public String toString() {
        return "User{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", userpic='" + userpic + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", birthday=" + birthday +
                ", securityCode='" + securityCode + '\'' +
                ", facebookInfo=" + facebookInfo +
                ", feedbackList=" + feedbackList +
                ", preferredRoles=" + preferredRoles +
                '}';
    }
}
