package ru.dz.clubadmin.domain.models;

import java.util.ArrayList;
import java.util.List;


/**
 * Report
 */
public class Report   {
  
  private List<ReservationItem> reservationItems = new ArrayList<ReservationItem>();

  
  /**
   **/
  public Report reservationItems(List<ReservationItem> reservationItems) {
    this.reservationItems = reservationItems;
    return this;
  }

  public List<ReservationItem> getReservationItems() {
    return reservationItems;
  }
  public void setReservationItems(List<ReservationItem> reservationItems) {
    this.reservationItems = reservationItems;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Report report = (Report) o;

    return reservationItems != null ? reservationItems.equals(report.reservationItems) : report.reservationItems == null;

  }

  @Override
  public int hashCode() {
    return reservationItems != null ? reservationItems.hashCode() : 0;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Report {\n");
    
    sb.append("    reservationItems: ").append(toIndentedString(reservationItems)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

