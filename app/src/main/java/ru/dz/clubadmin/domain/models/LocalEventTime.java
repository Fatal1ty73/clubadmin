package ru.dz.clubadmin.domain.models;


import io.realm.RealmObject;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;

public class LocalEventTime extends RealmObject {
    private int hour = 0;
    private int minute = 0;
    private int second = 0;
    private int nano = 0;

    public LocalEventTime() {
    }

    public LocalEventTime(DateTime date) {
        if(date != null){
            this.hour = date.getHourOfDay();
            this.minute = date.getMinuteOfHour();
            this.second = date.getSecondOfMinute();
            this.nano = date.getMillisOfSecond();
        }
    }

    public LocalEventTime(int hour, int minute, int second, int nano) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.nano = nano;
    }

    public DateTime getDateTime(){
        return  new LocalTime(hour,minute,second,nano).toDateTimeToday();
    }



    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getNano() {
        return nano;
    }

    public void setNano(int nano) {
        this.nano = nano;
    }
}
