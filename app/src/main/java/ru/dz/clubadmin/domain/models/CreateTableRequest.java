package ru.dz.clubadmin.domain.models;

import java.util.Objects;


/**
 * CreateTableRequest
 */
public class CreateTableRequest   {
  
  private Long id = null;
  private Integer placeNumber = null;
  private BottleServiceTypeEnum bottleServiceType = null;
  private Boolean closed = false;
  private Long venueId = null;

  
  /**
   **/
  public CreateTableRequest id(Long id) {
    this.id = id;
    return this;
  }

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }


  /**
   **/
  public CreateTableRequest placeNumber(Integer placeNumber) {
    this.placeNumber = placeNumber;
    return this;
  }

  public Integer getPlaceNumber() {
    return placeNumber;
  }
  public void setPlaceNumber(Integer placeNumber) {
    this.placeNumber = placeNumber;
  }


  /**
   **/
  public CreateTableRequest bottleServiceType(BottleServiceTypeEnum bottleServiceType) {
    this.bottleServiceType = bottleServiceType;
    return this;
  }

  public BottleServiceTypeEnum getBottleServiceType() {
    return bottleServiceType;
  }
  public void setBottleServiceType(BottleServiceTypeEnum bottleServiceType) {
    this.bottleServiceType = bottleServiceType;
  }


  /**
   **/
  public CreateTableRequest closed(Boolean closed) {
    this.closed = closed;
    return this;
  }

  public Boolean getClosed() {
    return closed;
  }
  public void setClosed(Boolean closed) {
    this.closed = closed;
  }


  /**
   **/
  public CreateTableRequest venueId(Long venueId) {
    this.venueId = venueId;
    return this;
  }

  public Long getVenueId() {
    return venueId;
  }
  public void setVenueId(Long venueId) {
    this.venueId = venueId;
  }



  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateTableRequest {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    placeNumber: ").append(toIndentedString(placeNumber)).append("\n");
    sb.append("    bottleServiceType: ").append(toIndentedString(bottleServiceType)).append("\n");
    sb.append("    closed: ").append(toIndentedString(closed)).append("\n");
    sb.append("    venueId: ").append(toIndentedString(venueId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

