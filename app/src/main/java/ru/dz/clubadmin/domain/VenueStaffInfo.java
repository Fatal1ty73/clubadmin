package ru.dz.clubadmin.domain;


import io.realm.RealmList;
import io.realm.RealmObject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import ru.dz.clubadmin.domain.models.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by arkady on 13/03/16.
 */

public class VenueStaffInfo extends RealmObject implements Serializable {

    private Long id;


    private String fullName;


    private String email;


    private String userpic;


    private String phoneNumber;


    private String birthday;


    private String securityCode;


    private String apiKey;


    private FacebookInfo facebookInfo;

    private boolean hasFacebookProfile;

    private String since;

    private String requestStatus;

    RealmList<FeedbackInfo> feedbackList = new RealmList<>();

    //TODO Доделать с ролями
    private RealmList<RealmString> preferredRoles;
//    private JsonNode preferredRoles;

    public List<VenueRole> getPreferredRoles() {
        ArrayList<VenueRole> preferredRolesString = new ArrayList<>();
        for(RealmString role : this.preferredRoles) preferredRolesString.add(VenueRole.valueOf(role.getString()));
        return preferredRolesString;
    }

    public void setPreferredRoles(List<VenueRole> preferredRoles) {
        RealmList<RealmString> preferredRolesString = new RealmList<>();
        for(VenueRole role : preferredRoles) preferredRolesString.add(new RealmString(role.name()));
        this.preferredRoles = preferredRolesString;
    }

    public VenueStaffInfo() {
    }

    public VenueStaffInfo(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public DateTime getBirthdayDateTime() {
        return DateTime.parse(birthday);
    }

    public void setBirthdayDateTime(DateTime birthday) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        String str = fmt.print(birthday);
        this.birthday = str;
    }

    public FacebookInfo getFacebookInfo() {
        return facebookInfo;
    }

    public void setFacebookInfo(FacebookInfo facebookInfo) {
        this.facebookInfo = facebookInfo;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserpic() {
        return userpic;
    }

    public void setUserpic(String userpic) {
        this.userpic = userpic;
    }

    public List<FeedbackInfo> getFeedbackList() {
        return feedbackList;
    }

    public void setFeedbackList(List<FeedbackInfo> feedbackList) {
        RealmList<FeedbackInfo> feedbackRealmList = new RealmList<>();
        for(FeedbackInfo feedback: feedbackList) feedbackRealmList.add(feedback);
        this.feedbackList = feedbackRealmList;
    }

    public boolean isHasFacebookProfile() {
        return hasFacebookProfile;
    }

    public void setHasFacebookProfile(boolean hasFacebookProfile) {
        this.hasFacebookProfile = hasFacebookProfile;
    }

    public DateTime getSince() {
        //Date date=new Date(Long.valueOf(since));
        return new DateTime(since);
    }

    public void setSince(DateTime since) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        String str = fmt.print(since);
        this.since = str;
    }

    public void setRequestStatus(VenueRequestStatus requestStatus) {
        this.requestStatus = requestStatus.name();
    }

    public VenueRequestStatus getRequestStatus() {
        if(this.requestStatus != null) {
            return VenueRequestStatus.valueOf(this.requestStatus);
        } else return null;
    }

}
