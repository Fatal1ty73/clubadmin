package ru.dz.clubadmin.domain;

import io.realm.RealmList;
import io.realm.RealmObject;
import ru.dz.clubadmin.domain.TableInfo;

import java.util.List;

/**
 * Created by nikitakulagin on 17.05.16.
 */
public class TablesList extends RealmObject {
    private RealmList<TableInfo> tablesList = new RealmList<>();

    public List<TableInfo> getTablesList() {
        return tablesList;
    }

    public void setTablesList(List<TableInfo> tablesList) {
        RealmList<TableInfo> tableInfoRealmList = new RealmList<>();
        for (TableInfo tableInfo: tablesList) tableInfoRealmList.add(tableInfo);
        this.tablesList = tableInfoRealmList;
    }
}
