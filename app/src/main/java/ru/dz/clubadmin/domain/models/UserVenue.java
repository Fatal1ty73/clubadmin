package ru.dz.clubadmin.domain.models;


import io.realm.RealmObject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by arkady on 16/02/16.
 */


public class UserVenue extends RealmObject {


    private UserVenuePK pk = new UserVenuePK();


    private User user;


    private Venue venue;


    private String requestStatus;


    private String since;

//TODO исправить роли до правильной реализации
//    private JsonNode roles;

    public UserVenuePK getPk() {
        return pk;
    }

    public void setPk(UserVenuePK pk) {
        this.pk = pk;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.getPk().userId = user.getId();
        this.user = user;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.getPk().venueId = venue.getId();
        this.venue = venue;
    }

    public void setRequestStatus(VenueRequestStatus requestStatus) {
        this.requestStatus = requestStatus.name();
    }

    public BottleServiceTypeEnum getRequestStatus() {
        return BottleServiceTypeEnum.valueOf(this.requestStatus);
    }

//    public List<VenueRole> getRoles() {
//        return Collections.arrayToList(Json.fromJson(roles, VenueRole[].class));
//    }

//    public void setRoles(List<VenueRole> roles) {
//        this.roles = Json.toJson(roles);
//    }

    public DateTime getSince() {
        return DateTime.parse(this.since);
    }

    public void setSince(DateTime since) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        String str = fmt.print(since);
        this.since = str;
    }
}
