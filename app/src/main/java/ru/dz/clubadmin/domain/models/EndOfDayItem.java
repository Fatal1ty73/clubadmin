package ru.dz.clubadmin.domain.models;

import ru.dz.clubadmin.domain.TableInfo;

import java.util.ArrayList;
import java.util.List;


/**
 * EndOfDayItem
 */
public class EndOfDayItem   {
  
  private TableInfo tableInfo = null;
  private List<EODReservationInfo> reservations = new ArrayList<EODReservationInfo>();

  
  /**
   **/
  public EndOfDayItem tableInfo(TableInfo tableInfo) {
    this.tableInfo = tableInfo;
    return this;
  }
  

  public TableInfo getTableInfo() {
    return tableInfo;
  }
  public void setTableInfo(TableInfo tableInfo) {
    this.tableInfo = tableInfo;
  }


  /**
   **/
  public EndOfDayItem reservations(List<EODReservationInfo> reservations) {
    this.reservations = reservations;
    return this;
  }

  public List<EODReservationInfo> getReservations() {
    return reservations;
  }
  public void setReservations(List<EODReservationInfo> reservations) {
    this.reservations = reservations;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EndOfDayItem {\n");
    
    sb.append("    tableInfo: ").append(toIndentedString(tableInfo)).append("\n");
    sb.append("    reservations: ").append(toIndentedString(reservations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

