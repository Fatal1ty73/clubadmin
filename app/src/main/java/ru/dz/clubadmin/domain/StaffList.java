package ru.dz.clubadmin.domain;

import io.realm.RealmList;
import io.realm.RealmObject;

import java.util.List;

/**
 * Created by arkady on 19/03/16.
 */
public class StaffList extends RealmObject {

    RealmList<VenueStaffInfo> servers = new RealmList<>();

    RealmList<VenueStaffInfo> bussers = new RealmList<>();

    RealmList<VenueStaffInfo> hosts = new RealmList<>();

    public List<VenueStaffInfo> getServers() {
        return servers;
    }

    public void setServers(List<VenueStaffInfo> servers) {
        RealmList<VenueStaffInfo> venueStaffInfoRealmList = new RealmList<>();
        for(VenueStaffInfo venueStaffInfo: servers) venueStaffInfoRealmList.add(venueStaffInfo);
        this.servers = venueStaffInfoRealmList;
    }

    public List<VenueStaffInfo> getBussers() {
        return bussers;
    }

    public void setBussers(List<VenueStaffInfo> bussers) {
        RealmList<VenueStaffInfo> venueStaffInfoRealmList = new RealmList<>();
        for(VenueStaffInfo venueStaffInfo: bussers) venueStaffInfoRealmList.add(venueStaffInfo);
        this.bussers = venueStaffInfoRealmList;
    }

    public List<VenueStaffInfo> getHosts() {
        return hosts;
    }

    public void setHosts(List<VenueStaffInfo> hosts) {
        RealmList<VenueStaffInfo> venueStaffInfoRealmList = new RealmList<>();
        for(VenueStaffInfo venueStaffInfo: hosts) venueStaffInfoRealmList.add(venueStaffInfo);
        this.hosts = venueStaffInfoRealmList;
    }
}
