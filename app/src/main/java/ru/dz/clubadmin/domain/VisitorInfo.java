package ru.dz.clubadmin.domain;

import io.realm.RealmList;
import io.realm.RealmObject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import ru.dz.clubadmin.domain.models.FacebookInfo;
import ru.dz.clubadmin.domain.models.PromotionCompany;
import ru.dz.clubadmin.domain.models.RealmString;
import ru.dz.clubadmin.domain.models.VenueRole;

import java.util.ArrayList;
import java.util.List;


public class VisitorInfo extends RealmObject {
    private Long id;
    private String fullName;
    private String email;
    private String userpic;
    private String phoneNumber;
    private String birthday;
    private RealmList<RealmString> preferredRoles;
    //    private JsonNode preferredRoles;
    private boolean hasFacebookProfile;
    private FacebookInfo facebookInfo;
    private PromotionCompany promotionCompany;
    private boolean isAdmin;
    private short rating;
    private int totalVisits;
    private int bsVisits;
    private int glVisits;
    private int totalReservations;
    private int glReservations;
    private int feedbackCount;
    private int bsReservations;
    private int totalSpent;
    private String lastReservationDate;

    public List<VenueRole> getPreferredRoles() {
        ArrayList<VenueRole> preferredRolesString = new ArrayList<>();
        for(RealmString role : this.preferredRoles) preferredRolesString.add(VenueRole.valueOf(role.getString()));
        return preferredRolesString;
    }

    public void setPreferredRoles(List<VenueRole> preferredRoles) {
        RealmList<RealmString> preferredRolesString = new RealmList<>();
        for(VenueRole role : preferredRoles) preferredRolesString.add(new RealmString(role.name()));
        this.preferredRoles = preferredRolesString;
    }

    public VisitorInfo() {
    }

    public VisitorInfo(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public DateTime getBirthdayDateTime() {
        return DateTime.parse(birthday);
    }

    public void setBirthdayDateTime(DateTime birthday) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        String str = fmt.print(birthday);
        this.birthday = str;
    }

    public FacebookInfo getFacebookInfo() {
        return facebookInfo;
    }

    public void setFacebookInfo(FacebookInfo facebookInfo) {
        this.facebookInfo = facebookInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserpic() {
        return userpic;
    }

    public void setUserpic(String userpic) {
        this.userpic = userpic;
    }

    public boolean isHasFacebookProfile() {
        return hasFacebookProfile;
    }

    public void setHasFacebookProfile(boolean hasFacebookProfile) {
        this.hasFacebookProfile = hasFacebookProfile;
    }

    public short getRating() {
        return rating;
    }

    public Integer getTotalVisits() {
        return totalVisits;
    }

    public Integer getTotalReservations() {
        return totalReservations;
    }

    public Integer getFeedbackCount() {
        return feedbackCount;
    }

    public void setTotalSpent(Integer totalSpent) {
        this.totalSpent = totalSpent;
    }

    public PromotionCompany getPromotionCompany() {
        return promotionCompany;
    }

    public void setPromotionCompany(PromotionCompany promotionCompany) {
        this.promotionCompany = promotionCompany;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public int getBsVisits() {
        return bsVisits;
    }

    public void setBsVisits(int bsVisits) {
        this.bsVisits = bsVisits;
    }

    public int getGlVisits() {
        return glVisits;
    }

    public void setGlVisits(int glVisits) {
        this.glVisits = glVisits;
    }

    public int getGlReservations() {
        return glReservations;
    }

    public void setGlReservations(int glReservations) {
        this.glReservations = glReservations;
    }

    public int getBsReservations() {
        return bsReservations;
    }

    public void setBsReservations(int bsReservations) {
        this.bsReservations = bsReservations;
    }

    public int getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(int totalSpent) {
        this.totalSpent = totalSpent;
    }

    public String getLastReservationDate() {
        return lastReservationDate;
    }

    public void setLastReservationDate(String lastReservationDate) {
        this.lastReservationDate = lastReservationDate;
    }
}
