package ru.dz.clubadmin.loaders.users;

import android.app.Application;

import android.test.ActivityInstrumentationTestCase2;
import android.test.ApplicationTestCase;
import ru.dz.clubadmin.VenueAuthActivity;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class CurrentUserLoaderTest extends ActivityInstrumentationTestCase2<VenueAuthActivity> {

    public CurrentUserLoaderTest(Class<Application> applicationClass) {
        super(VenueAuthActivity.class);
    }

    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}