package ru.dz.clubadmin.util;

import org.junit.Test;
import static org.junit.Assert.*;
import ru.dz.clubadmin.util.ExceptionUtil;

/**
 * Created by crashkin on 28.03.16.
 */
public class ThrowableUtilTest {

    @Test
    public void simpleThrowable() {
        String msg = "Simple message";
        Throwable t = new Throwable(msg);

        assertEquals(msg, ExceptionUtil.causeList(t));
    }

    @Test
    public void doubleThrowable() {
        String msg1 = "Simple message 1";
        String msg2 = null;
        String msg3 = "Simple message 3";

        Throwable t = new Throwable(msg1, new Throwable(msg2, new Throwable(msg3)));

        String totalMsg = String.valueOf(msg1) +
                ExceptionUtil.CAUSE_SEPARATOR +
                String.valueOf(msg2) +
                ExceptionUtil.CAUSE_SEPARATOR +
                String.valueOf(msg3);

        assertEquals(totalMsg, ExceptionUtil.causeList(t));
    }

    @Test
    public void nullThrowable() {
        Throwable t = new Throwable();
        String msg = null;

        assertEquals(String.valueOf(msg), ExceptionUtil.causeList(t));
    }
}
